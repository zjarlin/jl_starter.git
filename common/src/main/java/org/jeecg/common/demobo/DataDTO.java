package org.jeecg.common.demobo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author addzero
 * @since 2022/8/11 9:32 AM
 */
@NoArgsConstructor
@Data
public class DataDTO {
    private String status;
    private String name;

    private String phone;

}
