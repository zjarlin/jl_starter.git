package org.jeecg.common.demobo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author addzero
 * @since 2022/8/8 4:19 PM
 */
@NoArgsConstructor
@Data
public class OutPo {

    private int status;
    private String name;

    private DataDTO data;

}
