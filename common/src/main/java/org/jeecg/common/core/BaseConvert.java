package org.jeecg.common.core;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.common.util.util_entity.core.TableDataInfo;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.mapstruct.Named;

/**
 * @author addzero
 * @since 2022-10-06 13:00:17
 */
public interface BaseConvert<ConditionInVO, InVO, OutVO, PO> {

    static <T> Result<TableDataInfo<T>> ipage2tabledatainfores(Result<IPage<T>> in) {

        if (in == null) {
            return null;
        }

        Result<TableDataInfo<T>> result = new Result<TableDataInfo<T>>();

        result.setSuccess(in.isSuccess());
        result.setMessage(in.getMessage());
        result.setCode(in.getCode());
        result.setResult(ipage2tabledatainfo(in.getResult()));
        result.setTimestamp(in.getTimestamp());
        result.setOnlTable(in.getOnlTable());

        return result;

    }

    static <T> TableDataInfo<T> ipage2tabledatainfo(IPage<T> ipage) {
        if (ipage == null) {
            return null;
        }
        TableDataInfo<T> tableDataInfo = new TableDataInfo<T>();

        tableDataInfo.setTotalPage((int) ipage.getPages());
        tableDataInfo.setPageSize((int) ipage.getSize());
        tableDataInfo.setPageNum((int) ipage.getCurrent());
        List<T> list = ipage.getRecords();
        if (list != null) {
            tableDataInfo.setContent(new ArrayList<T>(list));
        }
        tableDataInfo.setTotal(ipage.getTotal());
        return tableDataInfo;
    }

    @Named("list2string")
    static String list2string(List<String> strings) {
        return StrUtil.join(",", strings);
    }

    @Named("string2list")
    static List<String> string2list(String str) {
        return StrUtil.split(",", str);
    }

    default LocalDateTime localdate2localdatemin(LocalDate localDate) {
        return LocalDateTime.of(localDate, LocalTime.MIN);
    }

    default LocalDateTime localdate2localdatemax(LocalDate localDate) {
        return LocalDateTime.of(localDate, LocalTime.MAX);
    }

    PO vo2po(InVO vo);

    OutVO po2vo(PO po);

    List<PO> vo2pos(List<InVO> vo);

    List<OutVO> po2vos(List<PO> po);

    PO conditionvo2po(ConditionInVO conditionInVO);

    ConditionInVO invo2conditioninvo(InVO vo);
}
