package org.jeecg.common.core;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author addzero
 * @since 2022/10/19 18:39
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueMsg {
    @AliasFor("message")
    String value() default "";

    @AliasFor("value")
    String msg() default "";

    String column() default "";
}
