package org.jeecg.common.core;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.util.auto_wrapper.AutoWhereUtil;
import org.jeecg.common.util.util_entity.core.BaseEntity;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zjarlin
 * @since 2023/3/18 11:28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseService<
    S extends IService<PO>
    , C extends BaseConvert<ConditionInVO, InVO, OutVO, PO>
    , ConditionInVO
    , InVO
    , OutVO
    , PO extends BaseEntity
    > {

    private S service;

    private C convert;

    public static <S extends IService<PO>
        , C extends BaseConvert<ConditionInVO, InVO, OutVO, PO>
        , ConditionInVO
        , InVO
        , OutVO
        , PO extends BaseEntity> BaseService<S, C, ConditionInVO, InVO, OutVO, PO> of(S service, C convert) {
        return new BaseService<>(service, convert);
    }

    public List<OutVO> listConditionalQuery(@Valid @RequestBody ConditionInVO vo) {
        LambdaQueryWrapper<PO> lambdaQueryWrapper = getConditionFun().apply(vo);
        final List<PO> list = service.list(lambdaQueryWrapper);
        return convert.po2vos(list);
    }

    protected Function<ConditionInVO, LambdaQueryWrapper<PO>> getConditionFun() {
        return
            conditionInVO -> {
                PO conditionvo2po = convert.conditionvo2po(conditionInVO);
                Class<?> poClass = conditionvo2po.getClass();
                return (LambdaQueryWrapper<PO>) AutoWhereUtil.lambdaQueryByField(poClass, conditionInVO);
            };
    }

    /**
     * 分页查询
     *
     * @param vo
     * @param pageNo
     * @param pageSize
     * @return {@link IPage }<{@link OutVO }>
     * @author zjarlin
     * @since 2023/03/18
     */
    public IPage<OutVO> page(ConditionInVO vo,
                             @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                             @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        Page<PO> page = new Page<>(pageNo, pageSize);
        LambdaQueryWrapper<PO> lambdaQueryWrapper = getConditionFun().apply(vo);
        Page<PO> pages = service.page(page, lambdaQueryWrapper);
        return pages.convert(convert::po2vo);
    }

    /**
     * 查询所有
     *
     * @return {@link Result }<{@link List }<{@link OutVO }>>
     * @author zjarlin
     * @since 2023/03/18
     */
    public List<OutVO> listAll() {
        //return Result.OK("查询成功", outVOS);

        final List<PO> list = service.list();
        //return Result.OK("查询成功", outVOS);
        return convert.po2vos(list);

    }

    /**
     * id查询单条
     *
     * @param id 入参
     * @return {@link Result }<{@link OutVO }>
     * @author zjarlin
     * @since 2023/03/18
     */
    public OutVO queryById(@RequestParam Serializable id) {
        final PO byId = service.getById(id);
        return convert.po2vo(byId);
    }

    /**
     * 添加
     *
     * @param inVO 入参
     * @return boolean
     * @author zjarlin
     * @since 2023/03/18
     */
    public boolean add(@RequestBody InVO inVO) {
        PO po = convert.vo2po(inVO);
        return service.save(po);
    }

    /**
     * 带唯一索引的新增
     *
     * @param inVO 入参
     * @return {@link Result }<{@link Boolean }>
     * @author zjarlin
     * @since 2023/03/18
     */
    public boolean addIfAbsent(@RequestBody InVO inVO) {
        //拿到注解上的提示消息
        String allQuniqueMsg = getAllQuniqueMsg(inVO);
        if (dataAlreadyExists(inVO)) {
            throw new IllegalArgumentException(allQuniqueMsg);//这里拿不到哪个字段eq或者like出来的数据存在!!
        }
        PO po = convert.vo2po(inVO);
        final boolean save = service.save(po);
        return save;
    }

    /**
     * 得到联合索引提示消息
     *
     * @param inVO 在签证官 入参
     * @return {@link String }
     * @author zjarlin
     * @since 2023/03/18
     */
    protected String getAllQuniqueMsg(InVO inVO) {
        Class<?> aClass = inVO.getClass();
        UniqueMsg uniqueMsg = aClass.getAnnotation(UniqueMsg.class);
        if (Objects.nonNull(uniqueMsg)) {
            String value1 = uniqueMsg.value();
            String msg = uniqueMsg.msg();
            return Stream.of(value1, msg).findAny().orElse("");
        }
        Field[] declaredFields = aClass.getDeclaredFields();
        List<Field> collect = Arrays.stream(declaredFields)
            .filter(field -> {
                Annotation annotation = field.getAnnotation(UniqueMsg.class);
                return Objects.nonNull(annotation);
            }).collect(Collectors.toList());

        return collect.stream()
            .map(field -> {
                    field.setAccessible(true);
                    UniqueMsg annotation = field.getAnnotation(UniqueMsg.class);
                    return annotation.value();
                }
            ).collect(Collectors.joining(","));

    }

    //

    protected boolean dataAlreadyExists(InVO inVO) {
        LambdaQueryWrapper<PO> apply = getUniqueConditionFun().apply(inVO);
        List<PO> list = service.list(apply);
        return CollUtil.isNotEmpty(list);
    }

    //唯一性校验默认对ConditionInVO上加了注解的条件校验
    protected Function<InVO, LambdaQueryWrapper<PO>> getUniqueConditionFun() {
        return
            invo -> {
                Class<?> poClass = convert.vo2po(invo).getClass();
                return (LambdaQueryWrapper<PO>) AutoWhereUtil.lambdaQueryByAnnotation(poClass, invo);
            };
    }

    /**
     * 如果给定条件不存在就修改
     *
     * @param inVO 在签证官 入参
     * @return {@link Result }<{@link Boolean }>
     * @author zjarlin
     * @since 2023/03/18
     */
    @SneakyThrows
    public boolean editIfAbsent(@RequestBody InVO inVO) {
        //拿到入参id
        String idin = (String) ReflectUtil.getFieldValue(inVO, inVO.getClass().getDeclaredField("id"));
        //拿到注解上的提示消息
        String allQuniqueMsg = getAllQuniqueMsg(inVO);
        ConditionInVO conditionInVO = convert.invo2conditioninvo(inVO);
        PO conditionvo2po = convert.conditionvo2po(conditionInVO);
        Class<?> poClass = conditionvo2po.getClass();
        //这里只用对加了注解的条件进行唯一性校验
        LambdaQueryWrapper<PO> lambdaQueryWrapper = (LambdaQueryWrapper<PO>) AutoWhereUtil.lambdaQueryByAnnotation(poClass, conditionInVO);
        List<PO> list = service.list(lambdaQueryWrapper);
        //boolean anyMatch = list.stream().anyMatch(e -> StringUtils.equals(e.getId(), idin));
        boolean nonMatch = list.stream().noneMatch(e -> StringUtils.equals(e.getId(), idin));
        boolean exists = CollUtil.isNotEmpty(list);
        if (exists && nonMatch) {
            throw new IllegalArgumentException(allQuniqueMsg);
        }
        //        查不到(数据库没这条)
//        或者  查出来的info.id和要改的一致(说明是同一条数据),这2种情况下放行
        PO po = convert.vo2po(inVO);
        final boolean body = service.updateById(po);
        return body;
    }

    /**
     * 修改-单条
     *
     * @param inVO 在签证官 入参
     * @return {@link Result }<{@link Boolean }>
     * @author zjarlin
     * @since 2023/03/18
     */
    public boolean edit(@RequestBody InVO inVO) {
        PO po = convert.vo2po(inVO);
        final boolean body = service.updateById(po);
        return body;
    }

    /**
     * 删除-单条j
     *
     * @param id id 入参
     * @return boolean
     * @author zjarlin
     * @since 2023/03/18
     *///    @ApiOperation("删除-单条")
    public boolean delete(@RequestParam Serializable id) {
        return service.removeById(id);
    }

    /**
     * delete-按id批量删除(支持逗号隔开)
     *
     * @param idList id列表 入参
     * @return {@link Result }<{@link Boolean }>
     * @author zjarlin
     * @since 2023/03/18
     */
    public boolean deleteByIdList(@RequestParam List<Long> idList) {
        return service.removeByIds(idList);
    }

}


