package org.jeecg.common.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 基本控制器
 *
 * @author addzero
 * @since 2022/10/04
 */
@SuppressWarnings("all")
public class BaseSimpleController<S extends IService<PO>, PO> {

    @Autowired
    protected S service;

    protected BaseSimpleService<S, PO> baseSimpleService;

    @PostConstruct
    public void init() {
        baseSimpleService = BaseSimpleService.of(service);
    }

    @PostMapping("/listByCondition")
    public Result<List<PO>> listConditionalQuery(@Valid @RequestBody PO vo) {
        List<PO> POS = baseSimpleService.listConditionalQuery(vo);
        return Result.OK("查询成功", POS);
    }

    //    @ApiOperation("分页查询-jeecg生成器自带")
    @GetMapping(value = "/page")
    public Result<IPage<PO>> pageByJeecgCondition(PO vo, @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        return Result.OK(baseSimpleService.page(vo, pageNo, pageSize));
    }

    @GetMapping("listAll")
    @ApiOperation("查询所有")
    public Result<List<PO>> listAll() {
        List<PO> POS = baseSimpleService.listAll();
        return Result.OK(POS);

    }

    @GetMapping("/queryById")
    public Result<PO> queryById(@RequestParam Serializable id) {
        PO PO = baseSimpleService.queryById(id);
        return Result.OK(PO);

    }

    @PostMapping("add")
    @ApiOperation("新增-单条")
    public Result<Boolean> add(@RequestBody PO po) {
        boolean add = baseSimpleService.add(po);
        return Result.OK("新增成功", add);
    }

    @PostMapping("addIfAbsent")
    @ApiOperation("新增-加唯一索引的新增")
    public Result<Boolean> addIfAbsent(@RequestBody PO po) {
        boolean save = baseSimpleService.addIfAbsent(po);
        return Result.OK("新增成功", save);
    }

    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    @ApiOperation("修改-单条")
    public Result<Boolean> edit(@RequestBody PO po) {
        boolean edit = baseSimpleService.edit(po);
        return Result.OK("修改成功", edit);
    }

    @ApiOperation("删除-单条")
    @DeleteMapping(value = "/delete")
    public Result<Boolean> delete(@RequestParam Serializable id) {
        boolean data = service.removeById(id);
        return Result.OK("删除成功", data);
    }

    @DeleteMapping("deleteByIdList")
    @ApiOperation("delete-按id List<>批量删除")
    public Result<Boolean> deleteByIdList(@RequestParam List<Long> idList) {
        return Result.OK("批量删除成功", service.removeByIds(idList));
    }
}
