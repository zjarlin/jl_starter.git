package org.jeecg.common.rc.aop.dicttrans.dictaop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author addzero
 * @since 2022/11/10 14:05
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(
    {
        ElementType.FIELD
        //, ElementType.ANNOTATION_TYPE
    }
)
public @interface DictCode2Names {

    DictCode2Name[] value();

}
