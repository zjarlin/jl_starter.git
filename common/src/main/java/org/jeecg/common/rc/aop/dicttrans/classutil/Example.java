package org.jeecg.common.rc.aop.dicttrans.classutil;

import org.jeecg.common.rc.aop.dicttrans.dictaop.DictCode2Name;
import com.github.houbb.data.factory.api.annotation.DataFactory;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import lombok.Data;

@Data
public class Example {
    //@DictCode2Name("announcement_type")
    @DataFactory(regex = "[1-2]{1,1}")
    @DictCode2Name(tab = "fhys_pro_people", codeColumn = "name", nameColumn = "phone")
    private String name;

    //    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private LocalDateTime ti = LocalDateTime.now();

    //    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private LocalDate ti2 = LocalDate.now();

    //    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private LocalTime ti3 = LocalTime.now();

    private List<Example2> children2;

    private Example2 children3;

}
