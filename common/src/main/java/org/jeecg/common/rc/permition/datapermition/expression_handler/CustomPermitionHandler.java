package org.jeecg.common.rc.permition.datapermition.expression_handler;

import org.jeecg.common.rc.permition.datapermition.anno.ex_anno.CustomPermission;
import org.jeecg.common.rc.permition.datapermition.constant.QueryRuleEnum;
import org.jeecg.common.util.spelutil.SpELUtils;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.HexValue;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.schema.Column;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author zjarlin
 * @since 2023/11/16 13:40
 */
public class CustomPermitionHandler implements PermitionHandler<CustomPermission> {


    @Override
    public Expression addPermition(Expression where, CustomPermission annotation) {
        where = Optional.ofNullable(where).orElse(new HexValue(" 1 = 1 "));
        if (Objects.isNull(annotation)) {
            return where;
        }
//        Table fromItem = (Table) plainSelect.getFromItem();
        // 有别名用别名，无别名用表名，防止字段冲突报错
//        Alias fromItemAlias = fromItem.getAlias();
//        String mainTableName = fromItemAlias == null ? fromItem.getName() : fromItemAlias.getName();

        String column = annotation.column();
        QueryRuleEnum condition1 = annotation.condition();
        String condition = condition1.getCondition();
        String s = annotation.spelPermitionData();
        Class<?> aClass = annotation.spelValueType();
        Object o = SpELUtils.evaluateExpression(s, aClass);

        if (condition.equals(QueryRuleEnum.EQ.getCondition())) {
            EqualsTo usesEqualsTo = new EqualsTo();
            usesEqualsTo.setLeftExpression(new Column(column));
            usesEqualsTo.setRightExpression(new StringValue(o.toString()));
            return new AndExpression(where, usesEqualsTo);
        }

        if (condition.equals(QueryRuleEnum.IN.getCondition())) {
            List<Object> values = (List<Object>) o; // 假设o是一个Object类型的集合
            ExpressionList expressionList = new ExpressionList();
            for (Object value : values) {
                if (value instanceof String) {
                    expressionList.addExpressions(new StringValue((String) value));
                } else if (value instanceof Integer) {
                    expressionList.addExpressions(new LongValue((Integer) value));
                } // 添加其他类型的处理逻辑
            }
            // 构建 InExpression 表达式
            InExpression inExpression = new InExpression();
            inExpression.setLeftExpression(new Column(column));
            inExpression.setRightItemsList(expressionList);
            // 使用 AND 连接原有的条件和自定义条件
            return new AndExpression(where, inExpression);
        }

        if (condition.equals(QueryRuleEnum.LIKE.getCondition())) {
            LikeExpression likeExpression = new LikeExpression();
            likeExpression.setLeftExpression(new Column(column));
            likeExpression.setRightExpression(new StringValue("%" + o.toString() + "%"));
            return new AndExpression(where, likeExpression);
        }


        if (condition.equals(QueryRuleEnum.RIGHT_LIKE.getCondition())) {
            // 构建 LikeExpression 表达式
            LikeExpression rightLikeExpression = new LikeExpression();
            rightLikeExpression.setLeftExpression(new Column(column));
            rightLikeExpression.setRightExpression(new StringValue(o.toString() + "%"));
            // 使用 AND 连接原有的条件和自定义条件
            return new AndExpression(where, rightLikeExpression);
        }
        return where;
    }


}
