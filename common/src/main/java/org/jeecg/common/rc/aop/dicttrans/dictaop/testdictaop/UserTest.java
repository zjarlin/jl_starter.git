package org.jeecg.common.rc.aop.dicttrans.dictaop.testdictaop;

import org.jeecg.common.rc.aop.dicttrans.dictaop.DictCode2Name;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zjarlin
 * @since 2023/10/7 14:03
 */
@Data
@RestController
public class UserTest {

    @ApiModelProperty("角色编码")
    @DictCode2Name("companyCode")
    @DictCode2Name("company_rank")
    String roleCode;
    @ApiModelProperty("手机号")
    @DictCode2Name("propertyInfoType")
    String phone;
    @ApiModelProperty("年龄")
    Integer age;
    @ApiModelProperty("证书")
    @NotBlank
    String 证书;
    String 证书编号;
    @DictCode2Name(tab = "boxun_bystander_record", codeColumn = "id", nameColumn = "key_parts_of_bystanders")
    @DictCode2Name(tab = "boxun_bystander_record", codeColumn = "id", nameColumn = "pouring_location")
    @DictCode2Name(spelExp = "id",spelValueType = String.class, serializationAlias = "files")
    @DictCode2Name(spelExp = "phone",spelValueType = String.class, serializationAlias = "files1")
    @DictCode2Name(spelExp = "age>=0?T(java.util.Arrays).asList(1,2,3):null",spelValueType = List.class, serializationAlias = "files2")
    private String id;
    private TeacherTest teacherTest;
    private List<BoyTest> boyTest;

    @DictCode2Name(spelExp = "@testServiceImpl.transIds(otherIds)",spelValueType = List.class, serializationAlias = "otherIds_dictText")
    private List<String > otherIds;

}
