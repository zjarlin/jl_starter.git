package org.jeecg.common.rc.aop.dicttrans.dictaop.entity;

import org.jeecg.common.rc.aop.dicttrans.dictaop.DictCode2Name;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.function.Function;

/**
 * @author zjarlin
 * @since 2023/10/13 09:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TranslateType {
    Integer translateType;
    Function<DictCode2Name, String> translator;
    Class<?> returnType;
}
