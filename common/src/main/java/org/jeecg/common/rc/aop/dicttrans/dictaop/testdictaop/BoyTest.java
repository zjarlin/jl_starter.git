package org.jeecg.common.rc.aop.dicttrans.dictaop.testdictaop;

import org.jeecg.common.rc.aop.dicttrans.dictaop.DictCode2Name;
import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/10/10 17:55
 */
@Data
public class BoyTest {
    @DictCode2Name(tab = "boxun_bystander_record", codeColumn = "id", nameColumn = "key_parts_of_bystanders")
    @DictCode2Name(tab = "boxun_bystander_record", codeColumn = "id", nameColumn = "pouring_location")
    private String id;

    @DictCode2Name("company_department")
    private String boyName ;
    private int boyage;
}
