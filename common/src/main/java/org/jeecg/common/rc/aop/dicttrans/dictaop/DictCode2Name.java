package org.jeecg.common.rc.aop.dicttrans.dictaop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

/**
 * @author addzero
 * @since 2022/11/10 14:05
 */
@Target({ElementType.METHOD, ElementType.FIELD
    , ElementType.ANNOTATION_TYPE
}
)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(value = DictCode2Names.class)
public @interface DictCode2Name {

    String tableTranslator = "@sysDictServiceImpl.translateDictFromTable(#tab, #nameColumn, #codeColumn, #fieldRuntimeStrValue)";
    String tableMultiTranslator = "@sysDictServiceImpl.queryTableDictTextByKeys2(#tab, #nameColumn, #codeColumn, #fieldRuntimeStrValue)";
    String dictTranslator = "@sysDictServiceImpl.translateDict(#dictCode, #fieldRuntimeStrValue)";
    String dictMultiTranslator = "@sysDictServiceImpl.translateManyDict(#dictCode, #fieldRuntimeStrValue)";

    String tableTranslator() default tableTranslator;

    String tableMultiTranslator() default tableMultiTranslator;

    String dictTranslator() default dictTranslator;
//     Map<String, List<DictModel>>
//   String dictMultiTranslator() default "@sysBaseApiImpl.translateManyDict(#dictCode, #fieldRuntimeStrValue) as T(java.util.Map)<java.lang.String, T(java.util.List)<org.jeecg.crud.sys.model.DictModel>>";

    String dictMultiTranslator() default dictMultiTranslator;

    String spelExp() default "";
    Class<?> spelValueType() default String.class;

    @AliasFor("dicCode")
    String value() default "";

    @AliasFor("value")
    String dicCode() default "";

//    boolean isMulti() default false;

    //如果不是系统内置字典会用到下面的参数
    String tab() default "";

    String codeColumn() default "";

    @AliasFor("serializationAlias")
    String nameColumn() default "";

//    boolean useSysDefaultDict() default true;

    /**
     * 序列化别名
     * <p>
     * 入参
     *
     * @return {@link String }
     * @author zjarlin
     * @since 2023/01/10
     */
    @AliasFor("nameColumn")
    String serializationAlias() default "";

}
