package org.jeecg.common.rc.permition.datapermition.anno.sys_anno;

import java.lang.annotation.*;

/**
 * @author zjarlin
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DeptPermission {
}
