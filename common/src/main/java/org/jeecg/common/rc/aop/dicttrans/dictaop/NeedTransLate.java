package org.jeecg.common.rc.aop.dicttrans.dictaop;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 仅对出参为String格式翻译||标识方法需要翻译
 *
 * @author zjarlin
 * @see Annotation
 * @since 2023/01/05
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NeedTransLate {
    @AliasFor("dicCode")
    String value() default "";

    @AliasFor("value")
    String dicCode() default "";

    boolean isMulti() default false;

    boolean useSysDefaultDict() default true;

    //如果不是系统内置字典会用到下面的参数
    String tab() default "";

    String codeColumn() default "";

    String nameColumn() default "";

    /**
     * 序列化别名
     * <p>
     * 入参
     *
     * @return {@link String }
     * @author zjarlin
     * @since 2023/01/10
     */
    @AliasFor("nameColumn")
    String deserializeAlias() default "";

    String spelExp() default "";
    Class<?> spelValueType() default String.class;
}
