package org.jeecg.common.rc.ignoretoken;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 将此注解加到controller的方法上，即可加入免token名单
 * anno是anonymous的简称
 * @author zjarlin
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)

public @interface NoTokenRequired {
}
