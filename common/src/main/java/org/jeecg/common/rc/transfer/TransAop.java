//package org.jeecg.common.rc.transfer;
//
//import cn.hutool.core.bean.BeanUtil;
//import cn.hutool.core.util.ArrayUtil;
//import cn.hutool.core.util.ReflectUtil;
//import org.jeecg.common.core.Result;
//import org.jeecg.common.rc.aop.dicttrans.classutil.ByteBuddyUtil;
//import org.jeecg.common.util.RefUtil;
//import org.jeecg.common.util.spelutil.SpELUtils;
//import org.jeecg.common.util.util_entity.Describetor;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.lang.reflect.Field;
//import java.util.List;
//import java.util.Map;
//import java.util.Objects;
//import java.util.function.Function;
//import java.util.stream.Collectors;
//
///**
// * @author zjarlin
// * @since 2023/03/25
// */
//@Aspect
//@Component
//@Slf4j
//@SuppressWarnings("all")
//public class TransAop {
//
//    @Autowired
//    ObjectMapper objectMapper;
//
//    private Function<? super String, List<?>> transFun;
//
//
//    @Pointcut("execution(* org.jeecg..BaseController.*(..))")
//    public void p1() {
//    }
//
//    @Pointcut("execution(* org.jeecg.modules..*Api*+.*(..))")
//    public void p2() {
//    }
//
//    //    @Pointcut("execution(* org.jeecg.modules..*.*Controller+.*(..))")
//    @Pointcut("execution(* org.jeecg.modules..*Controller*+.*(..))")
//    public void p3() {
//    }
//
//    /**
//     * 该切面会对实体中属性加了@DictCode2Name注解的字段生效,支持嵌套List<T>属性 T中加了注解的也会被翻译
//     * 支持可重复注解
//     * 支持嵌套翻译
//     *
//     * @param pjp  pjp
//     * @param anno 需要翻译
//     * @return {@link Object }
//     * @author zjarlin
//     * @since 2023/01/05
//     */
//    @Around("(p1()||p2()||p3()) &&  @annotation(anno)")
//    @SneakyThrows
//    public Object transLateDict(ProceedingJoinPoint pjp, Transfer anno) {
//        Object outVO = pjp.proceed();
//        Result result = outVO instanceof Result ? ((Result) outVO) : null;
//        if (Objects.isNull(result)) {
//            return outVO;
//        }
//        Object result1 = result.getResult();
////        Class<?> aClass = result1.getClass();
//        Object o = transOutVOS(result1);
//        return Result.OK(o);
//    }
//
//    @SneakyThrows
//    private Object transOutVOS(Object obj) {
//        Class<?> aClass = obj.getClass();
//        Field[] declaredFields = aClass.getDeclaredFields();
//
//        if (ArrayUtil.isEmpty(declaredFields)) {
//            return obj;
//        }
//
//        List<Describetor> fieldDescribetor = RefUtil.getAnnotationFieldDescribetor(obj, Transfer.class);
//        List<String> needAddList = fieldDescribetor.stream().map(e -> {
//            Transfer annotation1 = (Transfer) e.getAnnotation();
//            return annotation1.serializedName();
//        }).collect(Collectors.toList());
//        Class<?> aClass1 = ByteBuddyUtil.genChildWithList(needAddList, aClass);
//        Object o1 = aClass1.newInstance();
//
//        BeanUtil.copyProperties(obj, o1);
//
////       genChildWithList
//
//
////        JSONObject jsonObject = JSONObject.parseObject(json, Feature.OrderedField);
//        Map<String, ? extends List<?>> collect = fieldDescribetor.stream()
//                .filter(e -> Objects.nonNull(e))
//                .collect(Collectors.toMap(e -> {
//                    Transfer annotation = (Transfer) e.getAnnotation();
//                    String s = annotation.serializedName();
//                    return s;
//                }, e -> {
//                    Transfer annotation = (Transfer) e.getAnnotation();
//                    String transFun4Spel = annotation.transFun4Spel();
//
//                    Object fieldValue = e.getFieldValue();
////                    spelut
//                    String id = String.valueOf(fieldValue);
//
//                    List<?> apply = SpELUtils.evaluateExpressionWithParam(transFun4Spel, id, List.class);
////                    List<?> apply = SpELUtils.evaluateExpressionWithParam("@bizFileService.getBySuperId(#id)",id, List.class);
//                    return apply;
//                }, (o, n) -> n));
//        collect.forEach((k, v) -> {
//            ReflectUtil.setFieldValue(o1, k, v);
//        });
//        return o1;
//    }
//
//
//}
//
