package org.jeecg.common.rc.aop.dicttrans.dictaop.entity;

import lombok.Data;

import java.lang.annotation.Annotation;
import java.util.function.Function;

/**
 * @author zjarlin
 * @since 2023/10/11 16:52
 */
@Data
public class TransInfo<A extends Annotation> {
    /**
     * 父类属性类型是T还是COllT
     */
    private String superObjectFieldTypeEnum;

    /**
     * 父类属性名称
     */
    private String superObjectFieldName;

    /**
     * 上级对象,用于处理嵌套类型,为null说明是当前root对象
     */
    private Object superObject;

    /**
     * 需要递归的属性枚举,常规属性,T,Collection需递归处理
     */
    private String fieldEnum;

    /**
     * 翻译的注解
     */
    private A anno;//

    private Function<Object, Object> translationProcess;

    /**
     * 翻译前的对象
     */
    private Object rootObject;//
    /**
     * 翻译后的对象
     */
    private Object afterObject;
    /**
     * 翻译后的对象字节码
     */
    private Object afterObjectClass;

    /**
     * 翻译后的属性名
     */
    private String translatedAttributeNames;//
    /**
     * 翻译前的属性名
     */
    private String attributeNameBeforeTranslation;//
    /**
     * 翻译前的值
     */
    private Object valueBeforeTranslation;//

    /**
     * 翻译后的值
     */
    private Object translatedValue;
    /**
     * 翻译后的类型
     */
    private Class<?> translatedType;
    /**
     * 翻译的分类
     */
    private Integer classificationOfTranslation;
    /**
     * 根据rootObject和翻译后的属性名生成的唯一标识码
     */
    private String rootObjectHashBsm;
}
