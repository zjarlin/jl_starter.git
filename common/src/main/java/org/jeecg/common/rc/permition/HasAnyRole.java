package org.jeecg.common.rc.permition;

import java.lang.annotation.*;

/**
 *
 * @author zjarlin
 * @see Annotation
 * @since 2023/01/05
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface HasAnyRole {
    String[] value() default "";

    /**
     * isAccess
     * 如当前登录人所拥有角色abc
     * 接口要求具有b能登陆 默认有权限
     * 关闭认为无权限
     * <p>
     * 入参
     *
     * @return boolean
     * @author zjarlin
     * @since 2023/03/02
     */
    boolean isAccess() default true;

}
