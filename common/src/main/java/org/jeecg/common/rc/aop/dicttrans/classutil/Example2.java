package org.jeecg.common.rc.aop.dicttrans.classutil;

import org.jeecg.common.rc.aop.dicttrans.dictaop.DictCode2Name;
import lombok.Data;

@Data
public class Example2 {
    //@DictCode2Name("announcement_type")
    @DictCode2Name(tab = "fhys_pro_people", codeColumn = "id", nameColumn = "phone")
    @DictCode2Name(tab = "fhys_project", codeColumn = "people_id", nameColumn = "projectName")
    private String id;

    //@DictCode2Name(tab = "fhys_pro_record", codeColumn = "id", nameColumn = "building_index")
    //@DictCode2Name(tab = "fhys_pro_record", codeColumn = "id", nameColumn = "floor_no1")
    private String name = "1641254291970719745";

}
