package org.jeecg.common.rc.permition.datapermition;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

public class MethodInvocationChain {
   public static void trackMethodCall(Method method) {
    System.out.println(method.getName());
    Class<?> declaringClass = method.getDeclaringClass();
    if (declaringClass != Object.class) {
        for (Method declaredMethod : declaringClass.getDeclaredMethods()) {
            if (Modifier.isPublic(declaredMethod.getModifiers())) {
                trackMethodCall(declaredMethod);
            }
        }
    }
}

    public static void findMethodCallChain(String methodName, List<String> callChain) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for (StackTraceElement element : stackTrace) {
            String className = element.getClassName();
            String methodNameInStackTrace = element.getMethodName();

            if (methodNameInStackTrace.equals(methodName)) {
                callChain.add(className + "." + methodNameInStackTrace);
                break;
            }
        }

        // 递归向上查找调用链
        int stackTraceLength = stackTrace.length;
        if (callChain.size() < stackTraceLength) {
            String callerMethodName = stackTrace[callChain.size()].getMethodName();
            findMethodCallChain(callerMethodName, callChain);
        }
    }
}
