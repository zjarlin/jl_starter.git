package org.jeecg.common.rc.permition;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

/**
 * 仅为演示,替换为自己系统中的
 * @author addzero
 * @since 2022/9/28 6:27 PM
 */
public class LoginUtil {


//    /**
//     * 获取临时令牌
//     * <p>
//     * 模拟登陆接口，获取模拟 Token
//     *
//     * @param username
//     * @param password
//     * @return
//     */
//    public static String getTemporaryToken(final String username, final String password) {
//        RedisUtil redisUtil = SpringContextUtils.getBean(RedisUtil.class);
//        //模拟登录生成临时Token
//        //参数说明：第一个参数是用户名、第二个参数是密码的加密串
//        String token = JwtUtil.sign(username, password);
//        // 设置Token缓存有效时间为 5 分钟
//        redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
//        redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, 5 * 60 * 1000);
//        return token;
//    }

    @NotNull
    public static String getLoginUserId() {
//        LoginUser loginUser = getLoginUser();
//        String loginUserId = loginUser.getId();
//        return loginUserId;
        return null;
    }

    /**
     * 获取登录用户
     * <p>
     * 入参
     *
     * @return {@link LoginUser }
     * @author addzero
     * @since 2022/09/27
     */
    public static LoginUser getLoginUser() {
//        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        return loginUser;
        return null;
    }

    public static String getLoginUserName() {
        LoginUser loginUser = getLoginUser();
        String username = loginUser.getUsername();
        return username;
    }


    public static String getLoginOrgCode() {
        LoginUser loginUser = getLoginUser();
        return loginUser.getOrgCode();
    }

    public static Set<String> getLoginRoleCodes() {
//        CommonAPI bean = SpringContextUtils.getBean(CommonAPI.class);
//        String loginUserName = getLoginUserName();
//        Set<String> strings = bean.queryUserRoles(loginUserName);
//        LoginUser loginUser = getLoginUser();
        return new HashSet<>();
    }
}
