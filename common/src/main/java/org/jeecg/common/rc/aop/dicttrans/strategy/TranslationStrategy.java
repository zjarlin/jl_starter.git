package org.jeecg.common.rc.aop.dicttrans.strategy;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.common.core.Result;
import org.jeecg.common.util.RefUtil;
import org.jeecg.common.rc.aop.dicttrans.strategy.ex.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

/**
 * @author zjarlin
 * @since 2023/11/8 10:31
 */
@FunctionalInterface
public interface TranslationStrategy<T> {

    Map<Predicate<Object>, TranslationStrategy> predicateTranslationStrategyHashMap = new HashMap<Predicate<Object>, TranslationStrategy>() {{
        put(e -> Result.class.isAssignableFrom(e.getClass()), new ResultStrategy());
        put(e -> Collection.class.isAssignableFrom(e.getClass()), new CollectionStrategy());
        put(e -> IPage.class.isAssignableFrom(e.getClass()), new PageStrategy());
        put(e -> String.class.isAssignableFrom(e.getClass()), new StringStrategy());
        put(RefUtil::isT, new TStrategy());
//        put(e -> e.getClass().isAssignableFrom(Collection.class), new CollectionStrategy());
//        put(e -> e.getClass().isAssignableFrom(Collection.class), new CollectionStrategy());
//        put(e -> e.getClass().isAssignableFrom(Collection.class), new CollectionStrategy());
//        put(e -> e.getClass().isAssignableFrom(Collection.class), new CollectionStrategy());
    }};
    //    private T t;


    //    public Map<Predicate<T>, TranslationStrategy> 注册(Predicate<T> predicate, TranslationStrategy<T> tTranslationStrategy) {
//        this.predicateTranslationStrategyHashMap.put(predicate, tTranslationStrategy);
//        return predicateTranslationStrategyHashMap;
//    }
    public static void registrationPolicy(Predicate<Object> predicate, TranslationStrategy translationStrategy) {
        predicateTranslationStrategyHashMap.put(predicate, translationStrategy);
    }


    public static <T> TranslationStrategy<T> getStrategy(T t) {
        return predicateTranslationStrategyHashMap.entrySet().stream().filter(e -> {
            Predicate<T> key1 = (Predicate<T>) e.getKey();
            boolean test = key1.test(t);
            return test;
        }).map(Map.Entry::getValue).findAny().orElse(null);
    }

    T trans(T t);

}
