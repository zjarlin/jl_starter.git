package org.jeecg.common.rc.aop.dicttrans.dictaop.entity;

import org.jeecg.common.rc.aop.dicttrans.dictaop.DictCode2Name;
import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Set;

/**
 * @author zjarlin
 * @since 2023/10/11 12:09
 */
@Data
public class AfterObject {
    Object afterObject;
    List<Describetor<DictCode2Name>> describetors;
    //        Map<String, List<DictCode2Name>> annoMap;
    Set<Pair<String, ? extends Class<?>>> needAddField;
}
