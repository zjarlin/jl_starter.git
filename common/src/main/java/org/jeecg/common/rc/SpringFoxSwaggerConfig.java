package org.jeecg.common.rc;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/***
 * @author qingfeng.zhao
 * @date 2022/3/26
 * @apiNote
 */
@EnableOpenApi
@Configuration
public class SpringFoxSwaggerConfig {
    //@Bean
    //public ApiInfo apiInfo() {
    //    return new ApiInfoBuilder()
    //            .title("Swagger Test App Restful API")
    //            .value("swagger test app restful api")
    //            .termsOfServiceUrl("https://github.com/geekxingyun")
    //            .contact(new Contact("技术宅星云","https://xingyun.blog.csdn.net","fairy_xingyun@hotmail.com"))
    //            .version("1.0")
    //            .build();
    //}

    //2.5.6低版本管用
    //@Bean
    //public static BeanPostProcessor springfoxHandlerProviderBeanPostProcessor() {
    //    return new BeanPostProcessor() {
    //
    //        @Override
    //        public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    //            if (bean instanceof WebMvcRequestHandlerProvider || bean instanceof WebFluxRequestHandlerProvider) {
    //                customizeSpringfoxHandlerMappings(getHandlerMappings(bean));
    //            }
    //            return bean;
    //        }
    //
    //        private <T extends RequestMappingInfoHandlerMapping> void customizeSpringfoxHandlerMappings(List<T> mappings) {
    //            List<T> copy = mappings.stream()
    //                .filter(mapping -> mapping.getPatternParser() == null)
    //                .collect(Collectors.toList());
    //            mappings.clear();
    //            mappings.addAll(copy);
    //        }
    //
    //        @SuppressWarnings("unchecked")
    //        private List<RequestMappingInfoHandlerMapping> getHandlerMappings(Object bean) {
    //            try {
    //                Field field = ReflectionUtils.findField(bean.getClass(), "handlerMappings");
    //                field.setAccessible(true);
    //                return (List<RequestMappingInfoHandlerMapping>) field.get(bean);
    //            } catch (IllegalArgumentException | IllegalAccessException e) {
    //                throw new IllegalStateException(e);
    //            }
    //        }
    //    };
    //}
    //
    ///**
    // * 增加如下配置可解决Spring Boot 6.x 与Swagger 3.0.0 不兼容问题
    // *
    // * @param webEndpointsSupplier        web端点供应商
    // * @param servletEndpointsSupplier    servlet端点供应商
    // * @param controllerEndpointsSupplier 控制器终端供应商
    // * @param endpointMediaTypes          端点媒体类型
    // * @param corsProperties              歌珥属性
    // * @param webEndpointProperties       web端点属性
    // * @param environment                 环境 入参
    // * @return {@link WebMvcEndpointHandlerMapping }
    // * @author addzero
    // * @since 2022/11/29
    // */
    //@Bean
    //public WebMvcEndpointHandlerMapping webEndpointServletHandlerMapping(
    //        WebEndpointsSupplier webEndpointsSupplier, ServletEndpointsSupplier servletEndpointsSupplier,
    //        ControllerEndpointsSupplier controllerEndpointsSupplier, EndpointMediaTypes endpointMediaTypes,
    //        CorsEndpointProperties corsProperties, WebEndpointProperties webEndpointProperties, Environment environment) {
    //    List<ExposableEndpoint<?>> allEndpoints = new ArrayList<>();
    //    Collection<ExposableWebEndpoint> webEndpoints = webEndpointsSupplier.getEndpoints();
    //    allEndpoints.addAll(webEndpoints);
    //    allEndpoints.addAll(servletEndpointsSupplier.getEndpoints());
    //    allEndpoints.addAll(controllerEndpointsSupplier.getEndpoints());
    //    String basePath = webEndpointProperties.getBasePath();
    //    EndpointMapping endpointMapping = new EndpointMapping(basePath);
    //    boolean shouldRegisterLinksMapping = webEndpointProperties.getDiscovery().isEnabled() &&
    //            (org.springframework.util.StringUtils.hasText(basePath) || ManagementPortType.get(environment).equals(ManagementPortType.DIFFERENT));
    //    return new WebMvcEndpointHandlerMapping(endpointMapping, webEndpoints, endpointMediaTypes, corsProperties.toCorsConfiguration(), new EndpointLinksResolver(allEndpoints, basePath), shouldRegisterLinksMapping);
    //}

    /**
     * 摘要服务
     * <p>
     * 入参
     *
     * @return {@link Docket }
     * @author addzero
     * @since 2022/11/29
     */
    @Bean
    public Docket serviceDocket() {
        return new Docket(DocumentationType.OAS_30)
            .useDefaultResponseMessages(false)
            .select()
            .apis(RequestHandlerSelectors.basePackage("org.jeecg"))
            .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
            .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
            .paths(PathSelectors.any())
            .build();
    }

}
