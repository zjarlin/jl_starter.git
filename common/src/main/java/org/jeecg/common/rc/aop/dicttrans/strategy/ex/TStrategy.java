package org.jeecg.common.rc.aop.dicttrans.strategy.ex;

import cn.hutool.core.collection.CollUtil;
import org.jeecg.common.rc.aop.dicttrans.dictaop.util.TransUtil;
import org.jeecg.common.rc.aop.dicttrans.strategy.TranslationStrategy;

import java.util.*;

/**
 * @author zjarlin
 * @since 2023/11/8 11:15
 */
public class TStrategy implements TranslationStrategy<Object> {

    private final Map<String, Object> spelContextMap = new HashMap<>();

    @Override
    public Object trans(Object o) {

        spelContextMap.put(TransUtil.OUT_VO, o);
        List<Object> list = Arrays.asList(o);
        CollectionStrategy collectionStrategy = new CollectionStrategy();
        Collection<?> trans = collectionStrategy.trans(list);
        if (CollUtil.isEmpty(trans)) {
            return null;
        }
        return trans.iterator().next();
    }
}
