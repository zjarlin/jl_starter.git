package org.jeecg.common.rc.transfer;

import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/6/20 10:25
 */
@Data
public class TransExp {
    @Transfer
    private String id;
}
