package org.jeecg.common.rc.aop.dicttrans.dictaop.entity;

import lombok.Data;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author zjarlin
 * @since 2023/6/20 09:44
 */
@Data
public class Describetor<A extends Annotation> {

    /**
     * 父类属性类型是T还是COllT
     */
    private String superObjectFieldTypeEnum;
    /**
     * 父类属性名称
     */
    private String superObjectFieldName;

    /**
     * 上级对象,用于处理嵌套类型,为null说明是当前root对象
     */
    private Object superObject;

    /**
     * 需要递归的属性枚举,常规属性,T,Collection需递归处理
     */
    private String fieldEnum;


    private Object rootObject;
    private Class<?> rootObjectClass;


    private String fieldName;
    private Object fieldValue;
    private Type fieldType;
    private A needSearchAnnotation;

    private List<A> needSearchAnnotations;
    private Annotation[] annotations;


}
