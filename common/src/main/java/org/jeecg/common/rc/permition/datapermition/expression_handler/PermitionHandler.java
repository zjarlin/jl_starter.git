package org.jeecg.common.rc.permition.datapermition.expression_handler;

import net.sf.jsqlparser.expression.Expression;

import java.lang.annotation.Annotation;

/**
 * @author zjarlin
 * @since 2023/11/16 13:40
 */
@FunctionalInterface
public interface PermitionHandler <A extends Annotation>{

    public Expression addPermition(Expression where, A anno);

}
