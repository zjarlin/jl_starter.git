package org.jeecg.common.rc.aop.dicttrans.strategy.ex;

import cn.hutool.core.util.StrUtil;
import org.jeecg.common.rc.aop.dicttrans.dictaop.NeedTransLate;
import org.jeecg.common.rc.aop.dicttrans.dictaop.util.TransUtil;
import org.jeecg.common.rc.aop.dicttrans.strategy.TranslationStrategy;
import org.jeecg.common.util.spelutil.SpELUtils;
import org.jeecg.crud.sys.api.CommonAPI;
import org.jeecg.crud.sys.model.DictModel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zjarlin
 * @since 2023/11/8 11:05
 */
@Component
public class StringStrategy implements TranslationStrategy<String> {
    @Setter
    private NeedTransLate needTransLate;
    @Autowired
    private CommonAPI commonAPI;
    private final Map<String, Object> spelContextMap = new HashMap<>();

    @Override
    public String trans(String s) {
        spelContextMap.put(TransUtil.OUT_VO, s);
        return extractSingleAttributeTranslation(s, needTransLate);
    }

    private <T> String extractSingleAttributeTranslation(T fieldRuntimeValue, NeedTransLate dictCode2Name) {
        String dictCode = dictCode2Name.dicCode();
        String tab = dictCode2Name.tab();
        String codeColumn = dictCode2Name.codeColumn();
        String nameColumn = dictCode2Name.nameColumn();
        String spelExp = dictCode2Name.spelExp();
        if (StrUtil.isAllBlank(dictCode, tab, codeColumn, nameColumn)) {
            return String.valueOf(fieldRuntimeValue);
        }
        String fieldRuntimeStrValue = String.valueOf(fieldRuntimeValue);
        codeColumn = StrUtil.toUnderlineCase(codeColumn);
        nameColumn = StrUtil.toUnderlineCase(nameColumn);
        //dictCode不空 这仨参数全是空说明是内置字典翻译
        boolean isUseSysDefaultDict = StrUtil.isNotBlank(dictCode) && StrUtil.isAllBlank(tab, codeColumn, nameColumn);
        String string = fieldRuntimeStrValue;
        boolean isMulti = StrUtil.contains(string, ",");
        boolean useSpel = StrUtil.isNotBlank(spelExp);
        String retStr = "";
        //字典翻译上下文
        spelContextMap.put("dictCode", dictCode);
        spelContextMap.put("fieldRuntimeStrValue", fieldRuntimeValue);

        if (!isUseSysDefaultDict && !isMulti) {
            retStr = commonAPI.translateDictFromTable(tab, nameColumn, codeColumn, fieldRuntimeStrValue);
        }
        if (isUseSysDefaultDict && !isMulti) {
            retStr = commonAPI.translateDict(dictCode, fieldRuntimeStrValue);
        }
        //表翻译上下文
        spelContextMap.put("tab", tab);
        spelContextMap.put("nameColumn", nameColumn);
        spelContextMap.put("codeColumn", codeColumn);
//        spelContextMap.put("fieldRuntimeStrValue", fieldRuntimeValue);
        if (!isUseSysDefaultDict && isMulti) {
            //如果是多个用逗号隔开的翻译
            List<DictModel> dictModels = commonAPI.translateDictFromTableByKeys(tab, nameColumn, codeColumn, fieldRuntimeStrValue);
            Map<String, String> relation = dictModels.stream().collect(Collectors.toMap(DictModel::getValue, DictModel::getLabel));
            String[] split = fieldRuntimeStrValue.split(",");
            retStr = Arrays.stream(split).map(relation::get).collect(Collectors.joining(","));
        }
        //多表翻译
        if (isUseSysDefaultDict && isMulti) {
            Map<String, List<DictModel>> stringListMap = commonAPI.translateManyDict(dictCode, fieldRuntimeStrValue);
            List<DictModel> dictModels = stringListMap.get(dictCode);
            retStr = dictModels.stream().map(DictModel::getLabel).collect(Collectors.joining(","));
        }
        if (useSpel) {
            Class<?> aClass = dictCode2Name.spelValueType();
            Object o = SpELUtils.evaluateExpression(fieldRuntimeValue, spelContextMap, spelExp, aClass);
            retStr = o.toString();
        }
        return retStr;
    }

}
