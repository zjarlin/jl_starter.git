package org.jeecg.common.rc.permition.datapermition.expression_handler;

import org.jeecg.common.rc.exception.JlException;
import org.jeecg.common.rc.permition.LoginUtil;
import org.jeecg.common.rc.permition.datapermition.anno.sys_anno.DeptPermission;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.HexValue;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.schema.Column;

import java.util.Objects;
import java.util.Optional;

import static cn.hutool.core.text.CharSequenceUtil.isBlank;

/**
 * @author zjarlin
 * @since 2023/11/16 13:40
 */
public class DepartPermitionHandler implements PermitionHandler<DeptPermission> {


    @Override
    public Expression addPermition(Expression where, DeptPermission anno) {
        where = Optional.ofNullable(where).orElse(new HexValue(" 1 = 1 "));
        if (Objects.isNull(anno)) {
            return where;
        }
        String loginOrgCode = LoginUtil.getLoginOrgCode();
        //当登录人没有部门时选择抛异常,还是说不走部门权限条件,还是说给一个恒不成立的条件去返回空结果集,这个自己来定
        if (isBlank(loginOrgCode)) {
            throw new JlException("登录人没有部门,请先归属部门,再启用部门权限注解");
//            return new HexValue(" 1 = 2 ");
//            return where;
        }
        //部门一般需要右模糊来决定上下级,或者部门编码.in 组织架构子节点集合,这里根据自己系统情况,这里我使用右模糊
        // 构建 LikeExpression 表达式
        LikeExpression rightLikeExpression = new LikeExpression();
        rightLikeExpression.setLeftExpression(new Column("sys_org_code"));
        rightLikeExpression.setRightExpression(new StringValue(loginOrgCode + "%"));
        // 使用 AND 连接原有的条件和自定义条件
        return new AndExpression(where, rightLikeExpression);
    }


}
