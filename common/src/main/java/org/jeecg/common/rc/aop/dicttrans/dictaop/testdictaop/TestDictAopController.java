package org.jeecg.common.rc.aop.dicttrans.dictaop.testdictaop;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.core.Result;
import org.jeecg.common.rc.aop.dicttrans.dictaop.NeedTransLate;
import com.github.houbb.data.factory.core.util.DataUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * @author zjarlin
 * @since 2023/10/13 10:09
 */
@RestController
@RequestMapping("testDictAopController")
public class TestDictAopController {

    @NeedTransLate
    @GetMapping("testDictAop")
    @ApiOperation("测试翻译")
    public Result<UserTest> UserTestTrans() {
        UserTest exp = DataUtil.build(UserTest.class);
        exp.setId("1652952121777729538");
        exp.setRoleCode("2");
        exp.setPhone("2");
        exp.setOtherIds(Arrays.asList("1","2","3"));
        TeacherTest teacherTest = exp.getTeacherTest();
        teacherTest.setTeacherName("2");
        teacherTest.setId("1652952121777729538");

        List<BoyTest> boyTest = exp.getBoyTest();
        boyTest.forEach(e -> {
            e.setBoyName("2");
            e.setId("1652952121777729538");
        });
        Page<UserTest> objectPage = new Page<>(1, 10);
//        List<UserTest> list = Arrays.asList(exp, exp, exp);
//        Page<UserTest> objectPage1 = objectPage.setRecords(list);
        return Result.OK(exp);

    }


}
