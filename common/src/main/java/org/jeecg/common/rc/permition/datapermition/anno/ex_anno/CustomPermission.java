package org.jeecg.common.rc.permition.datapermition.anno.ex_anno;


import org.jeecg.common.rc.permition.datapermition.constant.QueryRuleEnum;

import java.lang.annotation.*;

/**
 * @author zjarlin
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(CustomPermission.List.class)
//@Inherited
public @interface CustomPermission {

    //    String column() default "";
    String column() default "create_by";

    QueryRuleEnum condition() default QueryRuleEnum.EQ;
//    QueryRuleEnum condition();

    String spelPermitionData() default "T(org.jeecg.common.jlstarter.common.util.LoginUtil).getLoginUserName()";
//    String spelPermitionData() default "";

    Class<?> spelValueType() default String.class;
//    Class<?> spelValueType();

    @Target({ElementType.METHOD, ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @interface List {
        CustomPermission[] value();
    }
}
