package org.jeecg.common.rc.transfer;

import java.lang.annotation.*;

/**
 * @author zjarlin
 * @since 2023/6/20 08:42
 */

@Target({ElementType.FIELD, ElementType.METHOD , ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(value = Transfers.class)
public @interface Transfer {
    String value() default "";
    String serializedName() default "data";

    String transFun4Spel() default "@bizFileService.getBySuperId(#id)";
}
