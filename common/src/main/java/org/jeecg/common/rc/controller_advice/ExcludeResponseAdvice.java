package org.jeecg.common.rc.controller_advice;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
//排除响应处理注解
public @interface ExcludeResponseAdvice {
}
