package org.jeecg.common.rc.exception;

public class JlException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public JlException(String message){
		super(message);
	}

	public JlException(Throwable cause)
	{
		super(cause);
	}

	public JlException(String message, Throwable cause)
	{
		super(message,cause);
	}
}
