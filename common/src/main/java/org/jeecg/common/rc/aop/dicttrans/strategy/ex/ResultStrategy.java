package org.jeecg.common.rc.aop.dicttrans.strategy.ex;

import org.jeecg.common.core.Result;
import org.jeecg.common.rc.aop.dicttrans.dictaop.util.TransUtil;
import org.jeecg.common.rc.aop.dicttrans.strategy.TranslationStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zjarlin
 * @since 2023/11/8 11:13
 */
public class ResultStrategy implements TranslationStrategy<Result> {
    private final Map<String, Object> spelContextMap = new HashMap<>();

    @Override
    public Result trans(Result result) {
        Object result1 = result.getResult();
        spelContextMap.put(TransUtil.OUT_VO, result1);
        TranslationStrategy<Object> strategy = TranslationStrategy.getStrategy(result1);
        Object trans = strategy.trans(result1);
        result.setResult(trans);
        return result;
    }
}
