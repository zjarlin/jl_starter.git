package org.jeecg.common.rc.aop.dicttrans.dictaop.testdictaop;

//import org.jeecg.common.rc.aop.dicttrans.dictaop.DictCode2Name;

import org.jeecg.common.rc.aop.dicttrans.dictaop.DictCode2Name;
import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/10/10 17:55
 */
@Data
public class TeacherTest {

    @DictCode2Name(tab = "boxun_bystander_record", codeColumn = "id", nameColumn = "key_parts_of_bystanders")
    @DictCode2Name(tab = "boxun_bystander_record", codeColumn = "id", nameColumn = "pouring_location")
    private String id;

    //    templateType
    @DictCode2Name("templateType")
    private String teacherName;
//    @DictCode2Name(tab = "fhys_pro_people", codeColumn = "id", nameColumn = "phone")
//    @DictCode2Name(tab = "fhys_project", codeColumn = "people_id", nameColumn = "projectName")
    private int teacherage;
}
