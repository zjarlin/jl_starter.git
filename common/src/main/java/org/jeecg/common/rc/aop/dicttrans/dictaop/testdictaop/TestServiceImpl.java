package org.jeecg.common.rc.aop.dicttrans.dictaop.testdictaop;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zjarlin
 * @since 2023/11/8 15:11
 */
@Service
public class TestServiceImpl {
    public List<String> transIds(List<String> ids) {
        List<String> collect = ids.stream().map(e -> e + "afterTrans").collect(Collectors.toList());
        return collect;
    }


}
