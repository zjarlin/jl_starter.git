package org.jeecg.common.rc.transfer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zjarlin
 * @since 2023/6/20 08:43
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(
    {
        ElementType.FIELD
        //, ElementType.ANNOTATION_TYPE
    }
)

public @interface Transfers {
    Transfer[] value();

}
