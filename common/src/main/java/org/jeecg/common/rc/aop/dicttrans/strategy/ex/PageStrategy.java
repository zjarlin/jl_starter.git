package org.jeecg.common.rc.aop.dicttrans.strategy.ex;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.common.rc.aop.dicttrans.dictaop.util.TransUtil;
import org.jeecg.common.rc.aop.dicttrans.strategy.TranslationStrategy;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zjarlin
 * @since 2023/11/8 11:03
 */
public class PageStrategy implements TranslationStrategy<IPage<?>> {
    private final Map<String, Object> spelContextMap = new HashMap<>();

    @Override
    public IPage trans(IPage iPage) {
        spelContextMap.put(TransUtil.OUT_VO, iPage);
        List<?> records = iPage.getRecords();
        CollectionStrategy collectionStrategy = new CollectionStrategy();
        Collection<?> trans = collectionStrategy.trans(records);
        List<?> collect = trans.stream().collect(Collectors.toList());
        iPage.setRecords(collect);
        return iPage;
    }
}
