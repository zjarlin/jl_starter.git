package org.jeecg.common.rc.exception;

public class Jl401Exception extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public Jl401Exception(String message){
		super(message);
	}

	public Jl401Exception(Throwable cause)
	{
		super(cause);
	}

	public Jl401Exception(String message, Throwable cause)
	{
		super(message,cause);
	}
}
