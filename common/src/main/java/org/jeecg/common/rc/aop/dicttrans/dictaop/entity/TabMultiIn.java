package org.jeecg.common.rc.aop.dicttrans.dictaop.entity;

import lombok.Data;

import java.util.List;

/**
 * @author zjarlin
 * @since 2023/10/13 15:58
 */
@Data
public class TabMultiIn {
    String tab;
    String code;
    String name;
   List<String > keys;
}
