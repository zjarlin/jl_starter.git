package org.jeecg.common.rc.aop.dicttrans.classutil;

import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.parser.deserializer.JavaBeanDeserializer;
import com.github.houbb.data.factory.core.util.DataUtil;
import java.lang.reflect.Type;

/**
 * @author zjarlin
 * @since 2023/4/1 10:29
 */
public class Test {
    public static void main(String[] args) {
        Example2 b1 = DataUtil.build(Example2.class);

    }

    public static final boolean isJavaBean(Type type) {
        if (null == type) {
            throw new NullPointerException();
        }
        // 根据 getDeserializer 返回值类型判断是否为 java bean 类型
        return ParserConfig.global.getDeserializer(type) instanceof JavaBeanDeserializer;
    }
}
