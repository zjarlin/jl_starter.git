package org.jeecg.common.rc.controller_advice;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.core.Result;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * 包装Controller结果,如果已是Result类型,则直接返回,否则进行包装
 * 如果引入了swagger或knife4j的文档生成组件，这里需要仅扫描自己项目的包，否则文档无法正常生成
 *
 * @author zjarlin
 * @see ResponseBodyAdvice
 * @since 2023/03/18
 */
//@RestControllerAdvice(basePackages = "org.jeecg.modules.aqjd")
@RestControllerAdvice(basePackages = "org.jeecg.crud")

public class ResponseAdvice implements ResponseBodyAdvice<Object> {
    private static final Set<Class<?>> EXCLUDED_CONTROLLER_CLASSES = new HashSet<>(Arrays.asList(
//            OpenApiController.class
//        ExcludedController2.class
    ));
    private static final Set<String> EXCLUDED_CONTROLLER_CLASSES2 = new HashSet<>(Arrays.asList(
//            "org.jeecg.modules.scyf.controller.OpenApiController"
//        "org.jeecg.modules.scyf.ExcludedController2"
    ));
    @Autowired
    ObjectMapper objectMapper;

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
//        ExcludeResponseAdvice methodAnnotation = returnType.getMethodAnnotation(ExcludeResponseAdvice.class);
        //如果加了排除注解,跳过包装
        boolean b = returnType.hasMethodAnnotation(ExcludeResponseAdvice.class);
//        boolean hasExcludeAnnotation = returnType.getContainingClass().isAnnotationPresent(ExcludeResponseAdvice.class);
        if (b) {
            return false; // If the class has the ExcludeResponseAdvice annotation, return as-is
        }
        // 如果不需要进行封装的，可以添加一些校验手段，比如添加标记排除的注解
        boolean contains1 = CollUtil.contains(EXCLUDED_CONTROLLER_CLASSES, returnType.getContainingClass());
        if (contains1) {
            return false; // If the class is in the exclusion list, return as-is
        }

        boolean contains = CollUtil.contains(EXCLUDED_CONTROLLER_CLASSES2, returnType.getContainingClass().getName());
        if (contains) {
            return false; // If the class is in the exclusion list, return as-is
        }

        return true;
    }

    @SneakyThrows
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if (Objects.isNull(body)) {
            return Result.OK(null);
        }
        // 提供一定的灵活度，如果body已经被包装了，就不进行包装
        if (body instanceof Result|| body instanceof JSONArray) {
            return body;
        }
        // Check if the body is a JSONObject with certain structure (assuming "code" and "msg" fields)
        if (body instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) body;
            if (jsonObject.containsKey("code") && jsonObject.containsKey("message")) {
                return body;
            }
            return Result.OK(body);
        }
        // 如果返回值是String类型，那就手动把Result对象转换成JSON字符串
        if (body instanceof String) {
            try {
                Result result = JSON.parseObject((String) body, Result.class);
                return this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
            } catch (Exception e) {
                return this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(Result.OK(body));
            }
        }
        return Result.OK(body);
    }
}
