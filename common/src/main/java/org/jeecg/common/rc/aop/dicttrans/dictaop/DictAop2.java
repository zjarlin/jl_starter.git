package org.jeecg.common.rc.aop.dicttrans.dictaop;

import org.jeecg.common.rc.aop.dicttrans.strategy.TranslationStrategy;
import org.jeecg.common.rc.aop.dicttrans.strategy.ex.StringStrategy;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * dict aop4
 *
 * @author zjarlin
 * @since 2023/03/25
 */
@Aspect
@Component
@Slf4j
public class DictAop2 {
    @Autowired
    ObjectMapper objectMapper;

    @Pointcut("execution(* org.jeecg..BaseController.*(..))")
    public void p1() {
    }

    @Pointcut("execution(* org.jeecg..*Controller+.*(..))")
    public void p2() {
    }

    @Pointcut("execution(* org.jeecg..*Api+.*(..))")
    public void p3() {
    }

    /**
     * 该切面会对实体中属性加了注解的字段生效,支持嵌套List<T>属性 T中加了注解的也会被翻译
     *
     * @param pjp pjp
     * @return {@link Object }
     * @author zjarlin
     * @since 2023/01/05
     */
    @Around("(p1()||p2()||p3()) &&  @annotation(dictCode2Name)")
    @SneakyThrows
    public Object transLateDict(ProceedingJoinPoint pjp, NeedTransLate dictCode2Name) {
        Object outVO = pjp.proceed();
        if (String.class.isAssignableFrom(outVO.getClass())) {
            StringStrategy stringStrategy = new StringStrategy() {{
                setNeedTransLate(dictCode2Name);
            }};
            return stringStrategy.trans((String) outVO);
        }
        TranslationStrategy<Object> strategy = TranslationStrategy.getStrategy(outVO);
        return strategy.trans(outVO);

    }


}
