package org.jeecg.common.rc.permition;//package org.jeecg.common.rc.permition;
//
//import cn.hutool.core.collection.CollUtil;
//import java.util.Arrays;
//import java.util.Set;
//import lombok.SneakyThrows;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.jeecg.common.api.CommonAPI;
//import org.jeecg.common.exception.JeecgBootException;
//import org.jeecg.common.jlstarter.util.LoginUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * @author zjarlin
// * @since 2023/3/2 15:19
// */
//@Aspect
//@Component
//public class PermitionAop {
//
//    //@Pointcut("@annotation(HasAnyRole)")
//    //public void p1() {
//    //}
//    @Autowired
//    CommonAPI commonAPI;
//
//    @Around("@annotation(hasAnyRole)")
//    @SneakyThrows
//
//    public Object checkPermition(ProceedingJoinPoint pjp, HasAnyRole hasAnyRole) {
//        String loginUserName = LoginUtil.getLoginUserName();
//        Set<String> strings = commonAPI.queryUserRoles(loginUserName);
//        String[] value = hasAnyRole.value();
//        boolean isAccess = hasAnyRole.isAccess();
//        //hasAnyRole.dicCoOde()
//        boolean has = CollUtil.containsAny(strings, Arrays.asList(value));
//
//        //boolean noAccess = !has && isAccess || has && !isAccess;
//
//        boolean noAccess = has ^ isAccess;
//        if (noAccess) {
//            throw new JeecgBootException("您无权访问");
//        }
//        Object proceed = pjp.proceed();
//
//        return proceed;
//    }
//
//}
