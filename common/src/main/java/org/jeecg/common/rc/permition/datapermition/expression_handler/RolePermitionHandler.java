package org.jeecg.common.rc.permition.datapermition.expression_handler;

import cn.hutool.core.collection.CollUtil;
import org.jeecg.common.rc.permition.LoginUtil;
import org.jeecg.common.rc.permition.datapermition.anno.sys_anno.RolePermission;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.HexValue;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.schema.Column;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zjarlin
 * @since 2023/11/16 13:40
 */
public class RolePermitionHandler implements PermitionHandler<RolePermission> {


    @Override
    public Expression addPermition(Expression where, RolePermission annotation) {
        where = Optional.ofNullable(where).orElse(new HexValue(" 1 = 1 "));
        if (Objects.isNull(annotation)) {
            return where;
        }
        if (hasAnyRole("admin")) {
            return where;
        }
        //比如企业只看自己的             eq逻辑
        if (hasAnyRole("comany_admin")) {
            EqualsTo usesEqualsTo = new EqualsTo();
            usesEqualsTo.setLeftExpression(new Column("create_by"));
            usesEqualsTo.setRightExpression(new StringValue(LoginUtil.getLoginUserName()));
            AndExpression andExpression = new AndExpression(where, usesEqualsTo);
            return andExpression;
        }
        //比如领导看某个区域的          in 逻辑
        if (hasAnyRole("leader")) {
            //todo 这里设置业务集合
            List<Object> values = (List<Object>) new ArrayList<>(); // 假设o是一个Object类型的集合
            ExpressionList expressionList = new ExpressionList();
            for (Object value : values) {
                if (value instanceof String) {
                    expressionList.addExpressions(new StringValue((String) value));
                } else if (value instanceof Integer) {
                    expressionList.addExpressions(new LongValue((Integer) value));
                } // 添加其他类型的处理逻辑
            }
            // 构建 InExpression 表达式
            InExpression inExpression = new InExpression();
            //todo 这里设置业务字段
            inExpression.setLeftExpression(new Column("area_code"));
            inExpression.setRightItemsList(expressionList);
            // 使用 AND 连接原有的条件和自定义条件
            return new AndExpression(where, inExpression);
        }

        if (hasAnyRole("other","other1")) {
            //todo 根据登录人不同的角色执行不同的逻辑
            return where;
        }
        return where;
    }

    public static boolean hasAnyRole(String... roleCode) {
        Set<String> loginRoleCodes = LoginUtil.getLoginRoleCodes();
        boolean b = CollUtil.containsAny(loginRoleCodes, Arrays.stream(roleCode).collect(Collectors.toList()));
        return b;
    }
}
