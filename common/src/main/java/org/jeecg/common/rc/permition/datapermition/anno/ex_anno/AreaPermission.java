package org.jeecg.common.rc.permition.datapermition.anno.ex_anno;


import org.jeecg.common.rc.permition.datapermition.constant.QueryRuleEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

/**
 * @author zjarlin
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@CustomPermission(column = "area_code",condition = QueryRuleEnum.IN,spelPermitionData = "@boxunProjectInfoController.getLoginAreaCodes()",spelValueType = List.class)
public @interface AreaPermission  {
}
