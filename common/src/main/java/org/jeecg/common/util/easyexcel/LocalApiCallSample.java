package org.jeecg.common.util.easyexcel;

import org.jeecg.common.util.Vars;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
/***
* 服务调用的步骤
* 1.用Get方式获取Token
* 2.用POST方式使用携带TOKEN和请求参数体去调用接口
*/
public class LocalApiCallSample {
    @SneakyThrows
    @Test
    public void main() {
        String ip = Vars.ip;

        /**
         * 定义睿治服务器的地址
         */
//            String rzUrl = "http://rzihost:8080/rz";
        String rzUrl = "http://222.88.83.2:8082/edg/";


        /**
         * 获取Token的URL，调用格式为edgopenapi/token/getToken?EDG-APPKEY={EDG-APPKEY}&EDG-APPSECRET={EDG-APPSECRET}
         */
        String tokenUrl = "edgopenapi/token/getToken";

        /**
         * 服务的地址
         */
        String serviceUrl = "edgopenapi/services/personnel";

        /**
         * 从个人中心---API秘钥  获取的EDG-APPKEY
         */
        String param_appkey = "02a89f95e35f462ca70236dbc687845a";

        /**
         * 从个人中心---API秘钥  获取的EDG-APPSECRET
         */
        String param_appsecret = "8888f558b6e54ff6b237d99dfe82fd0e";

        RestTemplate rest = new RestTemplate();
        String getTokenUrl = rzUrl + tokenUrl + "?EDG-APPKEY=" + param_appkey + "&EDG-APPSECRET=" + param_appsecret;

        /***
         * 调用获取token的接口
         * 返回值格式是：
         {
         "code":"",
         "msg":"",
         "data":{
         "token":"",
         "tokenexpiredate":"",
         "refreshtoken":"",
         "refreshtokenexpiredate":""
         }
         }
         *
         */

        String tokenJson = rest.getForObject(getTokenUrl, String.class);
        System.out.println(tokenJson);
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode root = (ObjectNode) objectMapper.readTree(tokenJson);
        ObjectNode dataNode = (ObjectNode) root.get("data");
        String token = dataNode.get("token").asText();

        /**
         * token的请求头
         */
        String header_token = "EDAPI-TOKEN";
        /**
         * 构造请求参数体,是个JSON
         {
         "request" : {
         "$queryOrder-page-size" : 20,//每页数据显示条数
         "$queryOrder-page-index" : 1,//页码
         "username" : "张三"//接口的其他参数
         }
         }
         */
        ObjectNode bodyNode = objectMapper.createObjectNode();
        ObjectNode requestNode = objectMapper.createObjectNode();
        requestNode.put("$queryOrder-page-size", 20);
        requestNode.put("$queryOrder-page-index", 1);
        requestNode.put("user_id", "1");
        bodyNode.set("request", requestNode);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(header_token, token);
        HttpEntity<String> requestEntity = new HttpEntity<>(bodyNode.toString(), headers);
        /**
         * 返回的值的结构是：
         {
         "retcode" : "EDAC-200",//响应码
         "retmsg" : "调用成功",//响应消息
         "response" : {//响应内容节点
         "$data-detail" : [ {//数据详情数组
         "username" : "张三",
         "age":20
         }],
         "$data-total-count":100,//数据总条数
         "$queryOrder-page-size":20,//每页条数
         "$queryOrder-page-index":1//页码
         }
         }
         */
        String result = rest.postForObject(rzUrl + serviceUrl, requestEntity, String.class);
        System.out.println(result);
    }


}
