package org.jeecg.common.util.poi_tl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import lombok.SneakyThrows;
import org.jeecg.common.util.Streams;
import org.jeecg.common.util.poi_tl.entity.FunBox;
import org.jeecg.common.util.poi_tl.zzsoftentity.ApiTableDTO;
import org.jeecg.common.util.poi_tl.zzsoftentity.FieldDTO;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ZZsoftMain {

    public static final String MODEL_NAME = "港口";
    public static final String outDir = "/Users/zjarlin/Desktop/out/";
    public static final String mergeOutDir = "/Users/zjarlin/Desktop/outMerge/";

    @SneakyThrows
    public static void main(String[] args) {

        List<FunBox> allFun = AbsFunBox.getAllFun("org.jeecg.common.util.poi_tl.testcontroller");

        String resourcePath = "/Users/zjarlin/IdeaProjects/jl_starter/common" + "/src/main/java/org/jeecg/common/util/poi_tl/zzsoftTemplate.docx";

        Stream.iterate(0, (i) -> ++i).limit(allFun.size()).map(i -> {
                    FunBox e = allFun.get(i);
                    String restUrl = e.getRestUrl();
                    String des = e.getDes();
                    String funName = e.getFunName();
                    List<FieldDTO> paramiter = e.getParamiter();
                    List<FieldDTO> returns = e.getReturns();

                    ApiTableDTO apiTableDTO = new ApiTableDTO();
                    apiTableDTO.setIndex(i);
                    apiTableDTO.setMethodType(e.getMethodType());
                    apiTableDTO.setRestName(des);
                    apiTableDTO.setRestUrl(restUrl);
                    apiTableDTO.setModelName(MODEL_NAME);

                    paramiter.forEach(x -> {
                        x.setFieldName(StrUtil.addPrefixIfNot(x.getFieldName(), "请求参数:"));
                        //todo 二维区域表格区域不可用一维区域的变量,所以这里还要再set一遍一维信息(poi-tl暂时不支持表格区域引用表头变量)
                        x.setRestName(des);
                        x.setRestUrl(restUrl);
                        x.setModelName(MODEL_NAME);
                    });

                    returns.forEach(x -> {
                        x.setFieldName(StrUtil.addPrefixIfNot(x.getFieldName(), "响应参数:"));
                        //todo 二维区域表格区域不可用一维区域的变量,所以这里还要再set一遍一维信息(poi-tl暂时不支持表格区域引用表头变量)
                        x.setRestName(des);
                        x.setRestUrl(restUrl);
                        x.setModelName(MODEL_NAME);
                    });

                    List<FieldDTO> collect = Stream.of(paramiter, returns)
                            .flatMap(List::stream)
                            .collect(Collectors.toList());

                    //这里根据区分请求响应后的字段去重,注意请求和响应里即便字段一样的也不能去重,;
                    List<FieldDTO> unique = Streams.unique(collect, FieldDTO::getFieldName);
                    apiTableDTO.setFieldDTO(unique);
                    return apiTableDTO;
                })
                .peek(tab -> {
                    Map<String, Object> model = BeanUtil.beanToMap(tab);
                    String snowflakeNextIdStr = IdUtil.getSnowflakeNextIdStr();
                    String outPath = StrUtil.concat(true, outDir, snowflakeNextIdStr, ".docx");
                    PoiTlUtil.export(model, resourcePath, outPath);
                })
                .collect(Collectors.toList());



        WordUtils.combineByDir(mergeOutDir, outDir);

        //PoiTlUtil.export(model, resourcePath, outPath, "tdkjQualityInspects");
//        PoiTlUtil.export(model, resourcePath, outPath);
        //System.out.println (export);
    }

}