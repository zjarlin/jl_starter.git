package org.jeecg.common.util;

import cn.hutool.core.util.StrUtil;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cn.hutool.core.text.CharSequenceUtil.isBlank;
import static java.util.stream.Collectors.toList;

/**
 * @author zjarlin
 * @since 2023/3/17 13:12
 */
public interface JlStrUtil {
    public static String getPathFromRight(String input, int n) {
        List<String> parts = Arrays.asList(input.split("\\."));

        if (parts.size() < n) {
            return input; // 输入字符串中的路径部分不足n个，返回整个输入字符串
        }

        List<String> reversedPaths = parts.stream()
                .limit(parts.size() - n)
                .collect(Collectors.toList());

        return String.join(".", reversedPaths);
    }


    public static String makeSurroundWith(String s, final String fix) {
        String s1 = StrUtil.addPrefixIfNot(s, fix);
        String s2 = StrUtil.addSuffixIfNot(s1, fix);
        return s2;
    }

    /**
     * split指定多个分隔符
     *
     * @param s         要处理的字符串
     * @param separator 多个分隔符 入参
     * @return {@link List }<{@link String }>
     * @author addzero
     * @since 2022/11/10
     */
    public static List<String> split4Separators(String s, String... separator) {
        Integer[] spliter = Arrays.stream(separator).map(s::indexOf).toArray(Integer[]::new);
        int separatorLastIndex = spliter.length - 1;

        List<String> collect = Stream.iterate(0, (index) -> ++index).limit(separatorLastIndex)
                .map(i -> StrUtil.sub(s, spliter[i], spliter[i + 1])).collect(toList());
        int strLastIndex = s.length();
        String sub = StrUtil.sub(s, spliter[separatorLastIndex], strLastIndex);
        collect.add(sub);
        return collect;
    }

    /**
     * 提取rest路径
     * @param url 入参
     * @return {@link String }
     * @author zjarlin
     * @since 2023/11/20
     */

    static String getRestUrl(String url) {
        if (isBlank(url)) {
            return "";
        }
        Pattern pattern = Pattern.compile(".*:\\d+(/[^/]+)(/.*)");
        Matcher matcher = pattern.matcher(url);

        if (matcher.matches() && matcher.groupCount() > 1) {
            return matcher.group(2);
        } else {
            return "";
        }
    }
}
