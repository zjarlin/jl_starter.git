package org.jeecg.common.util.poi_tl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.util.poi_tl.zzsoftentity.FieldDTO;

import java.util.List;

@Data
public class FunBox {
    @ApiModelProperty(value = "接口地址")
    String restUrl;
    @ApiModelProperty(value = "请求方式")
    String methodType;
    @ApiModelProperty(value = "接口描述")

    String des;
    @JsonIgnore
    @ApiModelProperty(value = "方法名称")
    String funName;
    @JsonIgnore
    @ApiModelProperty(value = "参数列表")
    List<FieldDTO> paramiter;

    @ApiModelProperty(value = "响应列表")
    List<FieldDTO> returns;
}