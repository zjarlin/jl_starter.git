//package org.jeecg.common.util.mputil;
//
//import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
//import com.baomidou.mybatisplus.extension.service.IService;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.io.Serializable;
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.List;
//import java.util.Set;
//import java.util.function.BiConsumer;
//import java.util.function.Function;
//import java.util.stream.Collectors;
//
///**
// * 2表级联操作
// * p是主表c是子表
// *
// * @author zjarlin
// * @since 2023/2/26 09:18
// */
////@Component
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Transactional(rollbackFor = Exception.class)
//public class MpUtil<P, C> {
//    protected MpConfig<P> pc;
//    protected McConfigs<P,C> ccs;
//
////    private P po=pc.getP();
////    private List<C> list;
////    private BiConsumer<C, P> setPkCon;
//
//    public boolean pSave(MpConfig<P> pc,McConfigs<P,C>... ccs) {
//        P po = pc.getP();
//        IService<P> ps = pc.getPs();
//        Function<P, String> pgetIdFun = pc.getPgetIdFun();
//        boolean save = ps.save(po);
//        Arrays.stream(ccs).forEach(cc-> {
//            Collection<C> cs1 = cc.getCs();
//            List<C> cs = (List<C>) cs1;
//            Function<C, String> cgetPidFun1 = cc.getCgetPidFun();
//            BiConsumer<C, P> csetPidFun4P = cc.getCsetPidFun4P();
//            IService<C> css1 = cc.getCss();
//            cs.forEach(c-> {
//                csetPidFun4P.accept(c,po);
//            });
//            css1.saveBatch(cs1);
//        });
//
//        return save && b;
//    }
//
//    public boolean pSave(P po, List<C> list, Function<P, String> pgetIdFun, BiConsumer<C, String> csetPkCon) {
//        boolean save = ps.save(po);
//        BiConsumer<C, P> setPkCon = (c, p) -> csetPkCon.accept(c, pgetIdFun.apply(p));
//        boolean b = pSave(po, list, setPkCon);
//        return save && b;
//    }
//
//    private boolean cSave(P po, List<C> list, BiConsumer<C, P> setPkCon) {
//        list.forEach(c -> {
//            setPkCon.accept(c, po);
//        });
//        boolean b = cs.saveBatch(list);
//        return b;
//    }
//
//
//    public boolean pRemove(Serializable id, SFunction<C, String> cGetPidFun) {
//        boolean b = ps.removeById(id);
//        boolean remove = cRemove(id, cGetPidFun);
//        return b && remove;
//    }
//
//    /**
//     * 子表删除
//     *
//     * @param id         id
//     * @param cGetPidFun c得到pid有趣 入参
//     * @return boolean
//     * @author zjarlin
//     * @since 2023/06/10
//     */
//    private boolean cRemove(Serializable id, SFunction<C, String> cGetPidFun) {
//        return cs.lambdaUpdate().eq(cGetPidFun, id).remove();
//    }
//
//    public boolean pRemoveBatch(Collection<P> pos, Function<P, String> pGetIdFun, SFunction<C, String> cGetPidFun) {
//        Set<String> pids = pos.stream().map(pGetIdFun).collect(Collectors.toSet());
//        boolean b = ps.removeBatchByIds(pos);
//        boolean remove = cRemoveBatch(pos, pGetIdFun, cGetPidFun);
//        return b && remove;
//    }
//
//    private boolean cRemoveBatch(Collection<P> pos, Function<P, String> pGetIdFun, SFunction<C, String> cGetPidFun) {
//        Set<String> pids = pos.stream().map(pGetIdFun).collect(Collectors.toSet());
//        return cs.lambdaUpdate().in(cGetPidFun, pids).remove();
//    }
//
//
//}
