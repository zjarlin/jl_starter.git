package org.jeecg.common.util.stream_wrapper;

import java.util.Collection;
import java.util.function.Function;

/**
 * 行为
 *
 * @author zjarlin
 * @since 2023/02/16
 */
public interface Xw<T> {
    StreamWrapper<T> eq(boolean condition, Function<T, ? extends CharSequence> getFun, CharSequence searchSeq);

    StreamWrapper<T> like(boolean condition, Function<T, ? extends CharSequence> getFun, CharSequence searchSeq);

    StreamWrapper<T> in(boolean condition, Function<T, ? extends CharSequence> getFun, Collection<?> searchSeqs);

}
