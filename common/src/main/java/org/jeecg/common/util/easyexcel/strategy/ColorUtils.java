package org.jeecg.common.util.easyexcel.strategy;

import org.apache.poi.ss.usermodel.IndexedColors;

public class ColorUtils {
    public static IndexedColors findClosestColor(int red, int green, int blue) {
        double minDistance = Double.MAX_VALUE;
        IndexedColors closestColor = null;

        for (IndexedColors color : IndexedColors.values()) {
            int colorRed = (color.index >> 16) & 0xFF;
            int colorGreen = (color.index >> 8) & 0xFF;
            int colorBlue = color.index & 0xFF;

            double distance = Math.sqrt(
                    Math.pow(red - colorRed, 2) +
                    Math.pow(green - colorGreen, 2) +
                    Math.pow(blue - colorBlue, 2)
            );

            if (distance < minDistance) {
                minDistance = distance;
                closestColor = color;
            }
        }

        return closestColor;
    }
}
