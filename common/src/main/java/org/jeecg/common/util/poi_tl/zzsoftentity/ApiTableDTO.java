package org.jeecg.common.util.poi_tl.zzsoftentity;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.util.List;

@Data
public class ApiTableDTO {
    @ApiModelProperty("序号")
    Integer index;
    @ApiModelProperty("请求方式")
    String methodType;


    @ApiModelProperty("接口名称")
    String restName;
    @ApiModelProperty("接口地址")
    String restUrl;
    @ApiModelProperty("模块描述")
    String modelName;

   private List<FieldDTO > fieldDTO;
}