package org.jeecg.common.util.stream_wrapper;

import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/3/4 22:32
 */
@Data
class Stu {

    String name;

    Integer age;

    String high;
}
