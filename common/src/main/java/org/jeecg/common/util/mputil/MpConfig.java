package org.jeecg.common.util.mputil;

import com.baomidou.mybatisplus.extension.service.IService;
import lombok.Data;
import org.apache.poi.ss.formula.functions.T;

import java.util.function.Function;

/**
 * @author zjarlin
 * @since 2023/6/11 09:08
 */
@Data
public class MpConfig<P> {
    private  P p;
    private IService<P> ps;
    private Function<P, String> pgetIdFun;
}
