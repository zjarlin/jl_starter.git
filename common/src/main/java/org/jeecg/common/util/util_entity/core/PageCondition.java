package org.jeecg.common.util.util_entity.core;

import lombok.*;

@Data
@NoArgsConstructor
public class PageCondition {
    private Integer pageNum = 1;

    private Integer pageSize = 10;

    protected boolean canEqual(final Object other) {
        return other instanceof PageCondition;
    }

}
