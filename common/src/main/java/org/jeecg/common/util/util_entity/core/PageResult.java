package org.jeecg.common.util.util_entity.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Data
@ApiModel(
    description = "分页查询响应结果"
)
public class PageResult<T> {
    @ApiModelProperty("总数量")
    private int total;
    @ApiModelProperty("页数")
    private int page;
    @ApiModelProperty("每页显示的数据大小")
    private int pageSize;
    @ApiModelProperty("数据列表")
    private List<T> data;

    public PageResult() {
    }

    public PageResult(List<T> list, int page, int pageSize, int total) {
        this.data = list;
        this.page = page;
        this.pageSize = pageSize;
        this.total = total;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PageResult;
    }

}
