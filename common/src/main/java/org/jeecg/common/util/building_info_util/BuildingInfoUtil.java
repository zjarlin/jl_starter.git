package org.jeecg.common.util.building_info_util;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BuildingInfoUtil {

    public static List<BuildInfoRes> generateBuildingInfo(String buildingNum, int numUnits, int numFloors, int numHousesPerFloor) {
        List<BuildInfoRes> buildingInfoList = new ArrayList<>();
        for (int i = 1; i <= numUnits; i++) {
            for (int j = 1; j <= numFloors; j++) {
                for (int k = 1; k <= numHousesPerFloor; k++) {
                    BuildInfoRes buildInfoRes = new BuildInfoRes();
                    buildInfoRes.setBuildingNum(buildingNum);
                    buildInfoRes.setUnitNo(i);
                    String format = String.format("%02d", j);
                    buildInfoRes.setFloorNo(format);
                    //int houseNo = (j - 1) * numHousesPerFloor + k;
                    //int houseNo =  k;
                    //String format1 = String.format("%02d", houseNo);
                    String format1 = String.format("%02d", k);
                    buildInfoRes.setHouseNo(format + format1);
                    buildInfoRes.setBuildingsAndUnitsText(buildingNum + "#号楼" + i + "单元" + j + "层" + k + "户");
                    buildingInfoList.add(buildInfoRes);
                }
            }
        }
        return buildingInfoList;
    }

    public static List<BuildInfoRes> generateBuildingInfo2(String buildingNum, int numUnits, int numFloors, int numHousesPerFloor) {
        return IntStream.rangeClosed(1, numUnits)
            .boxed()
            .flatMap(i -> IntStream.rangeClosed(1, numFloors)
                .boxed()
                .flatMap(j -> IntStream.rangeClosed(1, numHousesPerFloor)
                    .boxed()
                    .map(k -> {
                        BuildInfoRes buildInfoRes = new BuildInfoRes();
                        buildInfoRes.setBuildingNum(buildingNum);
                        buildInfoRes.setUnitNo(i);
                        String format = String.format("%02d", j);
                        buildInfoRes.setFloorNo(format);
                        String format1 = String.format("%02d", k);
                        buildInfoRes.setHouseNo(format + format1);
                        buildInfoRes.setBuildingsAndUnitsText(buildingNum + "#号楼" + i + "单元" + j + "层" + k + "户");
                        return buildInfoRes;
                    })
                )
            )
            .collect(Collectors.toList());
    }

}
