package org.jeecg.common.util.building_info_util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 建筑
 *
 * @author zjarlin
 * @since 2023/02/18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Building {
    private String id;

    private String value;
    private String name;

    /** 1楼盘2单元3层号4房号 */
    private Integer type;

    private String pid;

    private List<Building> children;

}
