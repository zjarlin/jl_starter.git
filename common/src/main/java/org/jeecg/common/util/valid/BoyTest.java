package org.jeecg.common.util.valid;

import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/10/10 17:55
 */
@Data
public class BoyTest {
    private  String boyName;
//    @DictCode2Name(tab = "fhys_pro_people", codeColumn = "id", nameColumn = "phone")
//    @DictCode2Name(tab = "fhys_project", codeColumn = "people_id", nameColumn = "projectName")
    private  int boyage;
}
