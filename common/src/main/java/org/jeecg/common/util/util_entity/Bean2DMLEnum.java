package org.jeecg.common.util.util_entity;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author addzero
 * @since 2022/11/20 15:15
 */
@AllArgsConstructor
@Getter
public enum Bean2DMLEnum {
    String(String.class, "varchar", "(255)"), Integer(Integer.class, "int", ""),
    LocalDateTime(java.time.LocalDateTime.class, "datetime", ""), LocalDate(java.time.LocalDate.class, "date", ""),
    LocalTime(java.time.LocalTime.class, "time", ""), Boolean(Boolean.class, "bit", "(1)"), Float(Float.class, "float", "(6,2)"), Double(Double.class, "double", "(6,2)"),
    BigDecimal(java.math.BigDecimal.class, "decimal", "(10,7)");

    public final Class<?> javaType;

    private final String sqlType;

    private final String length;

    public static <E extends Enum<E>> String get(Class<?> clazz, Function<E, String> filter) {
        Class<Bean2DMLEnum> bean2DMLEnumClass = Bean2DMLEnum.class;
        Bean2DMLEnum[] enumConstants = bean2DMLEnumClass.getEnumConstants();
        Stream<String> stringStream =
            Arrays.stream(enumConstants).filter(anyProperty -> anyProperty.getJavaType() == clazz)
                .map(e -> filter.apply((E) e));
        return stringStream.findAny().orElse("");
    }

}
