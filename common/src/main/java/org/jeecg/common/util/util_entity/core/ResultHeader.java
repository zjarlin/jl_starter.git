package org.jeecg.common.util.util_entity.core;

import java.io.Serializable;
import java.util.StringJoiner;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResultHeader implements Serializable {
    private static final long serialVersionUID = -7196527092849550024L;

    private int code;

    private String name;

    private String message;

    private String traceId;

    private Version version;

    public ResultHeader(Code code) {
        this.code = code.getCode();
        this.name = code.getName();
        this.message = code.getMessage();
    }

    public ResultHeader(int code, String name, String message, String traceId, Version version) {
        this.code = code;
        this.name = name;
        this.message = message;
        this.traceId = traceId;
        this.version = version;
    }

    public static ResultHeader of(int code, String name, String message) {
        ResultHeader header = new ResultHeader();
        header.setCode(code);
        header.setName(name);
        header.setMessage(message);
        return header;
    }

    public static ResultHeaderBuilder builder() {
        return new ResultHeaderBuilder();
    }

    public static class ResultHeaderBuilder {
        private int code;

        private String name;

        private String message;

        private String traceId;

        private Version version;

        ResultHeaderBuilder() {
        }

        public ResultHeaderBuilder code(int code) {
            this.code = code;
            return this;
        }

        public ResultHeaderBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ResultHeaderBuilder message(String message) {
            this.message = message;
            return this;
        }

        public ResultHeaderBuilder traceId(String traceId) {
            this.traceId = traceId;
            return this;
        }

        public ResultHeaderBuilder version(Version version) {
            this.version = version;
            return this;
        }

        public ResultHeader build() {
            return new ResultHeader(this.code, this.name, this.message, this.traceId, this.version);
        }

        public String toString() {
            return (new StringJoiner(",", "ResultHeader.ResultHeaderBuilder(", ")")).add("code=" + this.code).add("name=" + this.name).add("message=" + this.message).add("traceId=" + this.traceId).add("version=" + this.version).toString();
        }
    }
}
