package org.jeecg.common.util.poi_tl;

import lombok.Data;

/**
 * 项目关键点位表(质量大检查)(TdkjQualityInspect)表实体类
 *
 * @author zjarlin
 * @since 2023-03-03 09:16:55
 */
@Data
public class TdkjQualityInspect {

    private String parentId;

    private String inspectionItem;

    private Integer sort;

    private String icon;

    private String nocheck;

    private String level;

    private String itemNumber;

    private String checkContentAndRequire;

    private String inspectionResult;

    private String checkGist;

    private String remark;

}
