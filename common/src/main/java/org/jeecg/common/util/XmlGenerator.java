package org.jeecg.common.util;

import lombok.SneakyThrows;
import okhttp3.*;
import org.apache.commons.lang3.tuple.Triple;

import java.util.HashMap;
import java.util.Map;

public class XmlGenerator {
    public static void main(String[] args) {
        String s2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\"\n" +
                "             xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\"\n" +
                "             xmlns:zz=\"https://self\" targetNamespace=\"http://bpmn.io/bpmn\" exporter=\"bpmn-js (https://demo.bpmn.io)\"\n" +
                "             exporterVersion=\"5.1.2\">\n" +
                "    <collaboration id=\"node_1697769138353\">\n" +
                "        <participant id=\"node_1697769138007\" name=\"泳池\" processRef=\"P_1697769131268\" type=\"Participant\"/>\n" +
                "    </collaboration>\n" +
                "    <process id=\"P_1697769131268\" name=\"{{name}}\" isExecutable=\"false\" uid=\"{{uid}}\" acode=\"dlkc\"\n" +
                "             routename=\"onliedesign\" grouptype=\"{{grouptype}}\" isDefault=\"true\" version=\"V1.0\">\n" +
                "        <extensionElements>\n" +
                "            <zz:inputArgs key=\"startEvent\" label=\"启动事件\" defaultValue=\"\" description=\"\"/>\n" +
                "        </extensionElements>\n" +
                "        <startEvent id=\"node_1697769156239\" name=\"开始节点\" type=\"start\" submissionName=\"移交\" allowRevocation=\"true\">\n" +
                "            <extensionElements>\n" +
                "                <zz:inputArgs key=\"callbackEvents\" label=\"回调事件\" defaultValue=\"\" description=\"\"/>\n" +
                "            </extensionElements>\n" +
                "            <outgoing>node_1697769182957</outgoing>\n" +
                "        </startEvent>\n" +
                "        <endEvent id=\"node_1697769158471\" name=\"结束节点\" type=\"end\">\n" +
                "            <incoming>node_1697769196470</incoming>\n" +
                "        </endEvent>\n" +
                "        <userTask id=\"node_1697769163320\" name=\"发布人申请\" type=\"usertask\" submissionName=\"移交\" allowRevocation=\"true\">\n" +
                "            <extensionElements>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"常规操作\" key=\"generalOperation\" label=\"常规操作\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"参与者\" key=\"participant\" label=\"参与者\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"路由参数\" key=\"routingParameters\" label=\"路由参数\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"表单应用\" key=\"formApplication\" label=\"表单应用\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"事件触发\" key=\"eventTrigger\" label=\"事件触发\"/>\n" +
                "            </extensionElements>\n" +
                "            <incoming>node_1697769182957</incoming>\n" +
                "            <outgoing>node_1697769184603</outgoing>\n" +
                "        </userTask>\n" +
                "        <userTask id=\"node_1697769165636\" name=\"部门负责人审批\" type=\"usertask\" submissionName=\"移交\" allowRevocation=\"true\">\n" +
                "            <extensionElements>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"常规操作\" key=\"generalOperation\" label=\"常规操作\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"参与者\" key=\"participant\" label=\"参与者\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"路由参数\" key=\"routingParameters\" label=\"路由参数\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"表单应用\" key=\"formApplication\" label=\"表单应用\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"事件触发\" key=\"eventTrigger\" label=\"事件触发\"/>\n" +
                "            </extensionElements>\n" +
                "            <incoming>node_1697769184603</incoming>\n" +
                "            <outgoing>node_1697769187854</outgoing>\n" +
                "        </userTask>\n" +
                "        <userTask id=\"node_1697769168502\" name=\"人力行政中心审批\" type=\"usertask\" submissionName=\"移交\" allowRevocation=\"true\">\n" +
                "            <extensionElements>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"常规操作\" key=\"generalOperation\" label=\"常规操作\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"参与者\" key=\"participant\" label=\"参与者\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"路由参数\" key=\"routingParameters\" label=\"路由参数\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"表单应用\" key=\"formApplication\" label=\"表单应用\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"事件触发\" key=\"eventTrigger\" label=\"事件触发\"/>\n" +
                "            </extensionElements>\n" +
                "            <incoming>node_1697769187854</incoming>\n" +
                "            <outgoing>node_1697769191437</outgoing>\n" +
                "        </userTask>\n" +
                "        <userTask id=\"node_1697769170536\" name=\"etest\" type=\"usertask\" submissionName=\"移交\" allowRevocation=\"true\">\n" +
                "            <extensionElements>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"常规操作\" key=\"generalOperation\" label=\"常规操作\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"参与者\" key=\"participant\" label=\"参与者\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"路由参数\" key=\"routingParameters\" label=\"路由参数\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"表单应用\" key=\"formApplication\" label=\"表单应用\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"事件触发\" key=\"eventTrigger\" label=\"事件触发\"/>\n" +
                "            </extensionElements>\n" +
                "            <incoming>node_1697769191437</incoming>\n" +
                "            <outgoing>node_1697769193105</outgoing>\n" +
                "        </userTask>\n" +
                "        <userTask id=\"node_1697769172955\" name=\"ftest\" type=\"usertask\" submissionName=\"移交\" allowRevocation=\"true\">\n" +
                "            <extensionElements>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"常规操作\" key=\"generalOperation\" label=\"常规操作\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"参与者\" key=\"participant\" label=\"参与者\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"路由参数\" key=\"routingParameters\" label=\"路由参数\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"表单应用\" key=\"formApplication\" label=\"表单应用\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"事件触发\" key=\"eventTrigger\" label=\"事件触发\"/>\n" +
                "            </extensionElements>\n" +
                "            <incoming>node_1697769193105</incoming>\n" +
                "            <outgoing>node_1697769194771</outgoing>\n" +
                "        </userTask>\n" +
                "        <userTask id=\"node_1697769175086\" name=\"gtest\" type=\"usertask\" submissionName=\"移交\" allowRevocation=\"true\">\n" +
                "            <extensionElements>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"常规操作\" key=\"generalOperation\" label=\"常规操作\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"参与者\" key=\"participant\" label=\"参与者\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"路由参数\" key=\"routingParameters\" label=\"路由参数\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"表单应用\" key=\"formApplication\" label=\"表单应用\"/>\n" +
                "                <zz:inputArgs defaultValue=\"\" description=\"事件触发\" key=\"eventTrigger\" label=\"事件触发\"/>\n" +
                "            </extensionElements>\n" +
                "            <incoming>node_1697769194771</incoming>\n" +
                "            <outgoing>node_1697769196470</outgoing>\n" +
                "        </userTask>\n" +
                "        <sequenceFlow id=\"node_1697769182957\" sourceRef=\"node_1697769156239\" targetRef=\"node_1697769163320\"/>\n" +
                "        <sequenceFlow id=\"node_1697769184603\" sourceRef=\"node_1697769163320\" targetRef=\"node_1697769165636\"/>\n" +
                "        <sequenceFlow id=\"node_1697769187854\" sourceRef=\"node_1697769165636\" targetRef=\"node_1697769168502\"/>\n" +
                "        <sequenceFlow id=\"node_1697769191437\" sourceRef=\"node_1697769168502\" targetRef=\"node_1697769170536\"/>\n" +
                "        <sequenceFlow id=\"node_1697769193105\" sourceRef=\"node_1697769170536\" targetRef=\"node_1697769172955\"/>\n" +
                "        <sequenceFlow id=\"node_1697769194771\" sourceRef=\"node_1697769172955\" targetRef=\"node_1697769175086\"/>\n" +
                "        <sequenceFlow id=\"node_1697769196470\" sourceRef=\"node_1697769175086\" targetRef=\"node_1697769158471\"/>\n" +
                "    </process>\n" +
                "    <bpmndi:BPMNDiagram id=\"BpmnDiagram_1\">\n" +
                "        <bpmndi:BPMNPlane id=\"BpmnPlane_1\" bpmnElement=\"node_1697769138353\">\n" +
                "            <bpmndi:BPMNShape id=\"node_1697769138007_di\" bpmnElement=\"node_1697769138007\" isHorizontal=\"true\">\n" +
                "                <omgdc:Bounds x=\"70\" y=\"120\" width=\"1210\" height=\"490\"/>\n" +
                "            </bpmndi:BPMNShape>\n" +
                "            <bpmndi:BPMNShape id=\"node_1697769156239_di\" bpmnElement=\"node_1697769156239\">\n" +
                "                <omgdc:Bounds x=\"142\" y=\"332\" width=\"36\" height=\"36\"/>\n" +
                "                <bpmndi:BPMNLabel>\n" +
                "                    <omgdc:Bounds x=\"138\" y=\"371\" width=\"44\" height=\"14\"/>\n" +
                "                </bpmndi:BPMNLabel>\n" +
                "            </bpmndi:BPMNShape>\n" +
                "            <bpmndi:BPMNShape id=\"node_1697769158471_di\" bpmnElement=\"node_1697769158471\">\n" +
                "                <omgdc:Bounds x=\"1132\" y=\"332\" width=\"36\" height=\"36\"/>\n" +
                "                <bpmndi:BPMNLabel>\n" +
                "                    <omgdc:Bounds x=\"1128\" y=\"371\" width=\"44\" height=\"14\"/>\n" +
                "                </bpmndi:BPMNLabel>\n" +
                "            </bpmndi:BPMNShape>\n" +
                "            <bpmndi:BPMNShape id=\"node_1697769163320_di\" bpmnElement=\"node_1697769163320\">\n" +
                "                <omgdc:Bounds x=\"240\" y=\"310\" width=\"100\" height=\"80\"/>\n" +
                "                <bpmndi:BPMNLabel/>\n" +
                "            </bpmndi:BPMNShape>\n" +
                "            <bpmndi:BPMNShape id=\"node_1697769165636_di\" bpmnElement=\"node_1697769165636\">\n" +
                "                <omgdc:Bounds x=\"400\" y=\"310\" width=\"100\" height=\"80\"/>\n" +
                "                <bpmndi:BPMNLabel/>\n" +
                "            </bpmndi:BPMNShape>\n" +
                "            <bpmndi:BPMNShape id=\"node_1697769168502_di\" bpmnElement=\"node_1697769168502\">\n" +
                "                <omgdc:Bounds x=\"550\" y=\"310\" width=\"100\" height=\"80\"/>\n" +
                "                <bpmndi:BPMNLabel/>\n" +
                "            </bpmndi:BPMNShape>\n" +
                "            <bpmndi:BPMNShape id=\"node_1697769170536_di\" bpmnElement=\"node_1697769170536\">\n" +
                "                <omgdc:Bounds x=\"710\" y=\"310\" width=\"100\" height=\"80\"/>\n" +
                "                <bpmndi:BPMNLabel/>\n" +
                "            </bpmndi:BPMNShape>\n" +
                "            <bpmndi:BPMNShape id=\"node_1697769172955_di\" bpmnElement=\"node_1697769172955\">\n" +
                "                <omgdc:Bounds x=\"850\" y=\"310\" width=\"100\" height=\"80\"/>\n" +
                "                <bpmndi:BPMNLabel/>\n" +
                "            </bpmndi:BPMNShape>\n" +
                "            <bpmndi:BPMNShape id=\"node_1697769175086_di\" bpmnElement=\"node_1697769175086\">\n" +
                "                <omgdc:Bounds x=\"990\" y=\"310\" width=\"100\" height=\"80\"/>\n" +
                "                <bpmndi:BPMNLabel/>\n" +
                "            </bpmndi:BPMNShape>\n" +
                "            <bpmndi:BPMNEdge id=\"node_1697769182957_di\" bpmnElement=\"node_1697769182957\">\n" +
                "                <omgdi:waypoint x=\"178\" y=\"350\"/>\n" +
                "                <omgdi:waypoint x=\"240\" y=\"350\"/>\n" +
                "            </bpmndi:BPMNEdge>\n" +
                "            <bpmndi:BPMNEdge id=\"node_1697769184603_di\" bpmnElement=\"node_1697769184603\">\n" +
                "                <omgdi:waypoint x=\"340\" y=\"350\"/>\n" +
                "                <omgdi:waypoint x=\"400\" y=\"350\"/>\n" +
                "            </bpmndi:BPMNEdge>\n" +
                "            <bpmndi:BPMNEdge id=\"node_1697769187854_di\" bpmnElement=\"node_1697769187854\">\n" +
                "                <omgdi:waypoint x=\"500\" y=\"350\"/>\n" +
                "                <omgdi:waypoint x=\"550\" y=\"350\"/>\n" +
                "            </bpmndi:BPMNEdge>\n" +
                "            <bpmndi:BPMNEdge id=\"node_1697769191437_di\" bpmnElement=\"node_1697769191437\">\n" +
                "                <omgdi:waypoint x=\"650\" y=\"350\"/>\n" +
                "                <omgdi:waypoint x=\"710\" y=\"350\"/>\n" +
                "            </bpmndi:BPMNEdge>\n" +
                "            <bpmndi:BPMNEdge id=\"node_1697769193105_di\" bpmnElement=\"node_1697769193105\">\n" +
                "                <omgdi:waypoint x=\"810\" y=\"350\"/>\n" +
                "                <omgdi:waypoint x=\"850\" y=\"350\"/>\n" +
                "            </bpmndi:BPMNEdge>\n" +
                "            <bpmndi:BPMNEdge id=\"node_1697769194771_di\" bpmnElement=\"node_1697769194771\">\n" +
                "                <omgdi:waypoint x=\"950\" y=\"350\"/>\n" +
                "                <omgdi:waypoint x=\"990\" y=\"350\"/>\n" +
                "            </bpmndi:BPMNEdge>\n" +
                "            <bpmndi:BPMNEdge id=\"node_1697769196470_di\" bpmnElement=\"node_1697769196470\">\n" +
                "                <omgdi:waypoint x=\"1090\" y=\"350\"/>\n" +
                "                <omgdi:waypoint x=\"1132\" y=\"350\"/>\n" +
                "            </bpmndi:BPMNEdge>\n" +
                "        </bpmndi:BPMNPlane>\n" +
                "    </bpmndi:BPMNDiagram>\n" +
                "</definitions>\n";
        Triple<String, String, String> vars = Vars.getVars("数据修改功能", "", "");
        String left = vars.getLeft();
        String middle = vars.getMiddle();
        String right = vars.getRight();

        Map<String, String> variables = new HashMap<>();
        variables.put("name", left);
        variables.put("uid", middle);
        variables.put("grouptype", right);
        String s1 = generateXml(s2, variables);
        addxml(s1);
//        System.out.println(s1);
    }

    public static String generateXml(String template, Map<String, String> variables) {
        String xml = template;

        // 替换变量
        for (Map.Entry<String, String> entry : variables.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String placeholder = "\\{\\{" + key + "\\}\\}";
            xml = xml.replaceAll(placeholder, escapeXml(value));
        }

        return xml;
    }

    // XML转义字符处理
    private static String escapeXml(String input) {
        return input
                .replaceAll("&", "&amp;")
                .replaceAll("<", "&lt;")
                .replaceAll(">", "&gt;")
                .replaceAll("\"", "&quot;")
                .replaceAll("'", "&apos;");
    }

    @SneakyThrows
    public static void addxml(String xml) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
        RequestBody body = RequestBody.create(mediaType, "str=" + xml);
        Request request = new Request.Builder()
                .url("http://192.168.16.123:36888/zzportal/design/process/save")
                .method("POST", body)
                .addHeader("Cookie", Vars.token)
                .addHeader("User-Agent", "Apifox/1.0.0 (https://www.apifox.cn)")
                .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .build();
        Response response = client.newCall(request).execute();
        String string = response.body().string();
        String message = response.message();
        System.out.println(message);
        System.out.println(string);
    }
}
