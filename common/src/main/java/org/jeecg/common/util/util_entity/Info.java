package org.jeecg.common.util.util_entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * @author addzero
 * @since 2022/11/20 17:51
 */
@AllArgsConstructor
@Data
public class Info<T, S, F> {
    public BiConsumer<T, F> setFun;

    public Function<S, F> getFun;
}
