package org.jeecg.common.util.poi_tl.testcontroller;

import io.swagger.annotations.ApiOperation;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.jeecg.common.util.poi_tl.zzsoftentity.ApiTableDTO;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/biz1datazone")
public class TestController {
    @PostMapping("/listAll")
    @ApiOperation("查询所有区域")
    public ApiTableDTO listAll(@RequestBody ApiTableDTO dto) {
       return dto;
    }

}