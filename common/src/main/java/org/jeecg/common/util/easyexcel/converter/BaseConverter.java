//package org.jeecg.common.util.easyexcel.converter;
//
//import com.alibaba.excel.converters.Converter;
//import com.alibaba.excel.enums.CellDataTypeEnum;
//import com.alibaba.excel.metadata.CellData;
//import com.alibaba.excel.metadata.GlobalConfiguration;
//import com.alibaba.excel.metadata.property.ExcelContentProperty;
//
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
///**
// * 解决LocalDateTime转换问题
// *
// * @author addzero
// * @see Converter
// * @since 2022/12/05
// */
//public abstract class BaseConverter<T> implements Converter<T> {
//    private Class<T> clazz;
//
//    // 子类传入class，接收LocalDate.class,LocalDateTime.class
//    public BaseConverter(Class<T> clazz) {
//        this.clazz = clazz;
//    }
//
//    @Override
//    public CellDataTypeEnum supportExcelTypeKey() {
//        return CellDataTypeEnum.STRING;
//    }
//
//    @Override
//    public Class supportJavaTypeKey() {
//        return clazz;
//    }
//
//    @Override
//    public T convertToJavaData(CellData cellData, ExcelContentProperty contentProperty,
//                               GlobalConfiguration globalConfiguration) {
//
//        // LocalDateTime 时间转换
//        if (cellData.getData() instanceof LocalDate) {
//            if (cellData.getType().equals(CellDataTypeEnum.NUMBER)) {
//                LocalDate localDate = LocalDate.of(1900, 1, 1);
//                localDate = localDate.plusDays(cellData.getNumberValue().longValue());
//                return (T) localDate;
//            } else if (cellData.getType().equals(CellDataTypeEnum.STRING)) {
//                return (T) LocalDate.parse(cellData.getStringValue(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//            } else {
//                return null;
//            }
//        }
//
//        // LocalDateTime 时间转换
//        if (cellData.getData() instanceof LocalDateTime) {
//            return (T) LocalDateTime.parse(cellData.getStringValue(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
//        }
//        return null;
//
//    }
//
//    @Override
//    public CellData convertToExcelData(T o, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
//        if (o instanceof LocalDate) {
//            LocalDate parse = LocalDate.parse(o.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd")); // todo 此处为伪代码o.String转LocalDate
//            return new CellData<>(parse.toString());
//        }
//
//        if (o instanceof LocalDateTime) {
//            LocalDateTime parse = LocalDateTime.parse(o.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));// todo 此处为伪代码o.String转LocalDate
//            return new CellData<>(parse.toString());
//        }
//
//        return new CellData<>(o.toString());
//    }
//}
