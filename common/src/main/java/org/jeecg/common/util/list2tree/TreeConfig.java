package org.jeecg.common.util.list2tree;

import java.util.Comparator;
import java.util.function.Function;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zjarlin
 * @since 2023/4/8 15:42
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TreeConfig<T> {
    private String nodeName;

    private Function<T, ?> groupFunction;

    private Comparator<? super T> sortFunction;

}
