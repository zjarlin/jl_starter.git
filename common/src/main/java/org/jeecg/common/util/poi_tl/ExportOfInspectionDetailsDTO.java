package org.jeecg.common.util.poi_tl;

import java.util.List;
import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/3/9 14:12
 */
@Data
public class ExportOfInspectionDetailsDTO {
    private String projectName;

    private String passedOrNot;

    private String inspection;

    private List<TdkjQualityInspect> tdkjQualityInspects;

    private List<String> files;

}
