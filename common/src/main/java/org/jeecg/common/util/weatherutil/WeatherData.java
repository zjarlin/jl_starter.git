package org.jeecg.common.util.weatherutil;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zjarlin
 * @since 2023/4/9 22:36
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherData {
    /** 日期 */
    private String date;

    /** 最高温度 */
    private int highTemp;

    /** 最低温度 */
    private int lowTemp;

    /** 上午 */
    private String amCondition;

    /** 下午 */
    private String pmCondition;

    /** 风力风向 */
    private String wind;

    /** 空气质量指数 */
    private int aqi;

}

