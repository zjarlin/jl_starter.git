package org.jeecg.common.util.util_entity.core;

import cn.hutool.core.convert.Convert;
import com.google.common.collect.Lists;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
public class TableDataInfo<T> implements Serializable {
    private Integer pageNum;

    private Integer pageSize;

    private Long total;

    private Integer totalPage;

    private List<T> content;

    public static TableDataInfo EMPTY = new TableDataInfo(Lists.newArrayList(), 0L);


    public TableDataInfo(List<T> list, long total) {
        this.content = list;
        this.total = total;
    }

    public <E> TableDataInfo<E> convertTo(Class<E> clazz) {
        List<E> eList = Convert.toList(clazz, this.content);
        TableDataInfo<E> tableDataInfo = new TableDataInfo();
        tableDataInfo.setContent(eList);
        tableDataInfo.setTotal(this.total);
        return tableDataInfo;
    }

}
