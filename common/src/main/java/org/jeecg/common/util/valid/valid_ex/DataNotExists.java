package org.jeecg.common.util.valid.valid_ex;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = DataNotExistsValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
//@NotBlank // 结合 @NotBlank 使用
public @interface DataNotExists {
    String message() default "数据已存在";

    String tableName() default "";
    String tableColumn() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
