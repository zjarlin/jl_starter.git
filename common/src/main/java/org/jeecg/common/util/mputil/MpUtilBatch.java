//package org.jeecg.common.util.mputil;
//
//import cn.hutool.core.convert.Convert;
//import cn.hutool.core.util.ArrayUtil;
//import cn.hutool.core.util.ReflectUtil;
//import com.baomidou.mybatisplus.extension.service.IService;
//import lombok.AllArgsConstructor;
//import lombok.NoArgsConstructor;
//
//import java.io.Serializable;
//import java.lang.reflect.Field;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.List;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
///**
// * @author zjarlin
// */
//@NoArgsConstructor
//@AllArgsConstructor
//public class MpUtilBatch<P> {
//    private IService<P> ps;
//    private IService<?>[] cs;
//
//    public static <P> MpUtilBatch<P> of(IService<P> ps, IService<?>... cs) {
//        return new MpUtilBatch<P>(ps, cs);
//    }
//
//
//    public  boolean pRemoveBatch(Collection<?> primaryId) {
//        boolean b = ps.removeBatchByIds(primaryId);
//        return b && cRemoveBatch(primaryId);
//    }
//
//    private boolean cRemoveBatch(Collection<?>... primaryId) {
//        return Stream.iterate(0, (i) -> ++i).limit(cs.length).map(i -> {
//                    IService<?> c = cs[i];
//                    Class<?> entityClass = c.getEntityClass();
//                    Field id = ReflectUtil.getField(entityClass, "id");
//                    return c.update().in(ArrayUtil.isNotEmpty(primaryId), id.getName(), primaryId).remove();
//                })
//                .allMatch(e -> e.equals(true));
//    }
//
//    public boolean cRemove(Serializable id) {
//        boolean b = cRemoveBatch(Collections.singletonList(id));
//        return b;
//    }
//    public boolean pRemove(Serializable id) {
//        boolean b = pRemoveBatch(Collections.singletonList(id));
//        return b;
//    }
//
//
//    public  void pSaveBatch(P p) {
//        boolean save = ps.save(p);
//        cs
//    }
//
//    public  void cSaveBatch(P p,List<?>...colls) {
//        boolean save = ps.saveOrUpdate(p);
//        Stream.iterate(0, (i) -> ++i).limit(cs.length).map(i -> {
//            IService<?> c = cs[i];
//            List<?> coll = colls[i];
//            Class<?> entityClass = c.getEntityClass();
//            List<?> collect = coll.stream().map(e -> Convert.convert(entityClass, e)).collect(Collectors.toList());
//            c.saveOrUpdateBatch(collect);
//            return null;
//        }).collect(Collectors.toList());
//    }
//
//}
