package org.jeecg.common.util.building_info_util;

import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/2/17 16:23
 */
@Data
public class BuildInfoRes {
    /** 楼号 */
    private String buildingNum;

    /** 单元号 */
    private Integer unitNo;

    /** 层号 */
    private String floorNo;

    /** 户号 */
    private String houseNo;

    /** 几楼号几单元几户 */
    private String buildingsAndUnitsText;

}
