package org.jeecg.common.util.easyexcel.find_merge_range;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zjarlin
 * @since 2023/11/14 17:30
 */
@Getter
@AllArgsConstructor
enum MergeType {
    横向优先("1"), 纵向优先("2"), Range面积优先("3");

    final String type;
}
