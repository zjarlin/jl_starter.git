package org.jeecg.common.util.util_entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

/**
 * @author zjarlin
 * @since 2023/10/10 16:14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BeanDoc {
    private Object rootObject;
    private Class<?> rootObjectClass;

    private Field field;

    /**
     * 字段类型
     */
    private Type fieldType;
    /**
     * 上级对象,用于处理嵌套类型,为null说明是当前root对象
     */
    private Object superObject;

    /**
     * 需要递归的属性枚举,常规属性,T,Collection需递归处理
     */
    private String fieldEnum;
    /**
     * 字段上的注解
     */
    private Annotation[] annotations;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段类型简单名称
     */
    private String simpleTypeName;

    /**
     * 字段描述
     */
    private String comment;

    /**
     * 运行值
     */
    private Object runTimeValue;

    /**
     * 父类属性类型是T还是COllT
     */
    private String superObjectFieldTypeEnum;
    /**
     * 父类属性名称
     */
    private String superObjectFieldName;


}
