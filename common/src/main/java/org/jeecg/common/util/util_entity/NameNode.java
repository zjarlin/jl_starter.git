package org.jeecg.common.util.util_entity;

import lombok.Data;

import java.util.List;

/**
 * @author addzero
 * @since 2022/10/11 11:14 PM
 */
@Data
public class NameNode {
    Integer id;
    String name;
    Integer pid;
    List<NameNode> children;
}
