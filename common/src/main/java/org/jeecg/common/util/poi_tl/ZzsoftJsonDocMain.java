package org.jeecg.common.util.poi_tl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.TypeReference;
import org.apache.commons.lang3.tuple.Triple;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class ZzsoftJsonDocMain {
    public static final String outDir = "/Users/zjarlin/Desktop/out/";
    public static final String mergeOutDir = "/Users/zjarlin/Desktop/outMerge/";

    public static void main(String[] args) {
        String s = "/Users/zjarlin/IdeaProjects/jl_starter/common/src/main/java/org/jeecg/common/util/poi_tl/temp.json";
        JSON json = JSONUtil.readJSON(FileUtil.file(s), Charset.defaultCharset());

        TypeReference<LinkedHashSet<Triple<Map<String, Object>, String, String>>> typeReference =
                new TypeReference<LinkedHashSet<Triple<Map<String, Object>, String,
                        String>>>() {
                };
        String string = json.toString();
        LinkedHashSet<Triple<Map<String, Object>, String, String>> r =
                com.alibaba.fastjson2.JSON.parseObject(string, typeReference);
        List<Triple<Map<String, Object>, String, String>> collect = r.stream().map(e -> {
            Triple<Map<String, Object>, String, String> e2 = e;

            Map<String, Object> left = e2.getLeft();
            String middle = e2.getMiddle();
            String right = e2.getRight();
            PoiTlUtil.export(left, middle, right);
            return e;
        }).collect(Collectors.toList());
//        Triple<Map<String, Object>, String, String> mapStringStringTriple = collect.get(0);
//        String right = mapStringStringTriple.getRight();
        WordUtils.combineByDir(mergeOutDir, outDir);

    }

}