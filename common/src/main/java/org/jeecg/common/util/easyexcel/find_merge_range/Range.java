package org.jeecg.common.util.easyexcel.find_merge_range;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/11/14 13:31
 */
@Data
@AllArgsConstructor
public class Range {

    private int startRow;
    private int endRow;
    private int startCol;
    private int endCol;
    /**
     * 歧义优先原则
     * 1代表横向合并优先
     * 2代表纵向合并优先
     * 3代表横纵合并range面积优先原则
     */
    private String mergeType;
}
