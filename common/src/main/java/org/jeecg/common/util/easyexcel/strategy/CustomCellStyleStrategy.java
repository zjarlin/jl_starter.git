package org.jeecg.common.util.easyexcel.strategy;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.AbstractCellStyleStrategy;
import com.alibaba.excel.write.style.AbstractVerticalCellStyleStrategy;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

public class CustomCellStyleStrategy extends AbstractVerticalCellStyleStrategy {

    // 重写定义表头样式的方法
//    @Override
//    protected WriteCellStyle headCellStyle(Head head) {
//        WriteCellStyle writeCellStyle = new WriteCellStyle();
//        writeCellStyle.setFillPatternType(FillPatternType.SOLID_FOREGROUND);
////        对于 FFE3D4 这个颜色
//        IndexedColors closestColor = ColorUtils.findClosestColor(255, 227, 212);
//        writeCellStyle.setFillForegroundColor(closestColor.getIndex());
//        WriteFont writeFont = new WriteFont();
//        writeFont.setColor(IndexedColors.BLACK.getIndex());
//        writeFont.setBold(true);
//        writeCellStyle.setWriteFont(writeFont);
//        return writeCellStyle;
//    }

    // 重写定义内容部分样式的方法
    @Override
    protected WriteCellStyle contentCellStyle(Head head) {
        WriteCellStyle writeCellStyle = new WriteCellStyle();
        writeCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
        writeCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        return writeCellStyle;
    }
}
