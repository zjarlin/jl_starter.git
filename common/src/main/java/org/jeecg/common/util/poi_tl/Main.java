package org.jeecg.common.util.poi_tl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson2.JSON;
import java.util.Arrays;
import java.util.Map;
import lombok.SneakyThrows;

public class Main {
    @SneakyThrows
    public static void main(String[] args) {
        String s = "{\"files\":[],\"inspection\":\"123\",\"passedOrNot\":\"不合格\",\"projectName\":\"中科洛阳信息产业园项目（一期）\",\"tdkjQualityInspects\":[{\"checkGist\":\"《建筑工程施工质量验收统一标准》GB50300-2013第5.0.1、5.0.2、5.0.3条。\",\"createBy\":\"-1\",\"createTime\":\"2021-08-10T18:07:49\",\"deleted\":0,\"id\":\"412\",\"inspectionItem\":\"检验批、分项、子分部、分部工程验收记录\",\"inspectionResult\":\"无检验批、分项、子分部、分部工程验收记录或不完整的，检查为不合格。\",\"itemNumber\":\"4.4.4\",\"parentId\":\"408\",\"sort\":0},{\"checkGist\":\"《建筑工程施工质量验收统一标准》GB50300-2013第5.0.3条。\",\"createBy\":\"-1\",\"createTime\":\"2021-08-10T18:07:50\",\"deleted\":0,\"id\":\"416\",\"inspectionItem\":\"单位工程质量竣工验收记录\",\"inspectionResult\":\"无单位工程质量竣工验收记录或不完整的，检查为不合格。\",\"itemNumber\":\"4.4.8\",\"parentId\":\"408\",\"sort\":0}]}";
        ExportOfInspectionDetailsDTO exportOfInspectionDetailsDTO = JSON.parseObject(s, ExportOfInspectionDetailsDTO.class);
        exportOfInspectionDetailsDTO.setFiles(Arrays.asList("djoaisdjo", "djaosdjoiaj"));
        //exportOfInspectionDetailsDTO.setFiles(new ArrayList<>());
        //List<TdkjQualityInspect> tdkjQualityInspects = exportOfInspectionDetailsDTO.getTdkjQualityInspects();
        //List<String> files = exportOfInspectionDetailsDTO.getFiles();

        //String export = EasyWordUtil.export(exportOfInspectionDetailsDTO, "src/findAnnoMao/resources/检查记录详情表.docx", "/Users/zjarlin/Desktop/out.docx");
        Map<String, Object> model = BeanUtil.beanToMap(exportOfInspectionDetailsDTO);
        model.put("img", "https://img2.baidu.com/it/u=3202947311,1179654885&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500");
        String resourcePath = "/Users/zjarlin/IdeaProjects/jl_starter/common/src/main/java/org/jeecg/common/util/poi_tl/检查记录详情表.docx";
        String outPath = "/Users/zjarlin/Desktop/out.docx";
        //PoiTlUtil.export(model, resourcePath, outPath, "tdkjQualityInspects");
        PoiTlUtil.export(model, resourcePath, outPath);
        //System.out.println (export);
    }

}