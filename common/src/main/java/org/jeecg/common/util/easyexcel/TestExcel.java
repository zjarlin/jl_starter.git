package org.jeecg.common.util.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.metadata.data.ImageData;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson.JSON;
import org.jeecg.common.util.easyexcel.entity.ImageDemoData;
import org.jeecg.common.util.easyexcel.entity.MyTemp;
import io.swagger.annotations.ApiOperation;
import org.junit.jupiter.api.Test;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zjarlin
 * @since 2023/11/14 10:23
 */
@RestController
public class TestExcel {

    @Test
    public void 测试导出() {
//        String fileName = "/Users/zjarlin/Desktop/测试合并到处.xlsx";
        String fileName = "测试合并到处.xlsx";
        String s = "[\n" +
                   "    {\n" +
                   "        \"a\": 1,\n" +
                   "        \"b\": 2,\n" +
                   "        \"c\": 2,\n" +
                   "        \"d\": 1,\n" +
                   "        \"e\": 1\n" +
                   "    },\n" +
                   "    {\n" +
                   "        \"a\": 1,\n" +
                   "        \"b\": 2,\n" +
                   "        \"c\": 2,\n" +
                   "        \"d\": 2,\n" +
                   "        \"e\": 3\n" +
                   "    },\n" +
                   "    {\n" +
                   "        \"a\": 1,\n" +
                   "        \"b\": 4,\n" +
                   "        \"c\": 6,\n" +
                   "        \"d\": 3,\n" +
                   "        \"e\": 2\n" +
                   "    }\n" +
                   "]";
        List<MyTemp> data = JSON.parseArray(s, MyTemp.class);
        EasyExcelWriteUtil.listExport(data, fileName);
//        String s1 = "/Users/zjarlin/Desktop/测试合并到处.xlsx";

//        List<MyTemp> read = EasyExcelReadUtil.read(s1, MyTemp.class, 0);
//        System.out.println(read);
    }


    /**
     * 图片导出
     * <p>
     * 1. 创建excel对应的实体对象 参照{@link ImageDemoData}
     * <p>
     * 2. 直接写即可
     */
    @Test
    public void imageWrite() throws Exception {
        String prefixDIr = "/Users/zjarlin/Desktop/";
        String fileName = prefixDIr + "imageWrite" + System.currentTimeMillis() + ".xlsx";

        // 这里注意下 所有的图片都会放到内存 暂时没有很好的解法，大量图片的情况下建议 2选1:
        // 1. 将图片上传到oss 或者其他存储网站: https://www.aliyun.com/product/oss ，然后直接放链接
        // 2. 使用: https://github.com/coobird/thumbnailator 或者其他工具压缩图片

//        String imagePath = prefixDIr + "converter" + File.separator + "img.jpg";
        List<ImageDemoData> list = ListUtils.newArrayList();
        ImageDemoData imageDemoData = new ImageDemoData();
        imageDemoData.setUrl(new URL("https://img1.baidu.com/it/u=1414232962,2149362867&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1700154000&t=b787ea3ed1eeefd3a4d5930cfcd91264"));
        list.add(imageDemoData);
        // 放入五种类型的图片 实际使用只要选一种即可
//            imageDemoData.setByteArray(FileUtils.readFileToByteArray(new File(imagePath)));
//            imageDemoData.setFile(new File(imagePath));
//            imageDemoData.setString(imagePath);
//            imageDemoData.setInputStream(inputStream);
//            imageDemoData.setUrl(new URL( "https://i.loli.net/2019/11/10/lP3rLNUBaGtSVzc.png"));

        // 这里演示
        // 需要额外放入文字
        // 而且需要放入2个图片
        // 第一个图片靠左
        // 第二个靠右 而且要额外的占用他后面的单元格
//            WriteCellData<Void> writeCellData = new WriteCellData<>();
//            imageDemoData.setWriteCellDataFile(writeCellData);
        // 这里可以设置为 EMPTY 则代表不需要其他数据了
//            writeCellData.setType(CellDataTypeEnum.STRING);
//            writeCellData.setStringValue("额外的放一些文字");

        // 可以放入多个图片
        List<ImageData> imageDataList = new ArrayList<>();
        ImageData imageData = new ImageData();
        imageDataList.add(imageData);
//            writeCellData.setImageDataList(imageDataList);
        // 放入2进制图片
        // 图片类型
        imageData.setImageType(ImageData.ImageType.PICTURE_TYPE_PNG);
        // 上 右 下 左 需要留空
        // 这个类似于 css 的 margin
        // 这里实测 不能设置太大 超过单元格原始大小后 打开会提示修复。暂时未找到很好的解法。
        imageData.setTop(5);
        imageData.setRight(40);
        imageData.setBottom(5);
        imageData.setLeft(5);

        // 放入第二个图片
        imageData = new ImageData();
        imageDataList.add(imageData);
//            writeCellData.setImageDataList(imageDataList);
        imageData.setImageType(ImageData.ImageType.PICTURE_TYPE_PNG);
        imageData.setTop(5);
        imageData.setRight(5);
        imageData.setBottom(5);
        imageData.setLeft(50);
        // 设置图片的位置 假设 现在目标 是 覆盖 当前单元格 和当前单元格右边的单元格
        // 起点相对于当前单元格为0 当然可以不写
        imageData.setRelativeFirstRowIndex(0);
        imageData.setRelativeFirstColumnIndex(0);
        imageData.setRelativeLastRowIndex(0);
        // 前面3个可以不写  下面这个需要写 也就是 结尾 需要相对当前单元格 往右移动一格
        // 也就是说 这个图片会覆盖当前单元格和 后面的那一格
        imageData.setRelativeLastColumnIndex(1);

        // 写入数据
        EasyExcel.write(fileName, ImageDemoData.class).sheet().doWrite(list);
    }


    @PostMapping("djaosd")
    @ApiOperation("测试合并单元格导出")
    public void daosidj(HttpServletResponse response, MyTemp myTemp) {
//        String fileName = "/Users/zjarlin/Desktop/测试合并到处";
        String fileName = "测试合并到处1";
//        String fileName = "testexcel1";
        String s = "[\n" +
                   "    {\n" +
                   "        \"a\": 1,\n" +
                   "        \"b\": 2,\n" +
                   "        \"c\": 2,\n" +
                   "        \"d\": 1,\n" +
                   "        \"e\": 1\n" +
                   "    },\n" +
                   "    {\n" +
                   "        \"a\": 1,\n" +
                   "        \"b\": 2,\n" +
                   "        \"c\": 2,\n" +
                   "        \"d\": 2,\n" +
                   "        \"e\": 3\n" +
                   "    },\n" +
                   "    {\n" +
                   "        \"a\": 1,\n" +
                   "        \"b\": 4,\n" +
                   "        \"c\": 6,\n" +
                   "        \"d\": 3,\n" +
                   "        \"e\": 2\n" +
                   "    }\n" +
                   "]";
        List<MyTemp> myTemps = JSON.parseArray(s, MyTemp.class);
        EasyExcelWriteUtil.listExport(myTemps, fileName);
    }



}
