package org.jeecg.common.util.valid;

import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author zjarlin
 */
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(CustomValidators.class)
public @interface CustomValidator {
    boolean checkNull() default false;
    boolean useSpel() default false;

    String expression() default "";

    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
