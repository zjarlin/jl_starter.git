package org.jeecg.common.util.util_entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HeaderOption {

    显示("Content-Disposition", ".inline;fileName=", "显示"),
    下载("Content-attachment", ".inline;fileName=", "下载");

    private final String type;

    private final String postfix;

    private final String comment;

}
