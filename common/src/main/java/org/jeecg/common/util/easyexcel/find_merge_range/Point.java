package org.jeecg.common.util.easyexcel.find_merge_range;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/11/14 13:31
 */
@Data
@AllArgsConstructor
public class Point {
    private int row;
    private int col;
    private String value;
    private String rightValue;
    private String downValue;

}
