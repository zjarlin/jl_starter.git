package org.jeecg.common.util.poi_tl.zzsoftentity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FieldDTO {


    @ApiModelProperty("接口名称")
    String restName;
    @ApiModelProperty("接口地址")
    String restUrl;
    @ApiModelProperty("模块描述")
    String modelName;


    /**
     * 字段中文名
     */
    @ApiModelProperty("字段中文名")
    private String fieldName;
    /**
     * 字段英文名
     */
    @ApiModelProperty("字段英文名")
    private String fieldEng;
    /**
     * 字段类型
     */
    @ApiModelProperty("字段类型")
    private String fieldType;
    /**
     * 字段长度
     */
    @ApiModelProperty("字段长度")
    private String fieldLong;
}