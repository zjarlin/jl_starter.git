package org.jeecg.common.util.easyexcel.entity;

import lombok.Data;

import java.util.List;

/**
 * @author zjarlin
 * @since 2023/11/15 10:06
 */
@Data
public class ExportSheetConfig {
    //自适应列宽
    private Boolean adaptiveColumnWidth;
    // 自适应行高
    private Boolean adaptiveRowHeight;
    // 取消导出Excel的默认风格
    private Boolean useDefaultStyle;
    //自动清理空值
    private Boolean autoTrim;
    //自动合并表头
    private Boolean automaticMergeHead;
    /**
     * 是否自动合并单元格
     */
    private Boolean automaticallyMergeCells;


    /**
     * 是否居中
     */
    private Boolean isCenter;
    private String sheetName;
    private List<?> data;

    public ExportSheetConfig() {

        //自适应列宽
        this.adaptiveColumnWidth = false;
        // 自适应行高
        this.adaptiveRowHeight = false;
        // 取消导出Excel的默认风格
        useDefaultStyle = false;
        //自动清理空值
        autoTrim = true;
        //自动合并表头
        automaticMergeHead = true;
        //自动合并等值单元格
        automaticallyMergeCells = false;
        //是否居中
        isCenter = true;
    }
}
