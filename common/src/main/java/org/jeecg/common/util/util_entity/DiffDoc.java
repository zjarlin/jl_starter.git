package org.jeecg.common.util.util_entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zjarlin
 * @since 2023/10/10 16:13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public
class DiffDoc {
    /**
     * 字段名称
     */
    private String fieldNameA;

    /**
     * 字段类型
     */
    private String simpleTypeNameA;

    /**
     * 字段描述
     */
    private String commentA;

    /**
     * 字段名称
     */
    private String fieldNameB;

    /**
     * 字段类型
     */
    private String simpleTypeNameB;

    /**
     * 字段描述
     */
    private String commentB;
}
