package org.jeecg.common.util.mputil;

import com.baomidou.mybatisplus.extension.service.IService;
import lombok.Data;

import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * @author zjarlin
 * @since 2023/6/11 09:08
 */
@Data
public class McConfigs <P,C>{
    private Collection<C> cs;
    private IService<C> css;
    private Function<C, String> cgetPidFun;
    private BiConsumer<C, P> csetPidFun4P;
    private BiConsumer<C, String> csetPidFun4Pid;
}
