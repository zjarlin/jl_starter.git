package org.jeecg.common.util.easyexcel.entity;

import cn.hutool.core.io.FileUtil;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author zjarlin
 * @since 2023/11/15 11:30
 */
@Data
public class MyTemp {
    @ExcelProperty("A")
    private String a;
    @ExcelProperty("B")
    private String b;
    @ExcelProperty("C")
    private String c;
    @ExcelProperty("D")
    private String d;
    @ExcelProperty("E")
    private String e;

    private URL url;

    private byte[] bytes;

    {
        byte[] bytes1 = FileUtil.readBytes("/Users/zjarlin/Desktop/converter/img.jpg");
        bytes = bytes1;
        try {
            url = new URL("https://img1.baidu.com/it/u=1414232962,2149362867&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1700154000&t=b787ea3ed1eeefd3a4d5930cfcd91264");
        } catch (MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
