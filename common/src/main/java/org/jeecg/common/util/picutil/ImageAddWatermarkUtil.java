//package org.jeecg.common.util.picutil;
//
//import cn.hutool.core.util.IdUtil;
//import com.google.common.base.Joiner;
//import com.google.common.base.Splitter;
//import java.io.File;
//import java.text.MessageFormat;
//import java.util.Arrays;
//import java.util.List;
//
///** @Author 剑客阿良_ALiang @Date 2021/11/20 19:30 @Description: 图片加水印工具类 */
//public class ImageAddWatermarkUtil {
//
//  /**
//   * 图片添加文字水印
//   *
//   * @param imagePath 图片地址
//   * @param outputDir 输出目录
//   * @param text      水印文本
//   * @param fontName  字体名
//   * @param fontSize  字体大小
//   * @param position  文字位置，x:y 例如100:100
//   * @param fontColor 文字颜色
//   * @param enableBox 是否带背景
//   * @param boxColor  背景颜色
//   * @return 结果图片
//   * @throws Exception
//   */
//  public static String addText(
//      String imagePath,
//      String outputDir,
//      String text,
//      String fontName,
//      Integer fontSize,
//      String position,
//      String fontColor,
//      Boolean enableBox,
//      String boxColor)
//      throws Exception {
//    List<String> paths = Splitter.on(".").splitToList(imagePath);
//    String ext = paths.get(paths.size() - 1);
//    if (!Arrays.asList("png", "jpg").contains(ext)) {
//      throw new Exception("format error");
//    }
//    String resultPath =
//        Joiner.on(File.separator).join(Arrays.asList(outputDir, IdUtil.simpleUUID() + "." + ext));
//    ProcessBuilder builder =
//        new ProcessBuilder(
//            "C:\\ffpg\\bin\\ffmpeg.exe",
//            "-i",
//            imagePath,
//            "-vf",
//            MessageFormat.format(
//                "drawtext=\"fontsize={0}:fontfile={1}:text={2}:x={3}:y={4}:fontcolor={5}:box={6}:boxcolor={7}\"",
//                fontSize,
//                fontName,
//                text,
//                Splitter.on(":").splitToList(position).get(0),
//                Splitter.on(":").splitToList(position).get(1),
//                fontColor,
//                enableBox ? 1 : 0,
//                boxColor),
//            "-y",
//            resultPath);
//    builder.inheritIO().start().waitFor();
//    return resultPath;
//  }
//
//  /**
//   * 图片添加图片水印
//   *
//   * @param imagePath 图片地址
//   * @param outputDir 输出目录
//   * @param logoPath  logo地址
//   * @param position  文字位置，x:y 例如100:100
//   * @return 结果图片
//   * @throws Exception
//   */
//  public static String addLogo(String imagePath, String outputDir, String logoPath, String position)
//      throws Exception {
//    List<String> paths = Splitter.on(".").splitToList(imagePath);
//    String ext = paths.get(paths.size() - 1);
//    if (!Arrays.asList("png", "jpg").contains(ext)) {
//      throw new Exception("format error");
//    }
//    String resultPath =
//        Joiner.on(File.separator).join(Arrays.asList(outputDir, IdUtil.simpleUUID() + "." + ext));
//    ProcessBuilder builder =
//        new ProcessBuilder(
//            "C:\\ffpg\\bin\\ffmpeg.exe",
//            "-i",
//            imagePath,
//            "-vf",
//            MessageFormat.format(
//                "movie={0}[wm];[in][wm]overlay={1}:{2}[out]",
//                logoPath,
//                Splitter.on(":").splitToList(position).get(0),
//                Splitter.on(":").splitToList(position).get(1)),
//            "-y",
//            resultPath);
//    builder.inheritIO().start().waitFor();
//    return resultPath;
//  }
//}
