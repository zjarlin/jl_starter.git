package org.jeecg.common.util.util_entity.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Version {
    private String major;
    private String minor;
    private String patch;

    public static Version of(String version) {
        if (StringUtils.isNotEmpty(version)) {
            String[] versions = version.split("\\.");
            return new Version(versions[0], versions[1], versions[2]);
        } else {
            return null;
        }
    }
}
