package org.jeecg.common.util;

import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/10/20 14:13
 */
@Data
public class Temp {
 private String    grouptype;
 private Integer fbsm;
 private String acode;
 private String bsm;
 private String groupname;
 private String routename;
 private String sorttag;
}
