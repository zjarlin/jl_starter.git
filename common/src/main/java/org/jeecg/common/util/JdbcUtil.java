package org.jeecg.common.util;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author zjarlin
 * @since 2023/10/16 15:16
 */
//@Component

public class JdbcUtil {
//    @PostConstruct
//    public void init() {
//        jdbcTemplate = SpringContextUtils.getBean(JdbcTemplate.class);
//    }

    static {
       jdbcTemplate= SpringContextUtils.getBean(JdbcTemplate.class);
    }

    private static JdbcTemplate jdbcTemplate;

    public static boolean isTableExists(String tableName) {
        String sql = "SELECT COUNT(*) FROM information_schema.tables WHERE table_name = ?";
        int count = jdbcTemplate.queryForObject(sql, Integer.class, tableName);
        return count > 0;
    }

    public static boolean isColumnExists(String tableName, String columnName) {
        String sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name = ? AND column_name = ?";
        int count = jdbcTemplate.queryForObject(sql, Integer.class, tableName, columnName);
        return count > 0;
    }
}
