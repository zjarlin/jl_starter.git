package org.jeecg.common.util;

import cn.hutool.core.util.StrUtil;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author addzero
 * @since 2022/10/8 11:11 AM
 */
@Component
public class Envs {

    @Autowired
    Environment environment;

    public String getCurrentEnv() {
        return isActive("local") ? "local"
            : isActive("dev") ? "dev" : isActive("test") ? "test" : isActive("prod") ? "prod" : "";

    }

    public boolean isActive(final String envseq) {
        final String[] activeProfiles = environment.getActiveProfiles();
        return Arrays.stream(activeProfiles)
            .anyMatch(env -> StrUtil.containsIgnoreCase(envseq, env) || StrUtil.containsIgnoreCase(env, envseq));
    }

}
