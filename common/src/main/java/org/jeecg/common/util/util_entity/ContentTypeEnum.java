package org.jeecg.common.util.util_entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ContentTypeEnum {

    DOC("doc", ".doc", "application/msword"),
    DOCX("docx", ".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
    DOT("dot", ".dot", "application/msword"),
    DOTX("dotx", ".dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"),
    XLS("xls", ".xls", "application/vnd.ms-excel"),
    XLSX("xlsx", ".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
    PPT("ppt", ".ppt", "application/vnd.ms-powerpoint"),
    PPTX("pptx", ".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"),
    PDF("pdf", ".pdf", "application/pdf"),
    TXT("txt", ".txt", "text/plain"),
    GIF("gif", ".gif", "image/gif"),
    JPEG("jpeg", ".jpeg", "image/jpeg"),
    JPG("jpg", ".jpg", "image/jpeg"),
    PNG("png", ".png", "image/png"),
    CSS("css", ".css", "text/css"),
    HTML("html", ".html", "text/html"),
    HTM("htm", ".htm", "text/html"),
    XSL("xsl", ".xsl", "text/xml"),
    XML("xml", ".xml", "text/xml"),
    MPEG("mpeg", ".mpeg", "video/mpeg"),
    MPG("mpg", ".mpg", "video/mpeg"),
    AVI("avi", ".avi", "video/x-msvideo"),
    MOVIE("movie", ".movie", "video/x-sgi-movie"),
    BIN("bin", ".bin", "application/octet-stream"),
    EXE("exe", ".exe", "application/octet-stream"),
    SO("so", ".so", "application/octet-stream"),
    DLL("dll", ".dll", "application/octet-stream"),
    AI("ai", ".ai", "application/postscript"),
    DIR("dir", ".dir", "application/x-director"),
    JS("js", ".js", "application/x-javascript"),
    SWF("swf", ".swf", "application/x-shockwave-flash"),
    XHTML("xhtml", ".xhtml", "application/xhtml+xml"),
    XHT("xht", ".xht", "application/xhtml+xml"),
    ZIP("zip", ".zip", "application/zip"),
    MID("mid", ".mid", "audio/midi"),
    MIDI("midi", ".midi", "audio/midi"),
    MP3("mp3", ".mp3", "audio/mpeg"),
    RM("rm", ".rm", "audio/x-pn-realaudio"),
    RPM("rpm", ".rpm", "audio/x-pn-realaudio-plugin"),
    WAV("wav", ".wav", "audio/x-wav"),
    BMP("bmp", ".bmp", "image/bmp");

    private final String type;

    private final String postfix;

    private final String application;
}
