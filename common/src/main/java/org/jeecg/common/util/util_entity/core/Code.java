package org.jeecg.common.util.util_entity.core;

public interface Code {
    int COMM_BASE = 10000000;
    int MODULE_INTER_INC = 10000;
    int MODULE_INNER_INC = 500;

    Integer getCode();

    String getName();

    String getMessage();

    static Code newInstance(final Integer code, final String name, final String message) {
        return new Code() {
            public Integer getCode() {
                return code;
            }

            public String getMessage() {
                return message;
            }

            public String getName() {
                return name;
            }
        };
    }
}
