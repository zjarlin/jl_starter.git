//package org.jeecg.common.util;
//
//import io.minio.BucketExistsArgs;
//import io.minio.MakeBucketArgs;
//import io.minio.MinioClient;
//import io.minio.PutObjectArgs;
//import io.minio.StatObjectArgs;
//import io.minio.StatObjectResponse;
//import java.io.InputStream;
//import java.security.MessageDigest;
//import org.apache.commons.codec.binary.Hex;
//
//public class MinioUploader {
//
//    private static final String bucketName = "mybucket";
//
//    private static MinioClient minioClient;
//
//    /**
//     * 上传文件到minio
//     *
//     * @param stream
//     * @param relativePath
//     * @return
//     */
//    public static String upload(InputStream stream, String relativePath) throws Exception {
//        initMinio("http://localhost:9000", "minioadmin", "minioadmin");
//        if (minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())) {
//            System.out.println("Bucket already exists.");
//        } else {
//            // 创建一个名为mybucket的存储桶
//            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
//            System.out.println("create a new bucket.");
//        }
//
//        // 检查文件是否已存在
//        boolean isExist = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
//        if (isExist) {
//            StatObjectResponse stat = minioClient.statObject(StatObjectArgs.builder().bucket(bucketName).object(relativePath).build());
//            boolean isObjectExist = stat.hashCode() == stream.hashCode();
//            if (isObjectExist) {
//                System.out.println("Object already exists.");
//                return minioClient.getObjectUrl(bucketName, relativePath, 7 * 24 * 60 * 60);
//            }
//        }
//
//        // 计算文件的哈希值
//        MessageDigest md = MessageDigest.getInstance("MD5");
//        byte[] digest = md.digest(stream.readAllBytes());
//        String md5 = Hex.encodeHexString(digest);
//
//        PutObjectArgs objectArgs =
//            PutObjectArgs.builder().object(relativePath).bucket(bucketName).contentType("application/octet-stream")
//                .stream(stream, stream.available(), -1).build();
//        minioClient.putObject(objectArgs);
//        stream.close();
//        return minioClient.getObjectUrl(bucketName, relativePath, 7 * 24 * 60 * 60);
//    }
//
//    /**
//     * 初始化MinioClient
//     *
//     * @param endpoint
//     * @param accessKey
//     * @param secretKey
//     */
//    public static void initMinio(String endpoint, String accessKey, String secretKey) throws Exception {
//        minioClient = MinioClient.builder().endpoint(endpoint).credentials(accessKey, secretKey).build();
//    }
//
//}
