package org.jeecg.common.util;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.util.stream_wrapper.StreamWrapper;
import lombok.SneakyThrows;
import okhttp3.*;
import org.apache.commons.lang3.tuple.Triple;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static cn.hutool.core.text.CharSequenceUtil.isBlank;

/**
 * 一些常用变量
 *
 * @author zjarlin
 * @since 2023/03/02
 */
public interface Vars {
    String token = "zzPortalAuthorization=511ed13a01684630846a87d8ebcc790c5d8e8a5fd08bdc1c3ae0537d605a643eefd67ee5c9bad8fd9e1cf1023a0e0511c21a81a12b6c71fbb57083070681fa87";
    String ip = IpUtil.getLocalIp();
    String datePrefix = LocalDate.now().format(DateTimeFormatter.ISO_DATE);
    String timePrefix = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH_mm_ss_SSS_"));
    String timeSuffix = LocalDateTime.now().format(DateTimeFormatter.ofPattern("_HH_mm_ss_SSS"));

    public static void main(String[] args) {
        Triple<String, String, String> vars = getVars("费用报销申请", "", "");

        String name = vars.getLeft();
        System.out.println("name = " + name);
        String uid = vars.getMiddle();
        System.out.println("uid = " + uid);
        String bsm = vars.getRight();
        System.out.println("bsm = " + bsm);
    }

    /**
     * @param groupName
     * @param childName
     * @param mybsm     入参 入参
     * @return {@link Triple }<{@link String }, {@link String }, {@link String }> groupname uid groupid
     * @author zjarlin
     * @since 2023/10/24
     */

    @SneakyThrows
//    @Transactional
    public static Triple<String, String, String> getVars(String groupName, String childName, String mybsm) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
        RequestBody body = RequestBody.create(mediaType, "routename=onliedesign&acode=dlkc");
        Request request = new Request.Builder()
                .url("http://192.168.16.123:36888/zzportal/core/commongroup/getListObj")
                .method("POST", body)
                .addHeader("Cookie", token)
                .addHeader("User-Agent", "Apifox/1.0.0 (https://www.apifox.cn)")
                .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .build();
        Response response = client.newCall(request).execute();
        String string = response.body().string();
        JSONObject jsonObject = JSON.parseObject(string);
        Object o = jsonObject.get("data");
        List<Temp> temps = jsonObject.parseArray(o.toString(), Temp.class);
        List<Temp> list = StreamWrapper.lambdaquery(temps)
                .like(StrUtil.isNotBlank(groupName), Temp::getGroupname, groupName)
                .eq(StrUtil.isNotBlank(mybsm), Temp::getBsm, mybsm)
                .list();
        Set<String> collect = list.stream().map(e -> e.getBsm()).collect(Collectors.toSet());
        if (list.size() > 1) {
            throw new RuntimeException("找到多个菜单请传入标识码细化筛选条件" + collect);
        }
        Temp one = StreamWrapper.lambdaquery(temps)
                .like(true, Temp::getGroupname, groupName)
                .eq(StrUtil.isNotBlank(mybsm), Temp::getBsm, mybsm)
                .one();
        if (Objects.isNull(one)) {
            throw new RuntimeException("没查到菜单");
        }
        String bsm = one.getBsm();
        String groupname = one.getGroupname();
        String retGroupName = isBlank(childName) ? groupname : childName;
//        System.out.println("name = " + groupname);
//        Temp one = (Temp) list;
//        String bsm = one.getBsm();
//        o.stream().map(e->e.getGroupname())

        String snowflakeNextIdStr = IdUtil.getSnowflakeNextIdStr();
//        System.out.println("uid = " + snowflakeNextIdStr);
//        System.out.println("grouptype= " + bsm);
        Triple<String, String, String> of = Triple.of(retGroupName, snowflakeNextIdStr, bsm);
        return of;

//        System.out.println(snowflakeNextIdStr.length());
//        System.out.println(s.length());
    }

}
