package org.jeecg.common.util.stream_wrapper;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author zjarlin
 * @since 2023/2/15 16:57
 */
public class Test {
    public static void main(String[] args) {
        List<Stu> collect = IntStream.rangeClosed(0, 3).boxed().flatMap(i -> {
                Stu stu = new Stu() {{
                    setHigh("18" + i);
                    setName("张三" + i);
                }};
                Stu stu2 = new Stu() {{
                    setHigh("19" + i);
                    setName("李四" + i);
                }};
                return Stream.of(stu2, stu);
            }).sorted(Comparator.comparing(Stu::getName))
            .collect(Collectors.toList());

        List<Stu> list = StreamWrapper.lambdaquery(collect)
            .like(true, Stu::getHigh, "181")
            .or()
            .like(true, Stu::getHigh, "191")
            //.not()
            //.like(true, e -> e.getHigh(), "181")
            //.like(true, Stu::getName, "三")
            .negate()
            .list();
        System.out.println(collect);
        System.out.println("============");
        System.out.println(list);
    }

}
