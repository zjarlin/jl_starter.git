package org.jeecg.common.sdk.ocr.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author zjarlin
 * @since 2023/11/20 13:38
 */
@NoArgsConstructor
@Data
public
class BaiduOcrWord {

    private List<WordsResultDTO> words_result;
    private Integer words_result_num;
    private Long log_id;

}
