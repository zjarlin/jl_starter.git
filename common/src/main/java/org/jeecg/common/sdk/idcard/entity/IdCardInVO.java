package org.jeecg.common.sdk.idcard.entity;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zjarlin
 * @since 2023/3/23 13:48
 */
@Data
public class IdCardInVO {

    String access_token;

    /**
     * 是否开启身份证质量类型(边框/四角不完整、头像或关键字段被遮挡/马赛克)检测功能，默认不开启，即：false。
     * - true：开启，请查看返回参数card_quality；
     * - false：不开启
     */
    Boolean detect_quality;

    /**
     * 是否检测身份证进行裁剪，默认不检测。可选值：true-检测身份证并返回证照的 base64 编码及位置信息
     */
    Boolean detect_card;

    /**
     * 是否检测头像内容，默认不检测。
     * 可选值：true-检测头像并返回头像的 base64 编码及位置信息
     */

    Boolean detect_photo;

    /**
     * 默认值不进行图像方向自动矫正
     * - true: 开启图像方向自动矫正功能，可对旋转 90/180/270 度的图片进行自动矫正并识别
     */
    Boolean detect_direction;

    /**
     * 图像数据，base64编码后进行urlencode，要求base64编码和urlencode后大小不超过4M，最短边至少15px，最长边最大4096px,支持jpg/jpeg/png/bmp格式
     */
    private String image;

    /**
     * 图片完整URL，URL长度不超过1024字节，URL对应的图片base64编码后大小不超过4M，最短边至少15px，最长边最大4096px,支持jpg/jpeg/png/bmp格式，当image字段存在时url字段失效
     * 请注意关闭URL防盗链
     */
    private String url;

    /**
     * -front：身份证含照片的一面
     * -back：身份证带国徽的一面
     * 自动检测身份证正反面，如果传参指定方向与图片相反，支持正常识别，返回参数image_status字段为"reversed_side"
     */
    @NotBlank
    private String id_card_side;

    /**
     * 是否开启身份证风险类型(身份证复印件、临时身份证、身份证翻拍、修改过的身份证)检测功能，默认不开启，即：false。
     * - true：开启，请查看返回参数risk_type；
     * - false：不开启
     */
    private Boolean detect_risk;

    private Boolean AESEncry;
}
