package org.jeecg.common.sdk.ipsearch;

import io.swagger.annotations.ApiOperation;
import net.dreamlu.mica.ip2region.core.Ip2regionSearcher;
import net.dreamlu.mica.ip2region.core.IpInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zjarlin
 * @since 2023/11/20 09:19
 */
@RestController
public class IpSearchUtil {
    @Autowired
    private Ip2regionSearcher regionSearcher;

    @GetMapping("/search/ip")
    @ApiOperation("搜索ip")
    public IpInfo dasjoidj(String ip) {
        String localIp = "42.194.192.162";
//        String localIp = IpUtil.getLocalIp();
        IpInfo ipInfo = regionSearcher.memorySearch(ip);
        return ipInfo;
    }

}
