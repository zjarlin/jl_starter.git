package org.jeecg.common.sdk.idcard;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import org.jeecg.common.sdk.idcard.entity.IdCardInVO;
import org.jeecg.common.sdk.idcard.entity.IdCardOutVO;
import org.jeecg.common.sdk.idcard.entity.Side;

import java.net.URLEncoder;
import java.util.Map;

import org.springframework.boot.test.context.SpringBootTest;

/**
 * 身份证识别
 */
@SpringBootTest
public class Idcard {
    //@Autowired
    //BaiduAuthServiceClient baiduAuthServiceClient;

    public static void main(String[] args) {
        //String 人像 = "http://36.99.61.20:13026/smz/sfz/20230322/410305198808190558/20230322155227z.jpg";
        String 国徽 = "http://36.99.61.20:13026/smz/sfz/20230322/410305198808190558/20230322155227f.jpg";
        IdCardOutVO idcard = Idcard.idcard(国徽, Side.back.getGeoplane());
        System.out.println(idcard);
    }

    /**
     * 重要提示代码中所需工具类
     * FileUtil,Base64Util,HttpUtil,GsonUtils请从
     * https://ai.baidu.com/file/658A35ABAB2D404FBF903F64D47C1F72
     * https://ai.baidu.com/file/C8D81F3301E24D2892968F09AE1AD6E2
     * https://ai.baidu.com/file/544D677F5D4E4F17B4122FBD60DB82B3
     * https://ai.baidu.com/file/470B3ACCA3FE43788B5A963BF0B625F3
     * 下载
     *
     * @param imageUrl
     * @param side
     */
    public static IdCardOutVO idcard(final String imageUrl, final String side) {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/idcard";
        try {
            // 本地文件路径
            String filePath = "/Users/zjarlin/Desktop/WechatIMG123.png";
            byte[] imgData = FileUtil.readBytes(filePath);
            String imgStr = Base64.encode(imgData);
            String imgParam = URLEncoder.encode(imgStr, "UTF-8");

            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            //Res token = getToken();
            //String accessToken = token.getAccessToken();
            String auth = AuthService.getAuth();
            IdCardInVO idCardInVO = new IdCardInVO();
            idCardInVO.setDetect_quality(false);
            idCardInVO.setDetect_card(false);
            idCardInVO.setDetect_photo(true);
            idCardInVO.setDetect_direction(false);
            //idCardInVO.setImage(imgParam);
            idCardInVO.setUrl(imageUrl);
            idCardInVO.setId_card_side(side);
            idCardInVO.setDetect_risk(false);
            idCardInVO.setAESEncry(false);
            idCardInVO.setAccess_token(auth);
            Map<String, Object> stringObjectMap = BeanUtil.beanToMap(idCardInVO);

            String result = HttpUtil.post(url, stringObjectMap);
            IdCardOutVO idCardOutVO = JSON.parseObject(result, IdCardOutVO.class);
            System.out.println(result);
            return idCardOutVO;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
