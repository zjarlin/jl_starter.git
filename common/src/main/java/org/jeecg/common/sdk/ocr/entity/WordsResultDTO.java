package org.jeecg.common.sdk.ocr.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zjarlin
 * @since 2023/11/20 13:39
 */
@NoArgsConstructor
@Data
public class WordsResultDTO {
    private String words;
}
