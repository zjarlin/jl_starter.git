//package org.jeecg.common.util.idcard;
//
//import cn.hutool.core.io.FileUtil;
//import cn.hutool.http.HttpUtil;
//import com.alibaba.fastjson.JSONObject;
//import java.net.URLEncoder;
//import java.util.Base64;
//import java.util.HashMap;
//import javax.crypto.Cipher;
//import javax.crypto.spec.SecretKeySpec;
//
//public class IdcardAes {
//
//    /**
//     * 重要提示代码中所需工具类
//     * FileUtil,HttpUtil请从
//     * https://ai.baidu.com/file/658A35ABAB2D404FBF903F64D47C1F72
//     * https://ai.baidu.com/file/544D677F5D4E4F17B4122FBD60DB82B3
//     * 下载
//     */
//
//    // 请求url
//    static String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/idcard";
//
//    // aes key 从console控制台获取
//    static String aesKey = "[16位 aeskey]";
//
//    static byte[] originAesKey = null;
//
//    public static void findAnnoMao(String[] args) {
//        String front = IdcardAes.idcard("front");
//        System.out.println(front);
//    }
//
//    public static String idcard(final String front) {
//        try {
//            // 本地文件路径
//            String filePath = "/Users/zjarlin/Desktop/WechatIMG123.png";
//            byte[] imgData = FileUtil.readBytes(filePath);
//
//            String imgStr = encryptImg(aesKey, imgData);
//
//            String imgParam = URLEncoder.encode(imgStr, "UTF-8");
//
//            String param = "id_card_side=" + front +
//                           "&image=" + imgParam +
//                           "&AESEncry=" + false;
//            //Res token = getToken();
//            //String accessToken = token.getAccessToken();
//            HashMap<String, Object> hashMap = new HashMap<>() {{
//                put("id_card_side", front);
//                put("image", imgParam);
//                put("AESEncry", false);
//                //put("access_token", accessToken);
//            }};
//
//            String encryptResult = HttpUtil.post(url, hashMap);
//
//            String decryptResult = parseResult(encryptResult);
//
//            //return "";
//            return decryptResult;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    /**
//     * 加密图片
//     *
//     * @param aesKey
//     * @param imgData
//     * @return
//     * @throws Exception
//     */
//    private static String encryptImg(String aesKey, byte[] imgData) throws Exception {
//        originAesKey = AesKeyUtil.parseAesKey(aesKey);
//        byte[] encImgBytes = AesUtil.encrypt(imgData, originAesKey);
//        String imgStr = Base64Util.encodeBase64(encImgBytes);
//        return imgStr;
//    }
//
//    /**
//     * 解密结果
//     *
//     * @param encryptResult
//     * @return
//     * @throws Exception
//     */
//    private static String parseResult(String encryptResult) throws Exception {
//        JSONObject obj = JSONObject.parseObject(encryptResult);
//        String result = obj.getOcrString("result");
//        byte[] arr = Base64Util.decodeBase64(result);
//        String decryptResult = new String(AesUtil.decrypt(arr, originAesKey));
//        return decryptResult;
//    }
//
//    static class AesKeyUtil {
//        private static final String HEX = "0123456789abcdef";
//
//        /**
//         * 获得原生的128位的aeskey
//         * 因为一个byte位8位最后生成的byte数组长度为16
//         * <p>
//         * 16 * 8 = 128
//         *
//         * @param hex
//         * @return
//         */
//        public static byte[] parseAesKey(String hex) throws Exception {
//            char[] data = hex.toCharArray();
//            if (data.length != 16) {
//                throw new Exception(" ase key illegal ");
//            }
//            return decode(hex.toCharArray());
//        }
//
//        private static byte[] decode(char[] data) throws IllegalArgumentException {
//            int len = data.length;
//
//            byte[] out = new byte[len];
//
//            for (int i = 0; i < len; i++) {
//                int f = toDigit(data[i]);
//                out[i] = (byte) (f);
//            }
//            return out;
//        }
//
//        private static int toDigit(char ch) {
//            return HEX.indexOf(ch);
//        }
//    }
//
//    static class AesUtil {
//
//        private static final String ALGORITHM = "AES";
//
//        private static final String ALGORITHM_STR = "AES/ECB/PKCS5Padding";
//
//        /**
//         * aes 加密
//         */
//        private static byte[] encrypt(byte[] src, byte[] aesKey) throws Exception {
//            Cipher cipher = getCipher(aesKey, Cipher.ENCRYPT_MODE);
//            byte[] ret = cipher.doFinal(src);
//            return ret;
//        }
//
//        private static Cipher getCipher(byte[] aesKey, int mode) throws Exception {
//            SecretKeySpec secretKeySpec = new SecretKeySpec(aesKey, ALGORITHM);
//            Cipher cipher = Cipher.getInstance(ALGORITHM_STR);
//            cipher.init(mode, secretKeySpec);
//            return cipher;
//        }
//
//        /**
//         * aes 解密
//         */
//        public static byte[] decrypt(byte[] src, byte[] aesKey) throws Exception {
//            Cipher cipher = getCipher(aesKey, Cipher.DECRYPT_MODE);
//            byte[] original = cipher.doFinal(src);
//            return original;
//        }
//    }
//
//    static class Base64Util {
//
//        private static Base64.Encoder ENCODER = Base64.getEncoder();
//
//        // base64 加密
//        private static Base64.Decoder DECODER = Base64.getDecoder();
//
//        /**
//         * base64加密
//         *
//         * @param arr
//         * @return
//         */
//        private static String encodeBase64(byte[] arr) {
//            String base64 = null;
//            try {
//                base64 = ENCODER.encodeToString(arr);
//            } catch (Exception e) {
//            }
//            return base64;
//        }
//
//        /**
//         * base64解密
//         *
//         * @param str
//         * @return
//         */
//        public static byte[] decodeBase64(String str) {
//            byte[] encodeBase64 = new byte[0];
//            try {
//                encodeBase64 = DECODER.decode(str);
//            } catch (Exception e) {
//            }
//            return encodeBase64;
//        }
//    }
//}
