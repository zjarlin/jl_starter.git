package org.jeecg.common.sdk.ocr.service.entity;

import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/11/20 13:55
 */
@Data
public class ShiJianDTO {

    String 试件编号;
    String 龄期;
    String 养护条件;
    String 试件规格;
    String 实测值;
    String 强度代表值;
    String 达到设计强度等级;

}
