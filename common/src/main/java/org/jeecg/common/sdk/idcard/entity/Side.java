package org.jeecg.common.sdk.idcard.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 位面
 *
 * @author zjarlin
 * @see Enum
 * @since 2023/03/23
 */
@AllArgsConstructor
@Getter
public enum Side {

    front("front"),
    back("back"),
    ;

    final String geoplane;
}
