package org.jeecg.common.sdk.ocr.service.entity;

import lombok.Data;

import java.util.List;

/**
 * @author zjarlin
 * @since 2023/11/20 13:57
 */
@Data
public
class BaoGaoDTO {
    String 工程名称;
    String 工程编号;
    String 报告编号;
    String 工程部位;
    String 样品编号;
    String 委托日期;
    String 建设单位;
    String 送样人;
    String 试验日期;
    String 委托单位;
    String 见证人;
    String 报告日期;
    String 见证单位;
    String 累计养护温度;
    String 强度等级;
    String 施工单位;
    String 检测类别;
    String 制作日期;
    String 生产厂家;
    String 代表批量;
    String 是否拆模;
    //多个
//    @Excel(ex)
//            @ExcelCollection()
    List<ShiJianDTO> shiJianDTOS;
    String 检测依据;
    String 说明;
    String 备注;
}
