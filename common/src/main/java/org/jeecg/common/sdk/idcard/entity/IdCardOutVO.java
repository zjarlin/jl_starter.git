package org.jeecg.common.sdk.idcard.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zjarlin
 * @since 2023/3/23 11:48
 */
@NoArgsConstructor
@Data
public class IdCardOutVO {
    @JsonProperty("words_result")
    private WordsResultDTO wordsResult;

    //@JsonProperty("words_result_num")
    //private Integer wordsResultNum;

    //@JsonProperty("image_status")
    //private String imageStatus;

    @JsonProperty("photo")
    private String photo;

    //@JsonProperty("idcard_number_type")
    //private Integer idcardNumberType;

    //@JsonProperty("photo_location")
    //private PhotoLocationDTO photoLocation;

    //@JsonProperty("log_id")
    //private Long logId;

    @NoArgsConstructor
    @Data
    public static class WordsResultDTO {
        @JsonProperty("姓名")
        private 姓名DTO 姓名;

        @JsonProperty("民族")
        private 民族DTO 民族;

        @JsonProperty("住址")
        private 住址DTO 住址;

        @JsonProperty("公民身份号码")
        private 公民身份号码DTO 公民身份号码;

        @JsonProperty("出生")
        private 出生DTO 出生;

        @JsonProperty("性别")
        private 性别DTO 性别;

        @NoArgsConstructor
        @Data
        public static class 姓名DTO {
            ////@JsonProperty("location")
            //private LocationDTO location;

            @JsonProperty("words")
            private String words;

            @NoArgsConstructor
            @Data
            public static class LocationDTO {
                @JsonProperty("top")
                private Integer top;

                @JsonProperty("left")
                private Integer left;

                @JsonProperty("width")
                private Integer width;

                @JsonProperty("height")
                private Integer height;
            }
        }

        @NoArgsConstructor
        @Data
        public static class 民族DTO {
            //@JsonProperty("location")
            //private LocationDTO location;

            @JsonProperty("words")
            private String words;

            @NoArgsConstructor
            @Data
            public static class LocationDTO {
                @JsonProperty("top")
                private Integer top;

                @JsonProperty("left")
                private Integer left;

                @JsonProperty("width")
                private Integer width;

                @JsonProperty("height")
                private Integer height;
            }
        }

        @NoArgsConstructor
        @Data
        public static class 住址DTO {
            //@JsonProperty("location")
            //private LocationDTO location;

            @JsonProperty("words")
            private String words;

            @NoArgsConstructor
            @Data
            public static class LocationDTO {
                @JsonProperty("top")
                private Integer top;

                @JsonProperty("left")
                private Integer left;

                @JsonProperty("width")
                private Integer width;

                @JsonProperty("height")
                private Integer height;
            }
        }

        @NoArgsConstructor
        @Data
        public static class 公民身份号码DTO {
            //@JsonProperty("location")
            //private LocationDTO location;

            @JsonProperty("words")
            private String words;

            @NoArgsConstructor
            @Data
            public static class LocationDTO {
                @JsonProperty("top")
                private Integer top;

                @JsonProperty("left")
                private Integer left;

                @JsonProperty("width")
                private Integer width;

                @JsonProperty("height")
                private Integer height;
            }
        }

        @NoArgsConstructor
        @Data
        public static class 出生DTO {
            //@JsonProperty("location")
            //private LocationDTO location;

            @JsonProperty("words")
            private String words;

            @NoArgsConstructor
            @Data
            public static class LocationDTO {
                @JsonProperty("top")
                private Integer top;

                @JsonProperty("left")
                private Integer left;

                @JsonProperty("width")
                private Integer width;

                @JsonProperty("height")
                private Integer height;
            }
        }

        @NoArgsConstructor
        @Data
        public static class 性别DTO {
            //@JsonProperty("location")
            //private LocationDTO location;

            @JsonProperty("words")
            private String words;

            @NoArgsConstructor
            @Data
            public static class LocationDTO {
                @JsonProperty("top")
                private Integer top;

                @JsonProperty("left")
                private Integer left;

                @JsonProperty("width")
                private Integer width;

                @JsonProperty("height")
                private Integer height;
            }
        }
    }

    @NoArgsConstructor
    @Data
    public static class PhotoLocationDTO {
        @JsonProperty("top")
        private Integer top;

        @JsonProperty("left")
        private Integer left;

        @JsonProperty("width")
        private Integer width;

        @JsonProperty("height")
        private Integer height;
    }
}
