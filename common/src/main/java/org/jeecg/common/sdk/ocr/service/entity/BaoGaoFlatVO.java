package org.jeecg.common.sdk.ocr.service.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/11/20 15:02
 */
@Data
public class BaoGaoFlatVO {


    String 工程名称;
    String 工程编号;
    String 报告编号;
    String 工程部位;
    String 样品编号;
    String 委托日期;
    String 建设单位;
    String 送样人;
    String 试验日期;
    String 委托单位;
    String 见证人;
    String 报告日期;
    String 见证单位;
    String 累计养护温度;
    String 强度等级;
    String 施工单位;
    String 检测类别;
    String 制作日期;
    String 生产厂家;
    String 代表批量;
    String 是否拆模;


    String 检测依据;
    @ExcelIgnore
    String 说明;
    @ExcelIgnore
    String 备注;
    //子表数据
    String 试件编号;
    String 龄期;
    String 养护条件;
    String 试件规格;
    String 实测值;
    String 强度代表值;
    String 达到设计强度等级;

}
