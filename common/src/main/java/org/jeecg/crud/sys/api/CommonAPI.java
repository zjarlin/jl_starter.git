package org.jeecg.crud.sys.api;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.crud.sys.model.DictModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 通用api
 *
 * @author: jeecg-boot
 */
@RestController
@RequestMapping("sys/common")
@Api(tags = "共通Api")
//@FeignClient(value = "djasoij",url = "djaoisj")
//@GenFeignInvokeController(pkgName = "org.jeecg.crud.sys.controller")
public interface CommonAPI {

    /**
     * 6任意表表的列翻译
     *
     * @param table 表名
     * @param text  汉字列
     * @param code  code列
     * @param key   code入参
     * @return {@link String } 汉字列对应值
     * @author zjarlin
     * @since 2023/01/05
     */
    @GetMapping("translateDictionaryFromAnyTable")
    @ApiOperation("从任意表翻译字典")
    String translateDictFromTable(@RequestParam String table, @RequestParam String text, @RequestParam String code, @RequestParam String key);

    /**
     * 7内置字典的翻译
     *
     * @param code
     * @param key  入参
     * @return {@link String }
     * @author zjarlin
     * @since 2023/01/05
     */
    @ApiOperation("内置字典的翻译")
    @GetMapping("translationOfBuiltInDictionary")
    String translateDict(@RequestParam String code, @RequestParam String key);

    /**
     * 14 普通字典的翻译，根据多个dictCode和多条数据，多个以逗号分割
     *
     * @param dictCodes 例如：user_status,sex
     * @param keys      例如：1,2,0
     * @return
     */
    @ApiOperation("内置字典的批量翻译")
    @GetMapping("batchTranslationOfBuiltInDictionary")
    Map<String, List<DictModel>> translateManyDict(String dictCodes, String keys);

    /**
     * 15 字典表的 翻译，可批量
     *
     * @param table
     * @param text
     * @param code
     * @param keys  多个用逗号分割
     * @return
     */
    @ApiOperation("任意表的批量翻译")
    @GetMapping("batchTranslationOfAnyTable")
    List<DictModel> translateDictFromTableByKeys(String table, String text, String code, String keys);
    List<JSONObject> translateDictFromTableByKeys2(String table, String text, String code, String keys);
}
