# docker启动 MinIO 对象存储

containerName='minio' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='no' #重启策略可选
docker run -dit --restart=$Restart_strategy \
-v $reference/data:/data \
-v $reference/config:/root/.minio \
-p 9000:9000 \
-p 9001:9001 \
--name $containerName \
-e MINIO_ROOT_USER=minio \
-e MINIO_ROOT_PASSWORD=minio \
minio/minio server /data \
--console-address ":9001" --address ":9000"
