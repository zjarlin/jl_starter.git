# docker启动teamcity
containerName='teamcity' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='always' #重启策略可选
docker run -dit --restart=$Restart_strategy \
-v $reference/data:/data/teamcity_server/datadir \
-v $reference/logs:/opt/teamcity/logs \
-p 8111:8111 \
--name $containerName \
jetbrains/teamcity-server

