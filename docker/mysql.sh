# docker启动mysql
#------------------启动mysql容器  -ved     ---------------------
containerName='mysql' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='always' #重启策略可选
docker run -dit --restart=$Restart_strategy \
-v $reference/conf:/etc/mysql \
-v $reference/logs:/var/log/mysql \
-v $reference/data:/var/lib/mysql \
-p 3306:3306 \
--name $containerName \
-e MYSQL_ROOT_PASSWORD=root \
-d mysql
