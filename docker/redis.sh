# docker启动redis
#------------------启动redis     ---------------------
containerName='redis' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='always' #重启策略可选
docker run --restart=$Restart_strategy \
-p 6379:6379 \
--name $containerName \
-v $reference/conf:/etc/redis/redis.conf \
-v $reference/data:/data \
-d redis redis-server /etc/redis/redis.conf \
--appendonly yes
