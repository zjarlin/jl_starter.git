# docker启动teamcity

containerName='teamcity' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='always' #重启策略可选
docker run -dit --restart=$Restart_strategy \
-v $reference/data:/data/teamcity_server/datadir \
-v $reference/logs:/opt/teamcity/logs \
-p 8111:8111 \
--name $containerName \
jetbrains/teamcity-server




# docker启动 MinIO 对象存储

```sh
containerName='minio' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='no' #重启策略可选
docker run -dit --restart=$Restart_strategy \
-v $reference/data:/data \
-v $reference/config:/root/.minio \
-p 9000:9000 \
-p 9001:9001 \
--name $containerName \
-e MINIO_ROOT_USER=minio \
-e MINIO_ROOT_PASSWORD=minio \
minio/minio server /data \
--console-address ":9001" --address ":9000"


```

# docker启动nacos

```sh
#------------------启动nacos容器    ---------------------
containerName='jeecg-nacos' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
containerName='nacos' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
restartStrategy='always'
mkdir -p $reference/conf
docker run -dit --restart=$restartStrategy  \
-p 8848:8848 \
-e MODE=standalone \
-v $reference/logs:/home/nacos/logs \
--name $containerName \
-e SPRING_DATASOURCE_PLATFORM=mysql \
-e MYSQL_SERVICE_HOST=`ip`  \
-e MYSQL_SERVICE_PORT=3306  \
-e MYSQL_SERVICE_USER=root \
-e MYSQL_SERVICE_PASSWORD=root  \
-e MYSQL_SERVICE_DB_NAME=nacos  \
-e TIME_ZONE='Asia/Shanghai'  \
--restart=always  \
nacos/nacos-server
zhusaidong/nacos-server-m1:2.0.3

-p 9848:9848 \
-p 9849:9849 \
-v $reference/conf/application.properties:/home/nacos/conf/application.properties \

```

# # docker启动eureka

```sh
docker run -dit --name eureka -p 8761:8761 springcloud/eureka
```

# docker启动mysql

```sh
#------------------启动mysql容器  -ved     ---------------------
containerName='mysql' #指定容器名字

containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='always' #重启策略可选
docker run -dit --restart=$Restart_strategy \
-v $reference/conf:/etc/mysql \
-v $reference/logs:/var/log/mysql \
-v $reference/data:/var/lib/mysql \
-p 3306:3306 \
--name $containerName \
-e MYSQL_ROOT_PASSWORD=root \
-d mysql
```

# docker启动ngnix

```sh
containerName='ngnix' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='always' #重启策略可选
mkdir -p $reference/data
mkdir -p $reference/html
mkdir -p $reference/log
docker run --restart=$Restart_strategy \
-p 80:80 \
--name $containerName \
-d nginx



docker cp a367a82a2133:/etc/nginx  $reference/data
docker cp a367a82a2133:/usr/share/nginx/html   $reference
docker cp a367a82a2133:/var/log/nginx   $reference/log
# 关闭该容器
docker stop a367a82a2133
# 删除该容器
docker rm a367a82a2133



docker run --name nginx -p 80:80 \
-v $reference/data/nginx:/etc/nginx \
-v $reference/html:/usr/share/nginx/html \
-v $reference/log/nginx:/var/log/nginx \
-d nginx

```

# docker启动redis

```sh
#------------------启动redis     ---------------------
containerName='redis' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='always' #重启策略可选
docker run --restart=$Restart_strategy \
-p 6379:6379 \
--name $containerName \
-v $reference/conf:/etc/redis/redis.conf \
-v $reference/data:/data \
-d redis redis-server /etc/redis/redis.conf \
--appendonly yes
```



# docker 启动zookeeper

```sh
containerName=zookeeper
containerDataPath="$HOME/DockerData"

docker run -d \
-p 2181:2181 \
-v $ContainerDataPath/$containerName/data:/data/ \
--name=$containerName \
--privileged zookeeper
```

# docker启动openresty(是Nginx增强版)

```sh
docker run -it --name openresty -p 80:80 openresty/openresty
```

# docker启动rabbitMQ 端口为15672

```sh
docker run -it --rm \
--name rabbitmq \
-p 5672:5672 \
-p 15672:15672 \
rabbitmq:3-management
```

