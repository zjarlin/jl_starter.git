# docker启动ngnix
containerName='ngnix' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
Restart_strategy='always' #重启策略可选
mkdir -p $reference/data
mkdir -p $reference/html
mkdir -p $reference/log
docker run --restart=$Restart_strategy \
-p 80:80 \
--name $containerName \
-d nginx
docker cp a367a82a2133:/etc/nginx  $reference/data
docker cp a367a82a2133:/usr/share/nginx/html   $reference
docker cp a367a82a2133:/var/log/nginx   $reference/log
# 关闭该容器
docker stop a367a82a2133
# 删除该容器
docker rm a367a82a2133
docker run --name nginx -p 80:80 \
-v $reference/data/nginx:/etc/nginx \
-v $reference/html:/usr/share/nginx/html \
-v $reference/log/nginx:/var/log/nginx \
-d nginx
