# docker启动nacos

#------------------启动nacos容器    ---------------------
containerName='jeecg-nacos' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
containerName='nacos' #指定容器名字
containerDataName='DockerData'
reference="$HOME/$containerDataName/$containerName"
restartStrategy='always'
mkdir -p $reference/conf
docker run -dit --restart=$restartStrategy  \
-p 8848:8848 \
-e MODE=standalone \
-v $reference/logs:/home/nacos/logs \
--name $containerName \
-e SPRING_DATASOURCE_PLATFORM=mysql \
-e MYSQL_SERVICE_HOST=`ip`  \
-e MYSQL_SERVICE_PORT=3306  \
-e MYSQL_SERVICE_USER=root \
-e MYSQL_SERVICE_PASSWORD=root  \
-e MYSQL_SERVICE_DB_NAME=nacos  \
-e TIME_ZONE='Asia/Shanghai'  \
--restart=always  \
nacos/nacos-server
