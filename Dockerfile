# 用java8运行，这一步会pull镜像
FROM anapsix/alpine-java:8_server-jre_unlimited
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
ENV APP_PATH=/apps

#中文环境
ENV TZ="Asia/Shanghai"
# 修改字符集为UTF-8
ENV LANG=zh_CN.UTF-8
ENV LC_ALL=zh_CN.UTF-8
ENV LANGUAGE=zh_CN.UTF-8
RUN echo "export LANG=zh_CN.UTF-8" >>~/.bashrc
RUN echo "export LC_ALL=zh_CN.UTF-8" >>~/.bashrc

RUN echo   "https://mirrors.aliyun.com/alpine/v3.6/community" >> /etc/apk/repositories \
    && echo "https://mirrors.aliyun.com/alpine/v3.6/main" > /etc/apk/repositories \
    && apk update upgrade \
    && apk add ttf-dejavu \
    && ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && echo "Asia/Shanghai" > /etc/timezone



# Replace the content of /etc/apt/sources.list with Aliyun mirror sources
# Replace the content of /etc/apt/sources.list with Aliyun mirror sources
#RUN echo "deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse" > /etc/apt/sources.list && \
#    echo "deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse" >> /etc/apt/sources.list && \
#    echo "deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse" >> /etc/apt/sources.list && \
#    echo "deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse" >> /etc/apt/sources.list && \
#    echo "deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse" >> /etc/apt/sources.list && \
#    echo "deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse" >> /etc/apt/sources.list && \
#    echo "deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse" >> /etc/apt/sources.list && \
#    echo "deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse" >> /etc/apt/sources.list \

# Update APT
#RUN apt-get update
WORKDIR $APP_PATH
ADD ./jeecg-module-system/jeecg-system-start/target/*.jar $APP_PATH/apps.jar
ADD msyh.ttf /usr/share/fonts/msyh.ttf

##中文环境
#ENV LANG=zh_CN.UTF-8
#ENV LC_ALL=zh_CN.UTF-8
#ENV LANGUAGE=zh_CN.UTF-8

#ADD ./simsun.ttf /usr/share/fonts/
EXPOSE $EXPOSE_PORT
ENTRYPOINT ["java","-jar"]
CMD ["apps.jar"]
