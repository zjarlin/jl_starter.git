package org.jeecg.crud.sys.util.base642url;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import org.jeecg.common.util.Qrutil;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;

public class ImageToBase64Converter {
    public static String convertImageToBase64(String imagePath) throws IOException {
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            byte[] imageData = new byte[(int) file.length()];
            imageInFile.read(imageData);
            return Base64.getEncoder().encodeToString(imageData);
        }
    }

    public static void main(String[] args) {
        QrConfig config = QrConfig.create().setImg("/Users/zjarlin/Pictures/boxunlogo.png");
        BufferedImage generate = QrCodeUtil.generate("https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzkyMzMzMjYzOA==&scene=124#wechat_redirect", config);
//        BufferedImage bufferedImage = Qrutil.addNote(generate, null, Qrutil.NotePosition.BOTTOM_CENTER);
        String s = Qrutil.imageToBase64(generate);
        System.out.println(s);
        //        QrCodeUtil.generateAsBase64("https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzkyMzMzMjYzOA==&scene=124#wechat_redirect", QrConfig.create())
    }
}
