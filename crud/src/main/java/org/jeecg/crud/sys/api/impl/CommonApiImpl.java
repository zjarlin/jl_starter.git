package org.jeecg.crud.sys.api.impl;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.crud.sys.api.CommonAPI;
import org.jeecg.crud.sys.model.DictModel;
import org.jeecg.crud.sys.service.ISysDictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * sys基础api impl
 *
 * @author zjarlin
 * @see CommonAPI
 * @since 2023/01/04
 */
@Slf4j
@Service
public class CommonApiImpl implements CommonAPI {
    @Autowired
    private ISysDictService sysDictService;

    @Override
    public String translateDictFromTable(String table, String text, String code, String key) {
        //模拟字典翻译效果
//   sysDictService     queryTableDictTextByKey
        return sysDictService.queryTableDictTextByKey(table, text, code, key);

//        return "我是任意表翻译"+code+"后的效果";
    }

    @Override
    public String translateDict(String code, String key) {
        //模拟字典翻译效果
        //模拟字典
//        return "我是系统内置字典翻译" + code + "后的效果";
        return sysDictService.queryDictTextByKey(code, key);
    }

    @Override
    public Map<String, List<DictModel>> translateManyDict(String dictCodes, String keys) {
//        return null;
        List<String> dictCodeList = Arrays.asList(dictCodes.split(","));
        List<String> values = Arrays.asList(keys.split(","));
        return sysDictService.queryManyDictByKeys(dictCodeList, values);

    }

    @Override
    public List<DictModel> translateDictFromTableByKeys(String table, String text, String code, String keys) {
//        return null;
        return sysDictService.queryTableDictTextByKeys(table, text, code, Arrays.asList(keys.split(",")));

    }

    @Override
    public List<JSONObject> translateDictFromTableByKeys2(String table, String text, String code, String keys) {
        return sysDictService.queryTableDictTextByKeys2(table, text, code, Arrays.asList(keys.split(",")));
    }
}
