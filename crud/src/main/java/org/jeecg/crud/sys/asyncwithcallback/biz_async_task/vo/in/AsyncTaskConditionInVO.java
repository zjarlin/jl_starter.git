package org.jeecg.crud.sys.asyncwithcallback.biz_async_task.vo.in;


import java.time.LocalDateTime;


import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

/**
 * 未来任务(AsyncTask)条件查询入参传输对象
 *
 * @author zjarlin
 * @since 2023-12-11 20:59:03
 */
@Data
public class AsyncTaskConditionInVO {

    /**
     * 部门编号
     */
    @ApiModelProperty("部门编号")
    private String sysOrgCode;

    /**
     * 方法名
     */
    @ApiModelProperty("方法名")
    private String methodName;

    /**
     * 方法参数
     */
    @ApiModelProperty("方法参数")
    private String methodParameters;

    /**
     * 调用者 Bean
     */
    @ApiModelProperty("调用者 Bean")
    private String callerBean;

    /**
     * 返回类型
     */
    @ApiModelProperty("返回类型")
    private String returnType;

    /**
     * 回调 SpEL 表达式
     */
    @ApiModelProperty("回调 SpEL 表达式")
    private String callbackSpElExpression;

    /**
     * 任务状态:ing|finish
     */
    @ApiModelProperty("任务状态:ing|finish")
    private String taskStatus;


}

