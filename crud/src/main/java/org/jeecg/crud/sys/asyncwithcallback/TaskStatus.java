package org.jeecg.crud.sys.asyncwithcallback;

/**
 * @author zjarlin
 * @since 2023/12/11 21:08
 */
public interface TaskStatus {
    String COMPLETED = "finished";
    String ING = "ing";
    String ERROR = "error";
}
