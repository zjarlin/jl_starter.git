package org.jeecg.crud.sys.asyncwithcallback.biz_async_task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.po.AsyncTask;
import org.apache.ibatis.annotations.Mapper;

/**
 * 未来任务(AsyncTask)表数据库访问层
 *
 * @author zjarlin
 * @since 2023-12-11 20:59:03
 */
@Mapper
public interface AsyncTaskMapper extends BaseMapper<AsyncTask> {

}

