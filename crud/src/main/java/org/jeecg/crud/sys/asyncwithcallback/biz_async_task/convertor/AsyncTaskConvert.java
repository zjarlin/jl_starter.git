package org.jeecg.crud.sys.asyncwithcallback.biz_async_task.convertor;


import org.jeecg.common.core.BaseConvert;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.vo.in.AsyncTaskConditionInVO;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.vo.out.AsyncTaskOutVO;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.po.AsyncTask;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.vo.in.AsyncTaskInVO;
import org.mapstruct.Mapper;

/**
 * 未来任务(AsyncTask)表转换类
 *
 * @author zjarlin
 * @since 2023-12-11 20:59:03
 */
@Mapper(componentModel = "spring")
public interface AsyncTaskConvert extends BaseConvert<
        AsyncTaskConditionInVO
        , AsyncTaskInVO
        , AsyncTaskOutVO
        , AsyncTask
        > {
}

