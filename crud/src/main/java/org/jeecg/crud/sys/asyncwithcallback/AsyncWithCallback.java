package org.jeecg.crud.sys.asyncwithcallback;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AsyncWithCallback {
    String value() default "异步任务描述";

    String callbackSpel() default "";

    Class<?> callBackReturnType() default Object.class;

    /**
     * 自定义线程数量
     *
     * @return int
     * @author zjarlin
     * @since 2023/12/12
     */

    int numberOfThreads() default 4;
}
