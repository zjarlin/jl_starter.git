package org.jeecg.crud.sys.asyncwithcallback.biz_async_task.controller;

import cn.hutool.core.thread.ThreadUtil;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 测试异步任务
 *
 * @author zjarlin
 * @since 2023/12/12 08:33
 */
@Service
public class TestSyncWithCallBackService {
    @Async
    public Pair<String, String> oijdaoi(String 我是耗时任务的入参) {
        boolean sleep = ThreadUtil.sleep(5000);
        String 我是耗时任务结束的返回 = "我是耗时任务结束的返回";
        System.out.println(我是耗时任务结束的返回);
        return Pair.of("耗时任务结束了", 我是耗时任务结束的返回);
    }

    public Pair<String, String> doiajsoij(Pair<String, String> 耗时任务的返回) {
        String right = 耗时任务的返回.getRight();
        String 我是回调函数的返回 = "我是回调函数的返回";
        System.out.println(我是回调函数的返回);
        return Pair.of(耗时任务的返回.getRight(), 我是回调函数的返回);
    }


}
