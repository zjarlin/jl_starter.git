package org.jeecg.crud.sys.asyncwithcallback.biz_async_task.po;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecg.common.util.util_entity.core.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 未来任务(AsyncTask)表实体类
 *
 * @author zjarlin
 * @since 2023-12-11 20:59:03
 */
@Data
@TableName(value = "biz_async_task")
@EqualsAndHashCode(callSuper = true)
public class AsyncTask extends BaseEntity {
    @ApiModelProperty("业务描述")
    private String bizComment;
    @ApiModelProperty("当前耗时任务结果集")
    private String currResult;
    @ApiModelProperty("回调任务结果集")
    private String callbackResult;
    @TableField(exist = false)
    private Class<?> callBackReturnType;

    /**
     * 任务起始时间戳
     */
    @ApiModelProperty("任务起始时间戳")
    private Long taskStartTimestamp;

    /**
     * 任务终止时间戳
     */
    @ApiModelProperty("任务终止时间戳")
    private Long taskEndTimestamp;

    /**
     * 回调终止时间戳
     */
    @ApiModelProperty("回调终止时间戳")
    private Long callbackEndTimestamp;


    /**
     * 方法名
     */
    @ApiModelProperty("方法名")
    @Excel(name = "方法名", width = 15)
    private String methodName;

    /**
     * 方法参数
     */
    @ApiModelProperty("方法参数")
    @Excel(name = "方法参数", width = 15)
    private String methodParameters;

    /**
     * 调用者 Bean
     */
    @ApiModelProperty("调用者 Bean")
    @Excel(name = "调用者 Bean", width = 15)
    private String callerBean;

    /**
     * 返回类型
     */
    @ApiModelProperty("返回类型")
    @Excel(name = "返回类型", width = 15)
    private String returnType;
    /**
     * 当前耗时方法返回类型
     */
    @ApiModelProperty("返回类型")
    @Excel(name = "返回类型", width = 15)
    @TableField(exist = false)
    private Class<?> currentReturnTypeClass;

    /**
     * 当前异步方法  SpEL 表达式
     */
    @ApiModelProperty("当前异步方法  SpEL 表达式")
    @Excel(name = "当前异步方法 SpEL 表达式", width = 15)
    private String currentSpElExpression;
    /**
     * 回调 SpEL 表达式
     */
    @ApiModelProperty("回调 SpEL 表达式")
    @Excel(name = "回调 SpEL 表达式", width = 15)
    private String callbackSpElExpression;

    /**
     * 任务状态:ing|finish
     */
    @ApiModelProperty("任务状态:ing|finish")
    @Excel(name = "任务状态:ing|finish", width = 15)
    private String taskStatus;

}
