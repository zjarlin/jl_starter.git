package org.jeecg.crud.sys.asyncwithcallback.biz_async_task.service;



import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.po.AsyncTask;


/**
 * 未来任务(AsyncTask)表服务接口
 *
 * @author zjarlin
 * @since 2023-12-11 20:59:02
 */
public interface AsyncTaskService extends IService<AsyncTask> {

}

