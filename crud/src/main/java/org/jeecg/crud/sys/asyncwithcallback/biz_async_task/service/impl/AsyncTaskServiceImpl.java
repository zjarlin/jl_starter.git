package org.jeecg.crud.sys.asyncwithcallback.biz_async_task.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.mapper.AsyncTaskMapper;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.po.AsyncTask;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.service.AsyncTaskService;
import org.springframework.stereotype.Service;

/**
 * 未来任务(AsyncTask)表服务实现类
 *
 * @author zjarlin
 * @since 2023-12-11 20:59:02
 */
@Service
public class AsyncTaskServiceImpl extends ServiceImpl<AsyncTaskMapper, AsyncTask> implements AsyncTaskService {

}

