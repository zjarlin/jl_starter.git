package org.jeecg.crud.sys.asyncwithcallback.biz_async_task.vo.out;


import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.po.AsyncTask;
import lombok.EqualsAndHashCode;

import lombok.Data;

/**
 * 未来任务(AsyncTask)出参数据传输对象
 *
 * @author zjarlin
 * @since 2023-12-11 20:59:03
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AsyncTaskOutVO extends AsyncTask {

}
