package org.jeecg.crud.sys.asyncwithcallback.biz_async_task.controller;


import org.jeecg.common.core.BaseController;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.convertor.AsyncTaskConvert;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.po.AsyncTask;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.vo.in.AsyncTaskConditionInVO;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.vo.in.AsyncTaskInVO;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.vo.out.AsyncTaskOutVO;
import org.jeecg.crud.sys.asyncwithcallback.biz_async_task.service.AsyncTaskService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 未来任务(AsyncTask)表控制层
 *
 * @author zjarlin
 * @since 2023-12-11 20:59:03
 */
@Api(tags = "未来任务Api")
@RestController
@RequestMapping("/biz/asyncTask")
public class AsyncTaskController extends BaseController<AsyncTaskService, AsyncTaskConvert
        , AsyncTaskConditionInVO
        , AsyncTaskInVO
        , AsyncTaskOutVO
        , AsyncTask
        > {
    @GetMapping("dajsodi")
    public List<AsyncTask> dajosij() {
        List<AsyncTask> list = service.list();
        return list;
    }


}

