package org.jeecg.crud;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.util.Vars;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

import java.util.Optional;

@SpringBootApplication(
        scanBasePackages =
                {
                        //"com.addzero",
                        "org.jeecg"
                }
)
@ComponentScan(basePackages = {"org.jeecg"})
//@EnableFeignClients(basePackages = {
//        "com.gtmc.infra.srv.base.api",
//        "com.gtmc.infra.srv.receive.api",
//        "com.gtmc.agg.srv.central.api",
//        "com.gtmc.bff.srv.backend.core.auth.api"
//})

@MapperScan({
//        "org.jeecg.crud.sys.mapper",
        "org.jeecg.**.mapper"
//        , "org.jeecg.crud.mapper"
//        , "org.jeecg.crud.sys.mapper"
})
@Slf4j
//@EnableFeignClients(basePackages = "org.jeecg.common.util.idcard")
public class CrudApplication {

    @SneakyThrows
    public static void main(String[] args) {
        final ConfigurableApplicationContext application = SpringApplication.run(CrudApplication.class, args);

       Environment env = application.getEnvironment();
        String ip = Vars.ip;
        String port = env.getProperty("server.port");
        final String path = Optional.ofNullable(env.getProperty("server.servlet.context-path")).orElse("");
        log.info("\n----------------------------------------------------------\n\t" +
                "Application is running! Access URLs:\n\t" +
//                "Local: \t\thttp://localhost:" + port + path + "/\n\t" +
//                "External: \thttp://" + ip + ":" + port + path + "/\n\t" +
                "本机Swagger文档: \thttp://" + "localhost" + ":" + port + path + "/doc.html\n\t" +
                "Swagger文档: \thttp://" + ip + ":" + port + path + "/doc.html\n\t" +
                "----------------------------------------------------------");

    }

}
