package org.jeecg.crud.demotest.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.core.Result;
import org.jeecg.common.rc.aop.dicttrans.classutil.Example;
import org.jeecg.common.rc.aop.dicttrans.classutil.Example2;
import org.jeecg.common.rc.aop.dicttrans.dictaop.NeedTransLate;
import org.jeecg.common.rc.base64aop.Base64ToMinio;
import org.jeecg.common.rc.exception.JlException;
import org.jeecg.common.rc.monitor.Monitor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.houbb.data.factory.core.util.DataUtil;
import io.swagger.annotations.ApiOperation;
import java.util.Arrays;
import java.util.List;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController implements ExampleApi {

    @Autowired
    ObjectMapper objectMapper;

    public static void main(String[] args) {
        SaveCaptureRecordInVO b1 = DataUtil.build(SaveCaptureRecordInVO.class);
        SaveCaptureRecordInVO b2 = DataUtil.build(SaveCaptureRecordInVO.class);
        SaveCaptureRecordInVO b3 = DataUtil.build(SaveCaptureRecordInVO.class);
        List<SaveCaptureRecordInVO> bs = Arrays.asList(b1, b2, b3);
        System.out.println(JSON.toJSONString(bs));

    }

    @PostMapping("testStringUploadTransfer")
    @ApiOperation("String")
    @Base64ToMinio
    public String testStringUploadTransfer(@RequestBody ExInVO exInVO) {
        return exInVO.getS();
    }

    @PostMapping("testReturnsASingleNonPackageType7")
    @ApiOperation("7-测试返回string")
    @NeedTransLate(tab = "fhys_pro_people", codeColumn = "name", nameColumn = "phone")
    public String testReturnsASingleNonPackageTyper7() {
        return "15EhfY";
    }

    @SneakyThrows
    @PostMapping("jaosdcjoasi")
    @ApiOperation("jaosdcjoasi")
    @Monitor("测试抓拍记录入参")
    public String doaisjdoi(SaveCaptureRecordInVO vo) {

        //String s = objectMapper.writeValueAsString(vo);
        //String s1 = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(vo);
        String s1 = "cjaosdjcos";
        throw new JlException("djaosjdoaisdj");
        //Result.OK(s1)
        //return s1;

    }

    @Override
    @GetMapping("testReturnsASingleNonPackageType")
    @ApiOperation("1-测试返回List<T>")
    @NeedTransLate
    public List<Example> testReturnsASingleNonPackageType(Example example) {
        val b1 = DataUtil.build(Example.class);
        val b2 = DataUtil.build(Example.class);
        val b3 = DataUtil.build(Example.class);
        b3.setName("15EhfY");
        val bs = Arrays.asList(b1, b2, b3);
        return bs;
    }

    @Override
    @PostMapping("testReturnsASingleNonPackageType2")
    @ApiOperation("2-测试返回T")
    @NeedTransLate
    public Example testReturnsASingleNonPackageType2() {
        val b1 = DataUtil.build(Example.class);
        return b1;
    }

    @Override
    @PostMapping("testReturnsASingleNonPackageType3")
    @ApiOperation("3-测试返回Ipage<T>")
    @NeedTransLate
    public IPage<Example> testReturnsASingleNonPackageType3() {
        val b1 = DataUtil.build(Example.class);
        b1.setName("15EhfY");
        List<Example2> children2 = b1.getChildren2();
        Example2 children3 = b1.getChildren3();
        children3.setId("ULPz");
        children2.forEach(e -> e.setId("zzRN"));

        val b2 = DataUtil.build(Example.class);
        val b3 = DataUtil.build(Example.class);
        val bs = Arrays.asList(b1, b2, b3);

        IPage<Example> objectPage = new Page<>(1, 10);
        IPage<Example> exampleIPage = objectPage.setRecords(bs);
        return exampleIPage;
    }

    @Override
    @PostMapping("testReturnsASingleNonPackageType4")
    @ApiOperation("4-测试返回Result<IPage<T>>")
    @NeedTransLate
    public Result<IPage<Example>> testReturnsASingleNonPackageType4() {
        Result<IPage<Example>> ok = Result.OK(testReturnsASingleNonPackageType3());
        return ok;
    }

    @Override
    @PostMapping("testReturnsASingleNonPackageType5")
    @ApiOperation("5-测试返回Result<T>")
    @NeedTransLate
    public Result<Example> testReturnsASingleNonPackageType5() {
        val b1 = DataUtil.build(Example.class);
        return Result.OK(b1);
    }

    @Override
    @PostMapping("testReturnsASingleNonPackageType6")
    @ApiOperation("6-测试返回Result<List<T>>")
    @NeedTransLate
    public Result<List<Example>> testReturnsASingleNonPackageTyper6() {
        List<Example> examples = testReturnsASingleNonPackageType(new Example());
        return Result.OK(examples);
    }

}
