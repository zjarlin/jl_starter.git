package org.jeecg.crud.demotest.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author zjarlin
 * @since 2023/1/9 14:01
 */
@Data
public class Joijo {
    private int y;

    private Integer x;

    private String xasx;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime cj;
}
