package org.jeecg.crud.demotest.controller;

import org.jeecg.common.util.easyexcel.EasyExcelReadUtil;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zjarlin
 * @since 2023/3/24 11:16
 */
@RestController
public class ReadExcelController {
    @SneakyThrows
    public static void main(String[] args) {
        List<Person> read = EasyExcelReadUtil.read(Files.newInputStream(Paths.get("/Users/zjarlin/Desktop/人员信息导入模板.xls")), Person.class, 1);
        System.out.println(read);
    }

}
