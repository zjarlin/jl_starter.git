package org.jeecg.crud.demotest.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.common.core.Result;
import org.jeecg.common.rc.aop.dicttrans.classutil.Example;
import org.jeecg.common.rc.aop.dicttrans.dictaop.NeedTransLate;
import io.swagger.annotations.ApiOperation;
import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author zjarlin
 * @since 2023/3/8 20:10
 */
public interface ExampleApi {
    @PostMapping("testReturnsASingleNonPackageType")
    @ApiOperation("1-测试返回List<T>")
    @NeedTransLate
    List<Example> testReturnsASingleNonPackageType(@RequestBody Example example);

    @PostMapping("testReturnsASingleNonPackageType2")
    @ApiOperation("2-测试返回T")
    @NeedTransLate
    Example testReturnsASingleNonPackageType2();

    @PostMapping("testReturnsASingleNonPackageType3")
    @ApiOperation("3-测试返回Ipage<T>")
    @NeedTransLate
    IPage<Example> testReturnsASingleNonPackageType3();

    @PostMapping("testReturnsASingleNonPackageType4")
    @ApiOperation("4-测试返回Result<IPage<T>>")
    @NeedTransLate
    Result<IPage<Example>> testReturnsASingleNonPackageType4();

    @PostMapping("testReturnsASingleNonPackageType5")
    @ApiOperation("5-测试返回Result<T>")
    @NeedTransLate
    Result<Example> testReturnsASingleNonPackageType5();

    @PostMapping("testReturnsASingleNonPackageType6")
    @ApiOperation("6-测试返回Result<List<T>>")
    @NeedTransLate
    Result<List<Example>> testReturnsASingleNonPackageTyper6();
}
