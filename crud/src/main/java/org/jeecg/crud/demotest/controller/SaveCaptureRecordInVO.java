package org.jeecg.crud.demotest.controller;

import org.jeecg.common.rc.aop.dicttrans.classutil.Example;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 * 2.1抓拍记录
 *
 * @author zjarlin
 * @since 2023/03/13
 */
@Data
public class SaveCaptureRecordInVO implements Serializable {

    @ApiModelProperty("设备唯一码")
    private String deviceCode;

    @ApiModelProperty("摄像头线路")
    private String cameraName;

    @ApiModelProperty("抓拍事件")
    private List<Example> captureEvents;

    //@ApiModelProperty("抓拍图片,以文件格式提交")
    //private MultipartFile[] files;

}
