package org.jeecg.crud;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zjarlin
 * @since 2023/10/24 09:11
 */
@NoArgsConstructor
@Data
public class Caidan {

    private String mpath;
    private String icon;
    private String description;
    private Integer deltag;
    private String viewtype;
    private Object acode;
    private String mname;
    private String viewparam;
    private String sorttag;
    private String mtype;
    private String mlink;
    private String absm;
    private String fbsm;
    private String ext5;
    private String ext4;
    private String bsm;
    private String ext3;
    private String ext2;
    private String ext1;
}
