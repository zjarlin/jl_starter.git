package org.jeecg.crud;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zjarlin
 * @since 2023/10/9 10:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    String name;
    Integer age;
}
