package org.jeecg.crud;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import org.jeecg.common.util.Streams;
import org.jeecg.common.util.Temp;
import org.jeecg.common.util.stream_wrapper.StreamWrapper;
import lombok.SneakyThrows;
import okhttp3.*;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CrudApplication.class)
class DkyTest {

    @SneakyThrows
    @Test
    public void doajso() {
        List<Temp> temps1 = getTemps();
        System.out.println(temps1);
    }

    @SneakyThrows
    private static List<Temp> getTemps() {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
        RequestBody body = RequestBody.create(mediaType, "routename=onliedesign&acode=dlkc");
        Request request = new Request.Builder()
                .url("http://192.168.16.123:36888/zzportal/core/commongroup/getListObj")
                .method("POST", body)
                .addHeader("Cookie", "zzPortalAuthorization=511ed13a01684630846a87d8ebcc790c12dbe8073a4b6a0a2b0df5504f536aa64aac286532329f57858a6d610eb8b9f6dcd42114a555aa6cc7d1027c4ae09e26")
                .addHeader("User-Agent", "Apifox/1.0.0 (https://www.apifox.cn)")
                .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .build();
        Response response = client.newCall(request).execute();
        String string = response.body().string();
        JSONObject jsonObject = JSON.parseObject(string);
        Object o = jsonObject.get("data");
        List<Temp> temps1 = JSON.parseArray(o.toString(), Temp.class);
        return temps1;
    }

    public static void main(String[] args) {
        List<Temp> temps = getTemps();
        List<Caidan> caidans = getCaidans();
        List<Temp> genericDiffs = Streams.getGenericDiffs(temps, caidans, e -> e.getBsm(), e -> e.getBsm());
        List<Temp> list = StreamWrapper.lambdaquery(genericDiffs).like(true, e -> e.getGroupname(), "项目").list();
        System.out.println(genericDiffs);
    }

    @SneakyThrows
    @Test
    public void getByBsm() {

    }

    @SneakyThrows
    private static String getxmlByBsm(String uid) {
        OkHttpClient client = new OkHttpClient().newBuilder()
   .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
        RequestBody body = RequestBody.create(mediaType, "bsm="+uid+"&processversion=v1.0");
        Request request = new Request.Builder()
           .url("http://192.168.16.123:36888/zzportal/design/process/getByBsm")
           .method("POST", body)
           .addHeader("Cookie", "zzPortalAuthorization=511ed13a01684630846a87d8ebcc790c12dbe8073a4b6a0a2b0df5504f536aa64aac286532329f57858a6d610eb8b9f6dcd42114a555aa6cc7d1027c4ae09e26")
           .addHeader("User-Agent", "Apifox/1.0.0 (https://www.apifox.cn)")
           .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
           .build();
        Response response = client.newCall(request).execute();
        String string = response.body().string();
        JSONObject jsonObject = JSON.parseObject(string);
        Object o = jsonObject.get("data");
        String xml = o.toString();
        return xml;
    }
    @Value("${user.dir}")
    String s;
    String s1;
    @Test
    public void daiosjdo() {
        System.out.println(s);
        System.out.println(s1);

    }
    @Test
    public void 投影坐标转() {

    }




    @SneakyThrows
    @Test
    public void doiajsdoi() {
        List<Caidan> objects = getCaidans();
        System.out.println(objects);

    }

    @SneakyThrows
    @Nullable
    private static List<Caidan> getCaidans() {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
        RequestBody body = RequestBody.create(mediaType, "absm=1693502063645954048");
        Request request = new Request.Builder()
                .url("http://192.168.16.123:36888/zzportal/app/menu/getListObj")
                .method("POST", body)
                .addHeader("Cookie", "zzPortalAuthorization=511ed13a01684630846a87d8ebcc790c12dbe8073a4b6a0a2b0df5504f536aa64aac286532329f57858a6d610eb8b9f6dcd42114a555aa6cc7d1027c4ae09e26")
                .addHeader("User-Agent", "Apifox/1.0.0 (https://www.apifox.cn)")
                .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .build();
        Response response = client.newCall(request).execute();
        String string = response.body().string();
        JSONObject jsonObject = JSON.parseObject(string);
        Object o = jsonObject.get("data");
        List<Caidan> objects = JSON.parseArray(o.toString(), Caidan.class);
        return objects;
    }

}
