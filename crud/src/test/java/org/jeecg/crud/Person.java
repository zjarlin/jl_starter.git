package org.jeecg.crud;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author zjarlin
 * @since 2023/3/24 11:35
 */
@Data
public class Person {
    @ExcelProperty("项目名称")
    private String entryName;

    @ExcelProperty("身份证号")
    private String idNumber;

    @ExcelProperty("姓名")
    private String fullName;

    //@ExcelProperty(value = "生日")
    //private Date birthday;

    @ExcelProperty(value = "文化程度")
    private String degreeOfEducation;

    @ExcelProperty(value = "政治面貌")
    private String politicalOutlook;

    @ExcelProperty(value = "住址")
    private String address;

    @ExcelProperty(value = "民族")
    private String nation;

    @ExcelProperty(value = "联系电话")
    private String contactNumber;

    @ExcelProperty(value = "性别")
    private String gender;

    @ExcelProperty("籍贯")
    private String nativePlace;

    @ExcelProperty("紧急联系人")
    private String emergencyContact;

    @ExcelProperty("紧急联系人电话")
    private String emergencyContactPhone;

    @ExcelProperty("图片")
    private String picture;
}
