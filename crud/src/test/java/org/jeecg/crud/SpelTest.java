package org.jeecg.crud;

import org.jeecg.common.util.spelutil.SpELUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

//@SpringBootTest("")
//@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CrudApplication.class)
class SpelTest {
    @Value("#{T(org.jeecg.common.util.Vars).ip}")
    String ip;

    @Value("#{T(java.time.LocalDate).now().format(T(java.time.format.DateTimeFormatter).ISO_DATE)}")
    String datePrefix;

    @Value("#{T(java.time.LocalTime).now().format(T(java.time.format.DateTimeFormatter).ofPattern(\"HH_mm_ss_SSS_\"))}")
    String timePrefix;
    @Test
    public void doajso() {
        System.out.println(ip);
        System.out.println(datePrefix);
        System.out.println(timePrefix);
    }


    @Test
    public void doiajsdoi() {
        String s = "@commonApiImpl.translateDict(#dictCode, #fieldRuntimeStrValue)";
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("dictCode", "companyCode");
        stringObjectHashMap.put("fieldRuntimeStrValue", 1);
        String s1 = SpELUtils.evaluateExpression(stringObjectHashMap, s, String.class);
        System.out.println(s1);
    }

}
