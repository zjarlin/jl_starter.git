package org.jeecg.crud;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootTest
@EnableFeignClients(basePackages = "org.jeecg.common.util.idcard")
class OpenFegintest {

}


